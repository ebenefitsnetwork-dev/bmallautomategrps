//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AutomateBultOrders
{
    using System;
    using System.Collections.Generic;
    
    public partial class Carrier
    {
        public Carrier()
        {
            this.CarrierPlaneTypes = new HashSet<CarrierPlaneType>();
            this.CompCarriers = new HashSet<CompCarrier>();
            this.ChangeRequestProjects = new HashSet<ChangeRequestProject>();
        }
    
        public long CarrierID { get; set; }
        public string CarrierName { get; set; }
        public long OrderID { get; set; }
        public string FedralTaxID { get; set; }
        public string AlternativeID { get; set; }
        public string ProjectName { get; set; }
        public string CarrierType { get; set; }
        public string ContactName { get; set; }
        public string ContactPhone { get; set; }
        public string Website { get; set; }
        public string ContactEmail { get; set; }
        public string StreetAddress { get; set; }
        public string City { get; set; }
        public string ZipCode { get; set; }
        public string Country { get; set; }
        public string CarrierCode { get; set; }
        public string GroupNumber { get; set; }
        public Nullable<bool> CobraMembers { get; set; }
        public Nullable<long> stateID { get; set; }
        public Nullable<long> StatusId { get; set; }
        public Nullable<bool> QBCreated { get; set; }
        public Nullable<bool> Allcomp { get; set; }
        public string SecContactName { get; set; }
        public string SecContactPhone { get; set; }
        public string SecContactEmail { get; set; }
        public Nullable<System.DateTime> OEStartDate { get; set; }
        public Nullable<System.DateTime> OEEndDate { get; set; }
        public Nullable<int> ServiceType { get; set; }
        public Nullable<System.DateTime> SubmitDate { get; set; }
        public Nullable<long> EDICarrierID { get; set; }
        public Nullable<System.DateTime> CustomerDataETA { get; set; }
        public Nullable<System.DateTime> PlanYearStartDate { get; set; }
        public Nullable<long> CancelReasonID { get; set; }
        public Nullable<System.DateTime> EffectiveDate { get; set; }
        public Nullable<bool> Conversion { get; set; }
        public Nullable<System.DateTime> CurrentYearLastProductionFileDate { get; set; }
        public Nullable<bool> SndRgMail { get; set; }
        public Nullable<bool> AquireSSN { get; set; }
        public Nullable<bool> TermEnrollEmp { get; set; }
        public string TermEnrollEmpDetails { get; set; }
        public Nullable<bool> TermDeleteNoCoverageEmp { get; set; }
        public string TermDeleteNoCoverageEmpDetails { get; set; }
        public Nullable<bool> AddRemoveCoverage { get; set; }
        public string AddRemoveCoverageDetails { get; set; }
        public string CoordinationType { get; set; }
        public Nullable<int> CancellationReasonField { get; set; }
        public string OtherCancellationReason { get; set; }
        public string CancellationReasonComment { get; set; }
        public Nullable<bool> QBCancelled { get; set; }
        public Nullable<long> ChannelID { get; set; }
        public Nullable<bool> Validated { get; set; }
        public string OEQuestion { get; set; }
        public string OtherOEQuestion { get; set; }
        public Nullable<long> AnalystID { get; set; }
        public string AnalystEmail { get; set; }
        public Nullable<bool> dataready { get; set; }
        public Nullable<bool> SelfService { get; set; }
        public Nullable<int> QBprojno { get; set; }
        public Nullable<System.DateTime> EndBlackoutPeriod { get; set; }
        public Nullable<long> MultitenantId { get; set; }
        public Nullable<bool> MultitenantStatus { get; set; }
        public Nullable<long> eBNCarrierId { get; set; }
        public string FileType { get; set; }
        public Nullable<bool> UpdateTransmissionDate { get; set; }
        public Nullable<bool> IsMigrated { get; set; }
    
        public virtual CancellationReasonField CancellationReasonField1 { get; set; }
        public virtual Order Order { get; set; }
        public virtual ICollection<CarrierPlaneType> CarrierPlaneTypes { get; set; }
        public virtual ICollection<CompCarrier> CompCarriers { get; set; }
        public virtual eBNCarrier eBNCarrier { get; set; }
        public virtual ICollection<ChangeRequestProject> ChangeRequestProjects { get; set; }
    }
}
