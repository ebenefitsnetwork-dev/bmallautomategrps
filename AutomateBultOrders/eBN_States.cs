//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AutomateBultOrders
{
    using System;
    using System.Collections.Generic;
    
    public partial class eBN_States
    {
        public eBN_States()
        {
            this.CustomerCompanies = new HashSet<CustomerCompany>();
            this.Partners = new HashSet<Partner>();
            this.Customers = new HashSet<Customer>();
        }
    
        public long StateID { get; set; }
        public string State_Abb { get; set; }
        public string State_Name { get; set; }
    
        public virtual ICollection<CustomerCompany> CustomerCompanies { get; set; }
        public virtual ICollection<Partner> Partners { get; set; }
        public virtual ICollection<Customer> Customers { get; set; }
    }
}
