﻿using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomateBultOrders
{
     public class MiscHelpers
    {
        static string logpath = LoadProp.logpath;
        static ILog logger = LoadProp.logger;

        static public void LoadFile()
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                using (FileStream fileStream = new FileStream(
                    logpath,
                    FileMode.Open,
                    FileAccess.Read,
                    FileShare.ReadWrite))
                {
                    using (StreamReader streamReader = new StreamReader(fileStream))
                    {
                        sb.Append(streamReader.ReadToEnd());
                    }

                    string path = AppDomain.CurrentDomain.BaseDirectory.ToString();
                    string writelogFile = path + "log.txt";
                    using (StreamWriter outfile = new StreamWriter(writelogFile))
                    {
                        outfile.Write(sb.ToString());
                    }

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error loading log file: " + ex.Message);
                logger.Error("Stack trace=: " + ex.StackTrace);
            }
        }

        public static byte[] EncryptData(byte[] secretData)
        {
            var rsaCryptoServiceProvider = new System.Security.Cryptography.RSACryptoServiceProvider();
            rsaCryptoServiceProvider.FromXmlString("<RSAKeyValue><Modulus>kFqqx6bomtqmvhiSobhWqHpdkAK0grMEV/FdpX6N5TpDRWaD27haJdhJozjM/pNmBWnGfCFgVy49+iArlf5XMXl8rnL9izfHuzFSr/zbnf+mMlLZqxpmGQg6HO3HiHVj5cjlG52r5PB/rrSFEjV0oDaXjH4j6gpC5tJICOA9Lx8=</Modulus><Exponent>AQAB</Exponent><P>yWa1gwZH8GaGhIwxvwdT9ydShDJgWHJJagjpaz3UKr7FMZDHFGSvsgxcJaQF4DQxbrVx2jdlbtm1if1PYKMeXw==</P><Q>t3zhVi2nt/JsGWs0i4Z5H0+kfu7oAclZnYJ5KhipuTHM3msslZzIOM0h6KVQ6O9c8sN5RPXm9oaUg+hLrA4nQQ==</Q><DP>wwS1ll4qotpkP00Rjoyl/ZkSCfhN2tcvx4FBpRqFq652e/xZCaJFjv7w63HcTrG7fBwuVsN1cNVXOHsUtdq9uQ==</DP><DQ>p9WYoCU+pmkeK9n9xCoKnHNS+bA5k3jDeemgPrs0c+tzg3bw3yD7m8k23QBqE8budDgMsuFik9jh/A39ObHwgQ==</DQ><InverseQ>qfT0qUgprsf4RoOoN8YBBFtGnZbvG1GvBhKigDzVtlGsKkXlhhdKyR3JZZ1AWR2WsmQlT287hDk/l4JzfcOz8g==</InverseQ><D>dOnp/Y/SPnEusTHHuNFK5mNM2fFG78A7mVp0ZTAtjmV0zIWt78vMv3AAnADKDrmk3GeCCVEi7RkXuzhI9M+tH74xh+rXXXV63VpziimuWrAxv9DeXqSS/0WoPmaZD10kUb+mnuP7uRblNeu83xxoQJAiocH+7Uj4/M0VhyvZ2oE=</D></RSAKeyValue>");
            var lastBlockLength = secretData.Length % 58;
            var blockCount = secretData.Length / 58;
            var hasLastBlock = false;
            if (lastBlockLength != 0)
            {
                blockCount += 1;
                hasLastBlock = true;
            }
            var result = new byte[blockCount * 128];
            var currentBlock = new byte[58];
            for (var blockIndex = 0; blockIndex <= blockCount - 1; blockIndex++)
            {
                int thisBlockLength;

                //If this is the last block and we have a remainder, 
                //then set the length accordingly

                if (blockCount == blockIndex + 1 && hasLastBlock)
                {
                    thisBlockLength = lastBlockLength;
                    currentBlock = new byte[lastBlockLength];
                }
                else
                    thisBlockLength = 58;

                var startChar = blockIndex * 58;

                //Define the block that we will be working on
                //byte[] currentBlock = new byte[thisBlockLength];
                Array.Copy(secretData, startChar, currentBlock, 0, thisBlockLength);

                //Encrypt the current block and append it to the result stream
                var encryptedBlock = rsaCryptoServiceProvider.Encrypt(currentBlock, false);

                var originalResultLength = 128 * blockIndex; //result.Length;
                encryptedBlock.CopyTo(result, originalResultLength);
            }

            return result;
        }


    }
}
