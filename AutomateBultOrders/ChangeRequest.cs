//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AutomateBultOrders
{
    using System;
    using System.Collections.Generic;
    
    public partial class ChangeRequest
    {
        public ChangeRequest()
        {
            this.ChangeRequestProjects = new HashSet<ChangeRequestProject>();
        }
    
        public long ID { get; set; }
        public Nullable<long> OrderId { get; set; }
        public Nullable<long> ChRequestTypeId { get; set; }
        public Nullable<long> ChRequestStatusId { get; set; }
        public Nullable<System.DateTime> ChRequestDate { get; set; }
        public Nullable<int> UserId { get; set; }
        public string Comment { get; set; }
        public Nullable<System.DateTime> ChRequestSubmitDate { get; set; }
        public Nullable<int> AdminUserID { get; set; }
        public string UpgradeType { get; set; }
    
        public virtual Order Order { get; set; }
        public virtual ICollection<ChangeRequestProject> ChangeRequestProjects { get; set; }
    }
}
