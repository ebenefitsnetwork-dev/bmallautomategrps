﻿using eBNQuickBaseLibrary;
using log4net;
using log4net.Appender;
using log4net.Repository.Hierarchy;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomateBultOrders
{
    public class LoadProp
    {
        public static ILog logger;
        public static DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0);//Unix Epoch on January 1st, 1970
        public static string logfilename;
        public static string logpath;

        public static string Environment = ConfigurationManager.AppSettings["environment"];
        public static eBNPortal_ProductionEntities ebnd = new eBNPortal_ProductionEntities();

        public static string username = ConfigurationManager.AppSettings["QBUserName"];
        public static string Password = ConfigurationManager.AppSettings["QBPassword"];


        public static QuickBaseConnection connection = new QuickBaseConnection("santeon.quickbase.com", username, Password, 48);

        #region Logging Functions
        static void LogString(string stringToLog)
        {
            logger.Info(stringToLog);
        }
        public static void InitializeLogger()
        {
            if (log4net.LogManager.GetCurrentLoggers().Length == 0)
            {
                log4net.Appender.RollingFileAppender fileLogRoot = new RollingFileAppender();

                log4net.GlobalContext.Properties["date"] = DateTime.Now;

                string path = AppDomain.CurrentDomain.BaseDirectory.ToString();
                string configFile = path + "App.config";


                log4net.Config.XmlConfigurator.Configure(new FileInfo(configFile));


            }
            logger = log4net.LogManager.GetLogger(typeof(Program));
            logger.Info("Begin processing.");
            var rootAppender = ((Hierarchy)LogManager.GetRepository())
                                         .Root.Appenders.OfType<FileAppender>()
                                         .FirstOrDefault();

            logpath = rootAppender != null ? rootAppender.File : string.Empty;
            logfilename = Path.GetFileName(logpath);
            Console.WriteLine(logpath);
        }

        #endregion
    }
}
