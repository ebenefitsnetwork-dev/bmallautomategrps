﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpreadsheetLight;
using System.Transactions;
using log4net;
using log4net.Appender;
using log4net.Repository.Hierarchy;
using System.IO;
using System.Data.Entity;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Text.RegularExpressions;
using System.Configuration;
using Intuit.QuickBase.Client;
using System.Data.Entity.Validation;
using System.Xml;
using eBNQuickBaseLibrary;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Data;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace AutomateBultOrders
{
    class Program
    {
        static ILog logger = LoadProp.logger;
        static eBNPortal_ProductionEntities ebnd = LoadProp.ebnd;

        static void Main(string[] args)
        {
            string updateMT = ConfigurationManager.AppSettings["updateMT"];
            string createqb = ConfigurationManager.AppSettings["createqb"];

            DateTime start;
            TimeSpan duration = new TimeSpan(1, 12, 23, 62);

            LoadProp.InitializeLogger();

            start = DateTime.Now;
            LoadProp.logger.Info(DateTime.Now);

            Console.WriteLine("Application starts...");
            LoadProp.logger.Info("Application starts");
            Console.WriteLine("Please wait while connecting to QB...");

            eBNOrder.AddOrderFromJson();
             //  eBNOrder.UpdateMTFromDB();


            duration = DateTime.Now - start;
            string durationformat = duration.ToString();
            LoadProp.logger.Info("Duration: " + duration);
            LoadProp.logger.Info("Duration: " + durationformat.Substring(0, durationformat.LastIndexOf(".")));
            LoadProp.logger.Info("Completed at: " + DateTime.Now);
            // Console.ReadKey();
        }
      

    }
}