﻿using log4net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SpreadsheetLight;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Transactions;
using System.Xml;

namespace AutomateBultOrders
{
    public class eBNOrder
    {
        static eBNPortal_ProductionEntities ebnd = LoadProp.ebnd;
        static ILog logger = LoadProp.logger;


        public static Order CreateOrder(long partnerId)
        {
            //using (eBNPortal_ProductionEntities ebnDB = new eBNPortal_ProductionEntities())
            //{
            Order order = new Order
            {
                OrderDate = DateTime.Now,
                PartnerID = partnerId,  //bm testting 10362, demo 10292
                IsSubmitted = true
            };
            long Max = ebnd.Orders.Max(C => C.OrderNumber);
            order.OrderNumber = Max + 1;
            order.StatusId = 2;
            order.OrderType = 1;
            order.SubmitDate = DateTime.Now;
            return order;
            //  }
        }
        public static Order_History CreateHistory(long orderid)
        {
            try
            {
                //using (eBNPortal_ProductionEntities ebnDB = new eBNPortal_ProductionEntities())
                //{
                // add order create event in order history table
                Order_History oh = new Order_History
                {
                    OrderID = orderid,
                    UserID = 51, //bassemadmin
                    EventTypeID = 1,
                    Event_Date = DateTime.Now
                };

                return oh;

                //  }
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null) logger.Error("Inner Exce=: " + ex.InnerException);
                logger.Error("Err mes=: " + ex.Message);
                logger.Error("Stack trace=: " + ex.StackTrace);
                return null;
            }

        }
        public static ChangeRequest AddChangeRequest(long orderid, int? chtype = null)
        {
            try
            {
                // add Change request for Create order
                ChangeRequest ChReqobj = new ChangeRequest
                {
                    OrderId = orderid,
                    ChRequestTypeId = chtype ?? 5,
                    ChRequestStatusId = 2,
                    ChRequestDate = DateTime.Now,
                    ChRequestSubmitDate = DateTime.Now,
                    AdminUserID = 51,

                    UserId = 51 //bassemadmin
                };

                return ChReqobj;
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null) logger.Error("Inner Exce=: " + ex.InnerException);
                logger.Error("Err mes=: " + ex.Message);
                logger.Error("Stack trace=: " + ex.StackTrace);
                return null;
            }
        }
        public static Customer GetCustomerinfo(SLDocument doc, int rowno, long orderid)
        {
            try
            {
                Customer Cs = new Customer();
                Cs.CustomerName = doc.GetCellValueAsString(rowno, 1);
                if (Cs.CustomerName == "" || Cs.CustomerName == null) return null;

                Cs.CustomerCode = doc.GetCellValueAsString(rowno, 7);
                ////Cs.Website = model.Website;
                Cs.PrimayAdd = doc.GetCellValueAsString(rowno, 12);
                Cs.PrimaryCity = doc.GetCellValueAsString(rowno, 13);
                string state = doc.GetCellValueAsString(rowno, 14);
                eBNPortal_ProductionEntities ebndb = new eBNPortal_ProductionEntities();
                Cs.stateID = ebndb.eBN_States.Where(s => s.State_Name == state).Select(s => s.StateID).FirstOrDefault();
                ebndb.Dispose();
                Cs.PrimaryCode = doc.GetCellValueAsString(rowno, 15);
                //Cs.PrimaryCountry = null;
                Cs.FTaxID = doc.GetCellValueAsString(rowno, 8);
                Cs.Companynumber = doc.GetCellValueAsInt32(rowno, 17);
                Cs.OrderID = orderid;
                Cs.contactCustomer = (doc.GetCellValueAsString(rowno, 18) == "Yes") ? true : false;

                #region customerbenefitinfo
                SLDocument sl = new SLDocument(@"D:\Bulkorderstemplate.xlsx", "Customer Benefit Information");
                Cs.ConnectionsNumber = sl.GetCellValueAsInt32(rowno, 2);
                Cs.PlanYearStartDate = sl.GetCellValueAsDateTime(rowno, 3, "YYYY MM DD");//Convert.ToDateTime(sl.GetCellValueAsString(rowno, 3));
                Cs.CustomerDataETA = sl.GetCellValueAsDateTime(rowno, 4, "YYYY MM DD");
                Cs.EmployeesNumber = sl.GetCellValueAsInt32(rowno, 5);
                Cs.OrderEmployeesNo = sl.GetCellValueAsInt32(rowno, 5);
                Cs.EnrollementMonth = sl.GetCellValueAsString(rowno, 6);
                sl.Dispose();
                #endregion



                return Cs;

            }
            catch (Exception ex)
            {
                if (ex.InnerException != null) logger.Error("Inner Exce=: " + ex.InnerException);
                logger.Error("Err mes=: " + ex.Message);
                logger.Error("Stack trace=: " + ex.StackTrace);
                return null;
            }

        }
        public static CustomerContact GetPartnerContacts(SLDocument doc, int rowno, long customerid)
        {

            try
            {
                CustomerContact PartnerContacts = new CustomerContact();

                PartnerContacts.CustomerContactName = doc.GetCellValueAsString(rowno, 2);
                string phone = doc.GetCellValueAsString(rowno, 4);
                PartnerContacts.CustomerContactPhone = "(" + phone.Substring(0, 3) + ") " + phone.Substring(3, 3) + "-" + phone.Substring(6);
                PartnerContacts.CustomerContactEmail = doc.GetCellValueAsString(rowno, 5);
                PartnerContacts.ContactTitle = doc.GetCellValueAsString(rowno, 3);
                PartnerContacts.IsPartner = true;
                PartnerContacts.CustomerID = customerid;

                return PartnerContacts;

            }
            catch (Exception ex)
            {
                if (ex.InnerException != null) logger.Error("Inner Exce=: " + ex.InnerException);
                logger.Error("Err mes=: " + ex.Message);
                logger.Error("Stack trace=: " + ex.StackTrace);
                return null;
            }

        }
        public static CustomerContact GetCustomerContacts(SLDocument doc, int rowno, long customerid)
        {

            try
            {
                CustomerContact CC = new CustomerContact();

                CC.CustomerContactName = doc.GetCellValueAsString(rowno, 9);
                string phone = doc.GetCellValueAsString(rowno, 10);
                CC.CustomerContactPhone = "(" + phone.Substring(0, 3) + ") " + phone.Substring(3, 3) + "-" + phone.Substring(6);
                CC.CustomerContactEmail = doc.GetCellValueAsString(rowno, 11);
                // CC.ContactTitle =   doc.GetCellValueAsString("C" + rowno);
                CC.IsPartner = false;
                CC.CustomerID = customerid;

                return CC;

            }
            catch (Exception ex)
            {
                if (ex.InnerException != null) logger.Error("Inner Exce=: " + ex.InnerException);
                logger.Error("Err mes=: " + ex.Message);
                logger.Error("Stack trace=: " + ex.StackTrace);
                return null;
            }

        }
        public static CustomerCompany GetCompanyfromCustomer(SLDocument doc, int rowno, long customerid)
        {
            try
            {
                CustomerCompany CC = new CustomerCompany();

                CC.CompName = doc.GetCellValueAsString(rowno, 1);
                CC.FTaxID = doc.GetCellValueAsString(rowno, 8);
                CC.Address = doc.GetCellValueAsString(rowno, 12);
                CC.City = doc.GetCellValueAsString(rowno, 13);
                eBNPortal_ProductionEntities ebndb = new eBNPortal_ProductionEntities();
                string state = doc.GetCellValueAsString(rowno, 14);
                CC.stateID = ebndb.eBN_States.Where(s => s.State_Name == state).Select(s => s.StateID).FirstOrDefault();
                ebndb.Dispose();
                CC.CustomerID = customerid;

                return CC;

            }
            catch (Exception ex)
            {
                if (ex.InnerException != null) logger.Error("Inner Exce=: " + ex.InnerException);
                logger.Error("Err mes=: " + ex.Message);
                logger.Error("Stack trace=: " + ex.StackTrace);
                return null;
            }

        }
        public static CustomerCompany GetCompanyH(SLDocument doc, int rowno, long customerid, int colindex)
        {
            try
            {

                CustomerCompany CC = new CustomerCompany();

                CC.CompName = doc.GetCellValueAsString(rowno, colindex);
                if (CC.CompName == null || CC.CompName == "") return null;
                colindex++;
                CC.FTaxID = doc.GetCellValueAsString(rowno, colindex);
                colindex++;
                CC.Address = doc.GetCellValueAsString(rowno, colindex);
                colindex++;
                CC.City = doc.GetCellValueAsString(rowno, colindex);
                colindex++;
                eBNPortal_ProductionEntities ebndb = new eBNPortal_ProductionEntities();
                string state = doc.GetCellValueAsString(rowno, colindex);
                CC.stateID = ebndb.eBN_States.Where(s => s.State_Name == state).Select(s => s.StateID).FirstOrDefault();
                ebndb.Dispose();
                CC.CustomerID = customerid;


                return CC;

            }
            catch (Exception ex)
            {
                if (ex.InnerException != null) logger.Error("Inner Exce=: " + ex.InnerException);
                logger.Error("Err mes=: " + ex.Message);
                logger.Error("Stack trace=: " + ex.StackTrace);
                return null;
            }


        }
        public static Carrier GetCarriers(SLDocument doc, int rowno, long orderid, int colindex)
        {
            rowno = rowno + 1;
            try
            {
                Carrier cr = new Carrier();

                cr.Conversion = doc.GetCellValueAsString(rowno, colindex) == "Yes" ? true : false;
                colindex++;
                cr.CarrierName = doc.GetCellValueAsString(rowno, colindex);
                if (cr.CarrierName == null || cr.CarrierName == "") return null;
                colindex++;
                cr.FedralTaxID = doc.GetCellValueAsString(rowno, colindex);
                colindex++;
                // vendorid column
                colindex++;
                cr.GroupNumber = doc.GetCellValueAsString(rowno, colindex);
                colindex++;
                cr.ContactName = doc.GetCellValueAsString(rowno, colindex);
                colindex++;
                string phone = doc.GetCellValueAsString(rowno, colindex);
                if (!string.IsNullOrEmpty(phone))
                {
                    if (phone.Length == 10)
                        cr.ContactPhone = "(" + phone.Substring(0, 3) + ") " + phone.Substring(3, 3) + "-" + phone.Substring(6);
                }
                colindex++;
                cr.ContactEmail = doc.GetCellValueAsString(rowno, colindex);
                colindex++;
                cr.SecContactName = doc.GetCellValueAsString(rowno, colindex);
                colindex++;
                string phone2 = doc.GetCellValueAsString(rowno, colindex);
                if (!string.IsNullOrEmpty(phone2))
                {
                    if (phone2.Length == 10)
                        cr.SecContactPhone = "(" + phone2.Substring(0, 3) + ") " + phone2.Substring(3, 3) + "-" + phone2.Substring(6);
                }
                colindex++;
                cr.SecContactEmail = doc.GetCellValueAsString(rowno, colindex);
                colindex++;

                cr.CobraMembers = doc.GetCellValueAsString(rowno, colindex) == "Yes" ? true : false;
                colindex++;
                cr.Allcomp = doc.GetCellValueAsString(rowno, colindex) == "Yes" ? true : false;

                cr.OrderID = orderid;
                cr.StatusId = 1;


                eBNPortal_ProductionEntities ebndb = new eBNPortal_ProductionEntities();
                cr.ProjectName = ebndb.Customers.Where(c => c.OrderID == orderid).Select(c => c.CustomerName).FirstOrDefault() + "_" + doc.GetCellValueAsString("C" + rowno);
                ebndb.Dispose();
                cr.QBCreated = false;

                return cr;

            }
            catch (Exception ex)
            {
                if (ex.InnerException != null) logger.Error("Inner Exce=: " + ex.InnerException);
                logger.Error("Err mes=: " + ex.Message);
                logger.Error("Stack trace=: " + ex.StackTrace);
                return null;
            }

        }
        public static List<CarrierPlaneType> GetCarrierPlans(SLDocument doc, int rowno, long carid, int colindex)
        {
            try
            {
                rowno = rowno + 1;
                List<CarrierPlaneType> carplanslist = new List<CarrierPlaneType>();
                using (eBNPortal_ProductionEntities ebndb = new eBNPortal_ProductionEntities())
                {
                    for (int colindxx = colindex; colindxx <= (colindex + 9); colindxx++)
                    {
                        string plannmae = doc.GetCellValueAsString(rowno, colindxx);
                        if (!string.IsNullOrEmpty(plannmae))//plannmae != null || plannmae != "")
                        {
                            CarrierPlaneType carplan1 = new CarrierPlaneType();
                            carplan1.carrierID = carid;
                            carplan1.PlanID = ebndb.PlanTypes.Where(p => p.PlanTypeName == plannmae).Select(p => p.PlanID).FirstOrDefault();
                            carplanslist.Add(carplan1);
                        }
                    }

                }
                return carplanslist;

            }
            catch (Exception ex)
            {
                if (ex.InnerException != null) logger.Error("Inner Exce=: " + ex.InnerException);
                logger.Error("Err mes=: " + ex.Message);
                logger.Error("Stack trace=: " + ex.StackTrace);
                return null;
            }

        }
        public static List<int> GetCarrierCompanies(SLDocument doc, int rowno, long carid, int colindex)
        {
            rowno = rowno + 1;
            try
            {
                List<int> carcomp = new List<int>();
                int compnumber = 1;
                for (int colindxx = colindex; colindxx <= (colindex + 6); colindxx++)
                {
                    if (doc.GetCellValueAsString(rowno, colindxx).ToString() == "Yes")
                    {
                        carcomp.Add(compnumber);
                    }
                    compnumber++;
                }
                return carcomp;

            }
            catch (Exception ex)
            {
                if (ex.InnerException != null) logger.Error("Inner Exce=: " + ex.InnerException);
                logger.Error("Err mes=: " + ex.Message);
                logger.Error("Stack trace=: " + ex.StackTrace);
                return null;
            }

        }
        public static OrderMappingContact GetMappingcontact(SLDocument doc, int rowno, long orderid)
        {
            try
            {
                OrderMappingContact OMC = new OrderMappingContact();

                OMC.OrderId = orderid;
                if (doc.GetCellValueAsString(rowno, 19).Contains("Partner"))
                {
                    OMC.ContactName = doc.GetCellValueAsString(rowno, 2);
                    OMC.ContactEmail = doc.GetCellValueAsString(rowno, 5);
                    string phone = doc.GetCellValueAsString(rowno, 4);
                    OMC.ContactPhone = "(" + phone.Substring(0, 3) + ") " + phone.Substring(3, 3) + "-" + phone.Substring(6);
                    OMC.ContactTitle = doc.GetCellValueAsString(rowno, 3);
                    OMC.ContactType = "Partner";
                }
                if (doc.GetCellValueAsString(rowno, 19).Contains("Customer"))
                {
                    OMC.ContactName = doc.GetCellValueAsString(rowno, 9);
                    OMC.ContactEmail = doc.GetCellValueAsString(rowno, 11);
                    string phone = doc.GetCellValueAsString(rowno, 10);
                    OMC.ContactPhone = "(" + phone.Substring(0, 3) + ") " + phone.Substring(3, 3) + "-" + phone.Substring(6);
                    OMC.ContactType = "Customer";
                }
                if (doc.GetCellValueAsString(rowno, 19).Contains("Other"))
                {
                    OMC.ContactName = doc.GetCellValueAsString(rowno, 20);
                    OMC.ContactEmail = doc.GetCellValueAsString(rowno, 21);
                    string phone = doc.GetCellValueAsString(rowno, 22);
                    OMC.ContactPhone = "(" + phone.Substring(0, 3) + ") " + phone.Substring(3, 3) + "-" + phone.Substring(6);
                    OMC.ContactTitle = doc.GetCellValueAsString(rowno, 23);

                    OMC.ContactType = "Other";
                }


                return OMC;

            }
            catch (Exception ex)
            {
                if (ex.InnerException != null) logger.Error("Inner Exce=: " + ex.InnerException);
                logger.Error("Err mes=: " + ex.Message);
                logger.Error("Stack trace=: " + ex.StackTrace);
                return null;
            }
        }


        public static void AddOrderFromJsonFile2()
        {

            var oFS = File.OpenText(@"D:\syncgrps.txt");
            //var oFS = File.OpenText(@"D:\bmall\Prod-GroupsAndCarriers.txt");

            string josn = oFS.ReadToEnd();
            //deserialize JSON from file  


            dynamic items = JsonConvert.DeserializeObject(josn);



            int allCount = ((Newtonsoft.Json.Linq.JObject)items)["Groups"].Count();
            var GrpsProp = ((Newtonsoft.Json.Linq.JObject)items)["Groups"].Where(g => !g["oldvalueid"].ToString().ToUpper().Contains("CA")).ToList();
            var CarProp = ((Newtonsoft.Json.Linq.JObject)items)["Carriers"];
            var allCars = CarProp.GroupBy(s => s["CarrierCode"]).ToList();
            List<string> customerCodes = CarProp.Select(c => c["GroupNumber"].ToString().Trim()).Distinct().ToList();

            logger.Info("All Customers count: " + allCount);
            logger.Info("Customers Without Companies count: " + GrpsProp.Count());
            logger.Info("All Carriers in sheet: " + CarProp.Count());
            int newcustcount = 0;

            var cobraGrps = ((Newtonsoft.Json.Linq.JObject)items)["Groups"].Where(g => g["oldvalueid"].ToString().ToUpper().Contains("CA")).ToList();
            #region
            foreach (var cobraGrpItem in cobraGrps)
            {
                string cobraGrp = cobraGrpItem["CustomerCode"].ToString();
                string mainCustCode = cobraGrpItem["oldvalueid"].ToString();
                mainCustCode = mainCustCode.Substring(2, mainCustCode.Length - 2);
            }
            #endregion


            #region 
            foreach (string custCode in customerCodes)
            {
                long orderId = 0;
                List<long> CarIds = new List<long>();

                var grpItem = GrpsProp.Where(c => c["CustomerCode"].ToString().Trim() == custCode).FirstOrDefault();

                bool newCustomer = false;
                if (grpItem == null)
                {
                    logger.Error("Not Exist: This customer code does not exist in the valid code list " + custCode);
                    continue;
                }
                string custName = grpItem["CustomerName"].ToString();
                string allCompId = grpItem["oldvalueid"].ToString();
                logger.Info("custCode");

                using (TransactionScope ts = new TransactionScope())
                {
                    using (ebnd = new eBNPortal_ProductionEntities())
                    {
                        try
                        {
                            bool newcustomer = false;
                            Customer cs = ebnd.Customers.FirstOrDefault(c => c.CustomerCode == custCode && c.Order.PartnerID == 10291);
                            Order order;
                            ChangeRequest CH;
                            if (cs != null)
                            {
                                newcustomer = false;
                                logger.Info("customer already exist: " + custName + " , customercode: " + custCode);
                                order = ebnd.Orders.Where(o => o.OrderID == cs.OrderID).FirstOrDefault();
                                CH = AddChangeRequest(order.OrderID, 3);
                                ebnd.ChangeRequests.Add(CH);
                                ebnd.SaveChanges();
                                // for now
                                //continue;
                            }
                            else
                            {
                                newcustomer = true;
                                logger.Info("New Customer: " + custName);
                                order = CreateOrder(10291);
                                ebnd.Orders.Add(order);
                                ebnd.SaveChanges();

                                if (order != null)
                                {
                                    Order_History oh = CreateHistory(order.OrderID);
                                    ebnd.Order_History.Add(oh);
                                    ebnd.SaveChanges();

                                    CH = AddChangeRequest(order.OrderID);
                                    ebnd.ChangeRequests.Add(CH);
                                    ebnd.SaveChanges();
                                    #region customer
                                    cs = new Customer();
                                    cs.CustomerName = custName;

                                    JToken fTax = grpItem["FedralTaxID"];

                                    if (fTax != null)
                                        cs.FTaxID = fTax.ToString();

                                    cs.CustomerCode = custCode;
                                    cs.EmployeesNumber = Convert.ToInt32(grpItem["EmployeesNumber"]);//.ToString();
                                    cs.OrderEmployeesNo = Convert.ToInt32(grpItem["EmployeesNumber"]);
                                    cs.OrderID = order.OrderID;
                                    ebnd.Customers.Add(cs);
                                    ebnd.SaveChanges();

                                    //                        //custom Field Group ID
                                    OrderCustomField Oc = new OrderCustomField();
                                    Oc.OrderID = order.OrderID;
                                    Oc.CutomfieldID = 98;  //testing 106 , demo 98
                                    Oc.CustomFieldValue = custCode;
                                    ebnd.OrderCustomFields.Add(Oc);
                                    ebnd.SaveChanges();

                                    //Partner contacts
                                    CustomerContact pcs = new CustomerContact();
                                    pcs.CustomerID = cs.CustomerID;
                                    pcs.IsPartner = true;

                                    JToken fNToken = grpItem["firstname"];
                                    JToken lNToken = grpItem["lastname"];
                                    JToken eToken = grpItem["email"];

                                    string fulName = string.Empty;
                                    fulName = fNToken != null ? lNToken != null ? fNToken.ToString() + " " + lNToken.ToString()
                                              : (fNToken != null ? fNToken.ToString() : lNToken.ToString()) : string.Empty;


                                    pcs.CustomerContactName = fulName;

                                    if (eToken != null)
                                        pcs.CustomerContactEmail = eToken.ToString();

                                    ebnd.CustomerContacts.Add(pcs);
                                    ebnd.SaveChanges();
                                    #endregion

                                    #region companies
                                    //check if  custcompany exist b4 
                                    CustomerCompany custComp = ebnd.CustomerCompanies.FirstOrDefault(c => c.CustomerID == cs.CustomerID && c.CompName == custName);
                                    if (custComp != null)
                                    {
                                        custComp = new CustomerCompany();
                                        custComp.CustomerID = cs.CustomerID;
                                        custComp.CompName = custName;

                                        ebnd.CustomerCompanies.Add(custComp);
                                        ebnd.SaveChanges();
                                    }
                                    var customercomp = GrpsProp.Where(c => c["oldvalueid"].ToString().ToUpper() == "CA" + custCode).ToList();
                                    foreach (var childcomp in customercomp)
                                    {
                                        string childcompName = childcomp["CustomerName"].ToString();
                                        CustomerCompany custCompany = ebnd.CustomerCompanies.FirstOrDefault(c => c.CustomerID == cs.CustomerID && c.CompName == childcompName);
                                        if (custCompany != null)
                                        {
                                            custCompany = new CustomerCompany();
                                            custComp.CustomerID = cs.CustomerID;
                                            custComp.CompName = childcompName;
                                            ebnd.CustomerCompanies.Add(custCompany);
                                            ebnd.SaveChanges();

                                            Oc.CustomFieldValue += "," + childcomp["CustomerCode"].ToString();
                                        }
                                    }

                                }
                                #endregion

                            }
                            #region carriers
                            int savedcarcount = 0;
                            var custCars = CarProp.Where(c => c["GroupNumber"].ToString() == custCode)
                                .GroupBy(c => c["CarrierCode"].ToString().Trim())
                                .ToList();

                            foreach (var carItem in custCars)
                            {
                                try
                                {
                                    string carName = carItem.First()["CarrierName"].ToString().Trim();
                                    string vendorId = carItem.First()["CarrierCode"].ToString().Trim();
                                    // Carrier car = null;

                                    //   if (newcustomer)
                                    Carrier car = ebnd.Carriers.Where(c => c.CarrierName.Trim() == carName.Trim() && c.OrderID == cs.OrderID).FirstOrDefault();

                                    if (car != null)
                                    {
                                        logger.Info("Connection already exist " + car.ProjectName);
                                        // ts.Dispose();
                                        continue;
                                    }

                                    eBNCarrier eBNcar = ebnd.eBNCarriers.FirstOrDefault(c => c.CarrierName == carName);
                                    if (eBNcar == null)
                                    {
                                        eBNcar = new eBNCarrier();
                                        eBNcar.CarrierName = carName;
                                        ebnd.eBNCarriers.Add(eBNcar);
                                        ebnd.SaveChanges();
                                    }

                                    #region carrier
                                    car = new Carrier();
                                    car.CarrierName = carName.Trim();

                                    car.ProjectName = cs.CustomerName.Trim() + "_" + car.CarrierName.Trim();
                                    car.OrderID = cs.OrderID;
                                    car.Allcomp = true;

                                    try
                                    {
                                        car.FedralTaxID = carItem.First()["FedralTaxID"].ToString().Trim();
                                    }
                                    catch (Exception exc)
                                    {
                                        logger.Error("error adding car ftax, ignored for car: " + carName);
                                    }
                                    car.StatusId = 1;
                                    car.MultitenantStatus = false;
                                    car.eBNCarrierId = eBNcar.CarrierId;

                                    ebnd.Carriers.Add(car);
                                    ebnd.SaveChanges();
                                    car.CarrierCode = car.CarrierID.ToString();
                                    ebnd.SaveChanges();
                                    #endregion
                                    #region carrierpltypes

                                    string[] plansArray = carItem.Select(c => c["CarrierPlans"].ToString().Trim()).ToArray();
                                    if (plansArray.Count() > 0)
                                        foreach (var planchild in plansArray)
                                        {
                                            try
                                            {
                                                int planid = ebnd.PlanTypes.Where(p => p.PlanTypeName == planchild).Select(p => p.PlanID).FirstOrDefault();
                                                if (planid != 0)
                                                {
                                                    CarrierPlaneType carplans = new CarrierPlaneType();
                                                    carplans.PlanID = planid;
                                                    carplans.carrierID = car.CarrierID;
                                                    ebnd.CarrierPlaneTypes.Add(carplans);
                                                    ebnd.SaveChanges();
                                                }

                                            }
                                            catch (Exception ex)
                                            {
                                                logger.Error("Error adding plan types for Order: " + custName);
                                                if (ex.InnerException != null) logger.Error("Inner Exce=: " + ex.InnerException);
                                                logger.Error("Err mes=: " + ex.Message);
                                                logger.Error("End exception details");
                                            }
                                        }
                                    #endregion


                                    #region carriercustomfields //vendor id
                                    OrderCustomField octvendor = new OrderCustomField();
                                    octvendor.CarrierID = car.CarrierID;
                                    octvendor.OrderID = car.OrderID;
                                    octvendor.CustomFieldValue = vendorId;
                                    octvendor.CutomfieldID = 99;  //107 testing , 99 demo
                                    ebnd.OrderCustomFields.Add(octvendor);
                                    ebnd.SaveChanges();
                                    #endregion

                                    #region carrier com
                                    List<CustomerCompany> custcomp = ebnd.CustomerCompanies.Where(c => c.CustomerID == cs.CustomerID).ToList();
                                    foreach (CustomerCompany comp in custcomp)
                                    {
                                        CompCarrier compcarrier = new CompCarrier();
                                        compcarrier.CarrierID = car.CarrierID;
                                        compcarrier.CompID = comp.CompID;
                                        ebnd.CompCarriers.Add(compcarrier);
                                        ebnd.SaveChanges();
                                    }
                                    #endregion


                                    ebnd.SaveChanges();
                                    savedcarcount++;
                                    logger.Info("Connection added : " + car.ProjectName);
                                    CarIds.Add(car.CarrierID);

                                }
                                catch (DbEntityValidationException e)
                                {

                                    logger.Error("Error: adding Order for Customer: " + custName);

                                    foreach (var eve in e.EntityValidationErrors)
                                    {
                                        logger.Error(string.Format("Error: Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                               eve.Entry.Entity.GetType().Name, eve.Entry.State));
                                        foreach (var ve in eve.ValidationErrors)
                                        {
                                            logger.Error(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                               ve.PropertyName, ve.ErrorMessage));
                                        }
                                    }
                                    // ts.Dispose();
                                }
                                catch (Exception ex)
                                {
                                    logger.Error("Error: adding Order : " + custName);
                                    if (ex.InnerException != null) logger.Error("Inner Exce=: " + ex.InnerException);
                                    logger.Error("Err mes=: " + ex.Message);
                                    logger.Error("Stack trace=: " + ex.StackTrace);
                                    //   ts.Dispose();
                                }
                            }

                            ebnd.SaveChanges();
                            cs.ConnectionsNumber = ebnd.Carriers.Where(c => c.OrderID == cs.OrderID && c.StatusId == 1).Count();
                            ebnd.SaveChanges();

                            logger.Error("no. of saved carriers: " + savedcarcount);
                            if (newcustomer && savedcarcount > 0)
                            {
                                ts.Complete();
                                logger.Info("New Customer: successfuly saved new customer: " + custName + " , custcode: " + custCode);
                                newcustcount++;
                            }
                            else if ((!newcustomer) && savedcarcount > 0)
                            {
                                ts.Complete();
                                logger.Info("Add Connection(s): successfuly saved new connection(s): " + custName + " , custcode: " + custCode);
                                newcustcount++;
                            }
                            // ts.Complete();
                            else
                            {
                                logger.Error("Error: order was not saved for customer: " + custName);
                                ts.Dispose();
                            }
                            #endregion

                            orderId = order.OrderID;

                        }
                        catch (Exception ex)
                        {
                            logger.Error("Error: adding Order : " + custName);
                            if (ex.InnerException != null) logger.Error("Inner Exce=: " + ex.InnerException);
                            logger.Error("Err mes=: " + ex.Message);
                            logger.Error("Stack trace=: " + ex.StackTrace);
                            ts.Dispose();
                        }
                    }

                }

                #region QB
                //foreach (var carId in CarIds)
                //{
                //    try
                //    {

                //        bool QBcreated = QBase.AddNewProject_NewLib(orderId, carId);
                //        if (QBcreated)
                //            logger.Info("successfully added in QB: orderId: " + orderId + " , for carrier ID: " + carId);
                //        else
                //            logger.Info("Error adding in QB: orderId: " + orderId + " , for carrier ID: " + carId);
                //    }

                //    catch (Exception ex)
                //    {
                //        logger.Error("Error: adding in QB: orderId: " + orderId + " , for carrier ID: " + carId);
                //        if (ex.InnerException != null) logger.Error("Inner Exce=: " + ex.InnerException);
                //        logger.Error("Err mes=: " + ex.Message);
                //        logger.Error("Stack trace=: " + ex.StackTrace);
                //        //   ts.Dispose();
                //    }
                //}
                #endregion

            }
            logger.Info("new Customers = " + newcustcount);

            #endregion




        }

        public static void AddOrderFromJsonFile()
        {

            // FileStream oFS = new FileStream(@"D:\bmall\Prod-GroupsAndCarriers.txt", FileMode.Open, FileAccess.Read);
            var oFS = File.OpenText(@"D:\Bmall-syncjob\Test-GroupsAndCarriers.json");
            //var oFS = File.OpenText(@"D:\bmall\Prod-GroupsAndCarriers.txt");

            string josn = oFS.ReadToEnd();
            //deserialize JSON from file  


            dynamic items = JsonConvert.DeserializeObject(josn);

            var custcodes = "300079,300296,300354,300532,300537,301355,302230,302286,303004,303054,303057,303263,303348,303350,303441,303613,303680,303731,303751,303886,303935,303953,304029,304047,304154,304303,304357,304363,304381,304506,304583,304588,304620,304657,304675,304753,304772,304837,304886,304948,304967,305050,305058,305215,305216,305236,305237,305372,305430,305443,305494,305586,305617,305768,305829,305858,305870,305894,306085,306097,306119,306231,306288,306402,306403,306483,306560,306619,306753,306785,306886,306913,306946,307001,307148,307166,307202,307212,307214,307215,307233,307235,307431,307445,307446,307470,307489,307515,307556,307559,307566,307619,307690,307693,307699,307718,307806,307909,307931,307939,307960,307993,308009,308179,308244,308252,308257,308271,308322,308349,308351,308362,308416,308433,308460,308666,308759,308943,308996,309093,309143,309160,309226,309253,309258,309281,309300,309314,309316,309353,309386,309404,309431,309432,309496,309498,309508,309522,309556,309566,309573,309597,309681,309761,309925,309983,310107,310224,310236,310280,310289,310328,310372,310526,310744,310866,310898,310913,311235,311273,311310,311369,311607,311617,311894,312320,312457,312531,312534,312549,312583,312735,312768,312873,312877,312895,312993,313044,313080,313135,313237,313320,313330,313483,313536,313540,313564,313570,313575,313635,313672,313715,313788,313820,313829,313929,313952,314228,314263,314375,314428,314450,314470,314620,314692,314840,314845,314907,314913,314962,315063,315067,315094,315096,315200,315300,315313,315375,315437,315457,315468,315542,315559,315566,315567,315579,315598,315790,315851,315879,315944,315984,316010,316056,316061,316068,316085,316088,316094,316099,316168,316169,316229,316354,316374,316377,316389,316391,316407,316432,316471,316472,316488,316505,316514,316555,316572,316580,316616,316638,316663,316664,316670,316729,316747,316754,316775,316778,316780,316799,316805,316807,316817,316818,316819,316846,316847,316860,316865,316881,316904,316932,316943,316996,317000,317007,317051,317052,317068,317087,317210,317211,317212,317213,317214,317261,317280,317293,317297,317300,317302,317340,317346,317365,317410,317431,317432,317439,317440,317490,317491,317518,317581,317585,317604,317608,317697,317750,317751,317755,317830,317889,317928,317930,317961,317963,317994,318024,318043,318046,318069,318096,318108,318210,318215,318238,318242,318247,318264,318265,318267,318268,318320,318321,318326,318327,318349,318356,318357,318358,318359,318368,318381,318390,318402,318414,318417,318435,318447,318448,318451,318452,318453,318465,318474,318493,318503,318517,318521,318525,318537,318540,318546,318548,318549,318550,318554,318560,318566,318568,318583,318590,318594,318595,318602,318611,318612,318616,318620,318621,318623,318640,318645,318676,318699,318707,318708,318709,318713,318714,318724,318727,318728,318733,318740,318747,318756,318761,318765,318766,318767,318770,318787,318815,318823,318864,318870,318872,318873,318875,318877,318880,318889,318895,318907,318908,318909,318916,318922,318988,318989,318990,319000,319011,319023,319029,319050,319051,319057,319061,319066,319110,319130,319136,319149,319153,319159,319169,319189,319191,319236,319238,319245,319246,319256,319288,319323,319324,319325,319330,319335,319339,319341,319366,319367,319393,319401,319406,319407,319425,319434,319455,319460,319478,319479,319494,319495,319527,319529,319546,319554,319555,319583,319584,319585,319608,319611,319612,319614,319640,319666,319674,319710,319716,319737,319743,319766,319768,319780,319782,319799,319800,319801,319802,319803,319804,319805,319807,319808,319810,319811,319812,319816,319817,319818,319821,319830,319831,319832,319833,319834,319835,319836,319838,319840,319841,319842,319843,319844,319845,319846,319847,319848,319849,319850,319851,319852,319853,319854,319855,319856,319857,319858,319860,319861,319862,319863,319864,319865,319866,319867,319868,319869,319872,319873,319875,319876,319879,319881,319882,319883,319884,319885,319887,319888,319890,319891,319892,319893,319894,319896,319897,319898,319899,319900,319901,319902,319903,319904,319906,319907,319908,319909,319910,319911,319912,319913,319915,319916,319917,319918,319919,319921,319922,319923,319924,319925,319926,319927,319929,319930,319932,319933,319934,319935,319936,319937,319938,319939,319940,319941,319943,319944,319946,319948,319949,319950,319951,319952,319953,319955,319957,319963,319964,319966,319967,319968,319969,319970,319971,319972,319974,319975,319976,319977,319978,319979,319981,319982,319983,319984,319988,319989,319990,320000,320001,320013,320043,320054,320056,320074,320087,320095,320096,320097,320098,320099,320101,320103,320104,320105,320106,320111,320177,320190,320191,320194,320195,320196,320198,320199,320200,320202,320206,320272,320277,320278,320281,320285,320286,320287,320288,320306,320325,320328,320329,320333,320337,320354,320358,320372,320373,320374,320376,320380,320382,320390,320391,320394,320429,320470,320513,320514,320515,320519,320520,320533,320542,320562,320563,320570,320579,320607,320635,320636,320638,320639,320640,320641,320644,320645,320674,320676,320677,320695,320696,320740,320773,320788,320794,320823,320868,320880,320903,320909,320959,320961,320963,320964,320968,320972,320975,320993,320994,320996,321019,321028,321029,321030,321031,321032,321036,321038,321070,321085,321086,321088,321095,321096,321098,321131,321132,321137,321140,321143,321144,321145,321147,321150,321152,321156,321161,321162,321174,321180,321187,321189,321192,321196,321212,321219,321220,321226,321239,321240,321254,321262,321311,321313,321316,321317,321320,321321,321338,321368,321382,321390,321394,321396,321397,321398,321400,321401,321402,321404,321405,321406,321408,321456,321498,321522,321525,321529,321534,321537,321543,321547,321548,321549,321550,321552,321553,321554,321584,321586,321588,321604,321618,321625,321641,321643,321644,321646,321647,321651,321676,321677,321680,321683,321709,321723,321751,321752,321753,321755,321756,321766,321792,321801,321803,321814,321847,321857,321863,321864,321866,321867,321868,321880,321882,321883,321895,321896,321900,321916,321929,321931,321934,321948,321954,321959,321968,321970,321979,321984,321997,322003,322005,322056,322058,322059,322060,322062,322063,322064,322067,322074,322081,322094,322095,322163,322217,322219,322223,322224,322225,322226,322228,322229,322236,322237,322238,322239,322241,322242,322243,322244,322245,322247,322301,322309,322326,322327,322329,322345,322352,322353,322412,322437,322452,322453,322456,322457,322459,322484,322490,322531,322558,322570,322584,322703,322706,322728,322729,322738,322780,322792,322794,322795,322813,322815,322817,322821,322825,322826,322828,322830,322833,322835,322836,322837,322838,322839,322843,322844,322877,322887,322896,322909,322913,322925,322935,322936,322982,322994,323007,323009,323010,323012,323016,323018,323019,323020,323021,323022,323024,323025,323027,323028,323033,323046,323069,323076,323084,323127,323128,323129,323132,323134,323143,323144,323154,323164,323193,323203,323204,323205,323209,323212,323213,323216,323217,323227,323244,323254,323263,323265,323269,323270,323280,323288,323289,323301,323302,323305,323317,323320,323325,323329,323354,323359,323382,323383,323384,323385,323386,323387,323388,323394,323403,323414,323419,323420,323422,323429,323431,323447,323449,323452,323465,323466,323467,323468,323481,323488,323489,323493,323494,323495,323496,323498,323500,323508,323510,323512,323531,323549,323555,323556,323558,323559,323560,323587,323589,323607,323622,323626,323630,323637,323638,323639,323640,323641,323642,323643,323644,323645,323648,323649,323650,323658,323668,323676,323680,323682,323687,323693,323696,323698,323699,323700,323701,323702,323704,323707,323708,323709,323712,323715,323716,323718,323719,323724,323740,323745,323751,323752,323754,323755,323757,323759,323760,323762,323763,323764,323765,323766,323767,323770,323771,323772,323773,323775,323778,323779,323780,323781,323782,323783,323784,323785,323786,323788,323789,323790,323791,323792,323793,323794,323795,323796,323799,323802,323803,323804,323805,323807,323808,323809,323811,323812,323816,323817,323820,323823,323824,323826,323827,323828,323830,323831,323832,323833,323834,323835,323836,323838,323839,323840,323841,323842,323843,323846,323847,323848,323850,323851,323852,323853,323854,323856,323857,323859,323860,323862,323863,323864,323867,323869,323870,323871,323872,323876,323877,323880,323883,323884,323885,323886,323888,323889,323891,323892,323893,323894,323895,323898,323899,323900,323901,323902,323903,323904,323907,323908,323909,323910,323911,323912,323914,323915,323916,323919,323920,323921,323922,323924,323925,323926,323928,323929,323930,323931,323932,323933,323934,323935,323936,323937,323938,323939,323940,323941,323942,323944,323945,323946,323947,323948,323949,323950,323951,323952,323953,323954,323955,323956,323957,323958,323959,323960,323961,323962,323963,323964,323966,323967,323968,323969,323971,323973,323975,323976,323978,323979,323980,323981,323982,323983,323984,323985,323986,323987,323988,323991,323992,323994,323996,323997,323998,323999,324000,324003,324005,324006,324008,324009,324010,324011,324012,324013,324015,324017,324018,324019,324020,324021,324023,324025,324026,324027,324028,324029,324031,324032,324033,324034,324035,324037,324039,324040,324041,324043,324044,324045,324048,324049,324051,324052,324053,324054,324055,324056,324057,324058,324059,324060,324061,324062,324063,324064,324065,324066,324067,324069,324070,324071,324072,324074,324075,324076,324077,324078,324079,324081,324082,324083,324085,324087,324088,324089,324090,324091,324092,324093,324095,324096,324097,324098,324100,324101,324103,324105,324107,324108,324109,324111,324112,324114,324115,324116,324118,324119,324120,324122,324123,324124,324125,324126,324127,324128,324129,324130,324131,324132,324134,324135,324136,324137,324138,324139,324140,324141,324142,324143,324145,324146,324147,324150,324151,324152,324153,324154,324155,324156,324157,324158,324160,324161,324162,324166,324167,324169,324170,324171,324172,324174,324175,324176,324177,324178,324179,324182,324183,324184,324185,324186,324187,324191,324192,324195,324196,324197,324198,324199,324200,324202,324203,324204,324205,324207,324208,324211,324212,324213,324216,324218,324219,324220,324222,324224,324225,324226,324227,324228,324229,324230,324232,324233,324234,324235,324236,324237,324239,324243,324245,324247,324248,324249,324250,324251,324252,324253,324254,324255,324256,324257,324258,324259,324260,324261,324263,324264,324265,324266,324268,324270,324272,324274,324276,324281,324285,324287,324288,324289,324291,324292,324293,324294,324295,324296,324297,324302,324303,324304,324305,324306,324307,324308,324309,324310,324311,324312,324313,324314,324319,324322,324323,324324,324325,324326,324327,324328,324329,324330,324331,324332,324333,324335,324337,324338,324341,324342,324343,324344,324345,324346,324347,324348,324349,324352,324353,324354,324355,324356,324357,324358,324359,324360,324362,324372,324374,324375,324376,324377,324378,324379,324380,324381,324384,324386,324387,324388,324389,324390,324391,324392,324393,324394,324395,324396,324397,324398,324400,324401,324402,324403,324404,324405,324406,324407,324408,324410,324411,324412,324413,324414,324415,324416,324417,324419,324420,324421,324422,324423,324425,324426,324427,324428,324433,324434,324435,324437,324440,324446,324448,324449,324451,324452,324453,324454,324455,324457,324459,324460,324461,324463,324464,324465,324470,324471,324472,324473,324474,324475,324476,324477,324478,324479,324480,324481,324482,324483,324484,324488,324489,324490,324491,324493,324495,324496,324497,324498,324499,324501,324502,324503,324504,324505,324506,324507,324508,324509,5191,6581,8654,967";
            var custArray = custcodes.Split(',');



            int allCount = ((Newtonsoft.Json.Linq.JObject)items)["Groups"].Count();
            var GrpsProp = ((Newtonsoft.Json.Linq.JObject)items)["Groups"].Where(g => !g["oldvalueid"].ToString().ToUpper().Contains("CA")).ToList();

            //  GrpsProp = GrpsProp.Where(g => custArray.Contains(g["CustomerCode"].ToString())).ToList();
            GrpsProp = GrpsProp.Where(g => custArray.Any(c => c == g["CustomerCode"].ToString())).ToList();


            var termGrps = ((Newtonsoft.Json.Linq.JObject)items)["Groups"].Where(g => g["GroupTermDate"] != null).ToList();


            var CarPropall = ((Newtonsoft.Json.Linq.JObject)items)["Carriers"];

            var CarProp = ((Newtonsoft.Json.Linq.JObject)items)["Carriers"].Where(g => g["HSAAdminCarrier"] == null && g["COBRAExternalEntity"] == null).ToList();
            // var cobraCarProp = ((Newtonsoft.Json.Linq.JObject)items)["Carriers"].Where(g => g["COBRAExternalEntity"] != null).ToList();
            var hSACobraCarProp = ((Newtonsoft.Json.Linq.JObject)items)["Carriers"].Where(g => g["COBRAExternalEntity"] != null && g["COBRAExternalEntity"].ToString() == "1242").ToList();//(g => g["HSAAdminCarrier"] != null || g["COBRAExternalEntity"] != null).ToList();


            logger.Info("All Customers count: " + allCount);
            logger.Info("Customers Without Companies count: " + GrpsProp.Count());
            logger.Info("All Carriers in sheet: " + CarProp.Count());
            int newcustcount = 0;
            #region new customers
            foreach (var grpItem in GrpsProp)
            {
                string custCode = grpItem["CustomerCode"].ToString();


                if (!custArray.Any(ca => ca == custCode.Trim()))
                    continue;

                string custName = grpItem["CustomerName"].ToString();
                string allCompId = grpItem["oldvalueid"].ToString();
                logger.Info("custCode");

                JToken grpTerma = grpItem["GroupTermDate"];
                bool cancelledGrp = false;
                if (grpTerma != null)
                {
                    DateTime termDate = Convert.ToDateTime(grpTerma.ToString());

                    if (termDate.Date <= DateTime.Now.Date) cancelledGrp = true;
                }

                using (TransactionScope ts = new TransactionScope())
                {
                    using (ebnd = new eBNPortal_ProductionEntities())
                    {
                        try
                        {
                            bool newcustomer = false;
                            Customer cs = ebnd.Customers.FirstOrDefault(c => c.CustomerCode == custCode && c.Order.PartnerID == 10291);
                            Order order;
                            ChangeRequest CH;
                            if (cs != null)
                            {
                                newcustomer = false;
                                logger.Info("customer already exist: " + custName + " , customercode: " + custCode);
                                order = ebnd.Orders.Where(o => o.OrderID == cs.OrderID).FirstOrDefault();
                                // CH = addChangeReequest(order.OrderID, 4);
                                // for now
                                //continue;

                                #region check cancelled grp
                                JToken grpTerm = grpItem["GroupTermDate"];
                                if (grpTerm != null)// && cs.Order.StatusId == 2)
                                {
                                    CancelOrder(cs.OrderID, Convert.ToDateTime(grpTerm.ToString().Substring(grpTerm.ToString().LastIndexOf("T"))));
                                    continue;
                                }
                                #endregion


                            }
                            else
                            {
                                #region check cancelled grp
                                JToken grpTerm = grpItem["GroupTermDate"];
                                if (grpTerm != null)// && cs.Order.StatusId == 2)
                                {
                                    DateTime x = Convert.ToDateTime(grpTerm.ToString());
                                    continue;
                                }
                                #endregion
                            }

                            #region carriers
                            int savedcarcount = 0;
                            var custCars = CarProp.Where(c => c["GroupNumber"].ToString() == custCode)
                                .GroupBy(c => c["CarrierCode"].ToString().Trim())
                                .ToList();

                            foreach (var carItem in custCars)
                            {
                                try
                                {
                                    string carName = carItem.First()["CarrierName"].ToString().Trim();
                                    string vendorId = carItem.First()["CarrierCode"].ToString().Trim();
                                    // Carrier car = null;

                                    //   if (newcustomer)
                                    Carrier car = ebnd.Carriers.Where(c => c.CarrierName.Trim() == carName.Trim() && c.OrderID == cs.OrderID).FirstOrDefault();

                                    if (car != null)
                                    {
                                        logger.Info("Connection already exist " + car.ProjectName);
                                        ts.Dispose();
                                        continue;
                                    }

                                    eBNCarrier eBNcar = ebnd.eBNCarriers.FirstOrDefault(c => c.CarrierName == carName);
                                    if (eBNcar == null)
                                    {
                                        eBNcar = new eBNCarrier();
                                        eBNcar.CarrierName = carName;
                                        ebnd.eBNCarriers.Add(eBNcar);
                                        ebnd.SaveChanges();
                                    }

                                    #region carrier
                                    car = new Carrier();
                                    car.CarrierName = carName.Trim();

                                    car.ProjectName = cs.CustomerName.Trim() + "_" + car.CarrierName.Trim();
                                    car.OrderID = cs.OrderID;
                                    car.Allcomp = true;

                                    try
                                    {
                                        car.FedralTaxID = carItem.First()["FedralTaxID"].ToString().Trim();
                                    }
                                    catch (Exception exc)
                                    {
                                        logger.Error("error adding car ftax, ignored for car: " + carName);
                                    }
                                    car.StatusId = 1;
                                    car.MultitenantStatus = false;
                                    car.eBNCarrierId = eBNcar.CarrierId;

                                    ebnd.Carriers.Add(car);
                                    ebnd.SaveChanges();
                                    car.CarrierCode = car.CarrierID.ToString();
                                    ebnd.SaveChanges();
                                    #endregion
                                    #region carrierpltypes

                                    string[] plansArray = carItem.Select(c => c["CarrierPlans"].ToString().Trim()).ToArray();
                                    if (plansArray.Count() > 0)
                                        foreach (var planchild in plansArray)
                                        {
                                            try
                                            {
                                                int planid = ebnd.PlanTypes.Where(p => p.PlanTypeName == planchild).Select(p => p.PlanID).FirstOrDefault();
                                                if (planid != 0)
                                                {
                                                    CarrierPlaneType carplans = new CarrierPlaneType();
                                                    carplans.PlanID = planid;
                                                    carplans.carrierID = car.CarrierID;
                                                    ebnd.CarrierPlaneTypes.Add(carplans);
                                                    ebnd.SaveChanges();
                                                }

                                            }
                                            catch (Exception ex)
                                            {
                                                logger.Error("Error adding plan types for Order: " + custName);
                                                if (ex.InnerException != null) logger.Error("Inner Exce=: " + ex.InnerException);
                                                logger.Error("Err mes=: " + ex.Message);
                                                logger.Error("End exception details");
                                            }
                                        }
                                    //else
                                    //{
                                    //    int planid = ebnd.PlanTypes.Where(p => p.PlanTypeName == carrierPlans).Select(p => p.PlanID).FirstOrDefault();
                                    //    if (planid != 0)
                                    //    {
                                    //        CarrierPlaneType carplans = new CarrierPlaneType();
                                    //        carplans.PlanID = planid;
                                    //        carplans.carrierID = car.CarrierID;
                                    //        ebnd.CarrierPlaneTypes.Add(carplans);
                                    //        ebnd.SaveChanges();

                                    //    }

                                    //}
                                    #endregion


                                    #region carriercustomfields //vendor id
                                    OrderCustomField octvendor = new OrderCustomField();
                                    octvendor.CarrierID = car.CarrierID;
                                    octvendor.OrderID = car.OrderID;
                                    octvendor.CustomFieldValue = vendorId;
                                    octvendor.CutomfieldID = 99;  //107 testing , 99 demo
                                    ebnd.OrderCustomFields.Add(octvendor);
                                    ebnd.SaveChanges();
                                    #endregion

                                    #region carrier com
                                    List<CustomerCompany> custcomp = ebnd.CustomerCompanies.Where(c => c.CustomerID == cs.CustomerID).ToList();
                                    foreach (CustomerCompany comp in custcomp)
                                    {
                                        CompCarrier compcarrier = new CompCarrier();
                                        compcarrier.CarrierID = car.CarrierID;
                                        compcarrier.CompID = comp.CompID;
                                        ebnd.CompCarriers.Add(compcarrier);
                                        ebnd.SaveChanges();
                                    }
                                    #endregion


                                    ebnd.SaveChanges();
                                    savedcarcount++;
                                    logger.Info("Connection added : " + car.ProjectName);

                                }
                                catch (DbEntityValidationException e)
                                {

                                    logger.Error("Error: adding Order for Customer: " + custName);

                                    foreach (var eve in e.EntityValidationErrors)
                                    {
                                        logger.Error(string.Format("Error: Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                               eve.Entry.Entity.GetType().Name, eve.Entry.State));
                                        foreach (var ve in eve.ValidationErrors)
                                        {
                                            logger.Error(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                               ve.PropertyName, ve.ErrorMessage));
                                        }
                                    }
                                    ts.Dispose();
                                }
                                catch (Exception ex)
                                {
                                    logger.Error("Error: adding Order : " + custName);
                                    if (ex.InnerException != null) logger.Error("Inner Exce=: " + ex.InnerException);
                                    logger.Error("Err mes=: " + ex.Message);
                                    logger.Error("Stack trace=: " + ex.StackTrace);
                                    ts.Dispose();
                                }
                            }

                            ebnd.SaveChanges();
                            cs.ConnectionsNumber = ebnd.Carriers.Where(c => c.OrderID == cs.OrderID && c.StatusId == 1).Count();
                            ebnd.SaveChanges();

                            logger.Error("no. of saved carriers: " + savedcarcount);
                            if (savedcarcount > 0)
                            {
                                ts.Complete();
                                logger.Error("successfuly saved new customer: " + custName + " , custcode: " + custCode);
                                newcustcount++;
                            }
                            // ts.Complete();
                            else
                            {
                                logger.Error("order was not saved");
                                ts.Dispose();
                                continue;
                            }
                            #endregion

                        }
                        catch (Exception ex)
                        {
                            logger.Error("Error: adding Order : " + custName);
                            if (ex.InnerException != null) logger.Error("Inner Exce=: " + ex.InnerException);
                            logger.Error("Err mes=: " + ex.Message);
                            logger.Error("Stack trace=: " + ex.StackTrace);
                            ts.Dispose();
                        }
                    }

                }
            }
            logger.Info("new Customers = " + newcustcount);

            #endregion

            logger.Info("Adding connections");

            #region Add connection
            var CarGrp1 = CarProp.GroupBy(c => c["GroupNumber"].ToString().Trim()).ToList();

            var Cars = CarProp
                .GroupBy(s => s["GroupNumber"], (carriercode) => new
                {
                    carrierCode = carriercode.GroupBy(s => s["CarrierCode"])
                });

            var cars2 = CarProp.GroupBy(s => s["GroupNumber"]).Select(s => s.GroupBy(d => d["CarrierCode"])).ToList();

            foreach (var CarGrp in cars2)
            {
                foreach (var carItem in CarGrp)
                {
                    string custCode = carItem.First()["GroupNumber"].ToString();
                    string carName = carItem.First()["CarrierName"].ToString().Trim();

                    using (TransactionScope ts = new TransactionScope())
                    {
                        using (ebnd = new eBNPortal_ProductionEntities())
                        {
                            try
                            {
                                Customer cs = ebnd.Customers.FirstOrDefault(c => c.CustomerCode == custCode && c.Order.PartnerID == 10291);
                                Order order;
                                ChangeRequest CH;
                                if (cs != null)
                                {
                                    logger.Info("customer already exist  customercode: " + custCode);


                                    order = ebnd.Orders.Where(o => o.OrderID == cs.OrderID).FirstOrDefault();
                                    CH = AddChangeRequest(order.OrderID, 3);

                                    ebnd.ChangeRequests.Add(CH);
                                    ebnd.SaveChanges();
                                    // for now
                                    //continue;


                                    try
                                    {
                                        string vendorId = carItem.First()["CarrierCode"].ToString().Trim();
                                        // Carrier car = null;

                                        //   if (newcustomer)
                                        Carrier car = ebnd.Carriers.Where(c => c.CarrierName.Trim() == carName.Trim() && c.OrderID == cs.OrderID).FirstOrDefault();

                                        if (car != null)
                                        {
                                            logger.Info("Connection already exist " + car.ProjectName);
                                            ts.Dispose();
                                            continue;
                                        }

                                        eBNCarrier eBNcar = ebnd.eBNCarriers.FirstOrDefault(c => c.CarrierName == carName);
                                        if (eBNcar == null)
                                        {
                                            eBNcar = new eBNCarrier();
                                            eBNcar.CarrierName = carName;
                                            ebnd.eBNCarriers.Add(eBNcar);
                                            ebnd.SaveChanges();
                                        }

                                        #region carrier
                                        logger.Info("Connection New " + custCode);

                                        car = new Carrier();
                                        car.CarrierName = carName.Trim();

                                        car.ProjectName = cs.CustomerName.Trim() + "_" + car.CarrierName.Trim();
                                        car.OrderID = cs.OrderID;
                                        car.Allcomp = true;

                                        try
                                        {
                                            car.FedralTaxID = carItem.First()["FedralTaxID"].ToString().Trim();
                                        }
                                        catch (Exception exc)
                                        {
                                            logger.Error("error adding car ftax, ignored for car: " + carName);
                                        }
                                        car.StatusId = 1;
                                        car.MultitenantStatus = false;
                                        car.eBNCarrierId = eBNcar.CarrierId;

                                        ebnd.Carriers.Add(car);
                                        ebnd.SaveChanges();
                                        car.CarrierCode = car.CarrierID.ToString();
                                        ebnd.SaveChanges();
                                        #endregion
                                        #region carrierpltypes

                                        string[] plansArray = carItem.Select(c => c["CarrierPlans"].ToString().Trim()).ToArray();
                                        if (plansArray.Count() > 0)
                                        {
                                            logger.Info("plans count for customer: " + cs.CustomerName + " = " + plansArray.Count());
                                            foreach (var planchild in plansArray)
                                            {
                                                try
                                                {
                                                    int planid = ebnd.PlanTypes.Where(p => p.PlanTypeName == planchild).Select(p => p.PlanID).FirstOrDefault();
                                                    if (planid != 0)
                                                    {
                                                        logger.Info("valid plan for customer: " + cs.CustomerName);
                                                        CarrierPlaneType carplans = new CarrierPlaneType();
                                                        carplans.PlanID = planid;
                                                        carplans.carrierID = car.CarrierID;
                                                        ebnd.CarrierPlaneTypes.Add(carplans);
                                                        ebnd.SaveChanges();
                                                    }

                                                }
                                                catch (Exception ex)
                                                {
                                                    logger.Error("Error adding plan types for Order: " + cs.CustomerName);
                                                    if (ex.InnerException != null) logger.Error("Inner Exce=: " + ex.InnerException);
                                                    logger.Error("Err mes=: " + ex.Message);
                                                    logger.Error("End exception details");

                                                }
                                            }
                                        }
                                        #endregion


                                        #region carriercustomfields //vendor id
                                        OrderCustomField octvendor = new OrderCustomField();
                                        octvendor.CarrierID = car.CarrierID;
                                        octvendor.OrderID = car.OrderID;
                                        octvendor.CustomFieldValue = vendorId;
                                        octvendor.CutomfieldID = 99;  //107 testing , 99 demo
                                        ebnd.OrderCustomFields.Add(octvendor);
                                        ebnd.SaveChanges();
                                        #endregion

                                        #region carrier com
                                        List<CustomerCompany> custcomp = ebnd.CustomerCompanies.Where(c => c.CustomerID == cs.CustomerID).ToList();
                                        foreach (CustomerCompany comp in custcomp)
                                        {
                                            CompCarrier compcarrier = new CompCarrier();
                                            compcarrier.CarrierID = car.CarrierID;
                                            compcarrier.CompID = comp.CompID;
                                            ebnd.CompCarriers.Add(compcarrier);
                                            ebnd.SaveChanges();
                                        }
                                        #endregion


                                        ebnd.SaveChanges();
                                        ts.Complete();
                                        logger.Info("Connection added : " + car.ProjectName);

                                    }
                                    catch (DbEntityValidationException e)
                                    {

                                        logger.Error("Error: adding Order for Customer: " + cs.CustomerName);

                                        foreach (var eve in e.EntityValidationErrors)
                                        {
                                            logger.Error(string.Format("Error: Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                                   eve.Entry.Entity.GetType().Name, eve.Entry.State));
                                            foreach (var ve in eve.ValidationErrors)
                                            {
                                                logger.Error(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                                   ve.PropertyName, ve.ErrorMessage));
                                            }
                                        }
                                        ts.Dispose();
                                    }
                                    catch (Exception ex)
                                    {
                                        logger.Error("Error: adding Order : " + cs.CustomerName);
                                        if (ex.InnerException != null) logger.Error("Inner Exce=: " + ex.InnerException);
                                        logger.Error("Err mes=: " + ex.Message);
                                        logger.Error("Stack trace=: " + ex.StackTrace);
                                        ts.Dispose();
                                    }
                                }
                                else
                                {
                                    logger.Info("customer already added customercode: " + custCode);
                                    ts.Dispose();
                                    continue;

                                }
                            }
                            catch (Exception ex)
                            {
                                logger.Error("Error: adding Connection  : " + carName + " , cust code: " + custCode);
                                if (ex.InnerException != null) logger.Error("Inner Exce=: " + ex.InnerException);
                                logger.Error("Err mes=: " + ex.Message);
                                logger.Error("Stack trace=: " + ex.StackTrace);
                                ts.Dispose();

                            }

                        }
                    }
                    //   ts.Complete();
                }
            }
            #endregion

        }
        public static void AddOrderFromJson()
        {
            logger.Info("Start Adding Groups");

            JObject items = GetGroupsAndCarriersJson();
            int allCount = ((Newtonsoft.Json.Linq.JObject)items)["Groups"].Count();
            var GrpsProp = ((Newtonsoft.Json.Linq.JObject)items)["Groups"].Where(g => !g["oldvalueid"].ToString().ToUpper().Contains("CA")).ToList();
            var cobraGrps = ((Newtonsoft.Json.Linq.JObject)items)["Groups"].Where(g => g["oldvalueid"].ToString().ToUpper().Contains("CA")).ToList();
            var termGrps = ((Newtonsoft.Json.Linq.JObject)items)["Groups"].Where(g => g["GroupTermDate"] != null).ToList();


            var CarProp = ((Newtonsoft.Json.Linq.JObject)items)["Carriers"].Where(g => g["HSAAdminCarrier"] == null && g["COBRAExternalEntity"] == null).ToList();
            var hSACobraCarProp = new List<JToken>();// ((Newtonsoft.Json.Linq.JObject)items)["Carriers"].Where(g => g["HSAAdminCarrier"] != null).ToList();
            var cobraCarProp = new List<JToken>();//  ((Newtonsoft.Json.Linq.JObject)items)["Carriers"].Where(g => g["COBRAExternalEntity"] != null && g["COBRAExternalEntity"].ToString() == "1242").ToList();//(g => g["HSAAdminCarrier"] != null || g["COBRAExternalEntity"] != null).ToList();
            var allCars = CarProp.GroupBy(s => s["CarrierCode"]).ToList();

            List<string> customerCodes = CarProp.Select(c => c["GroupNumber"].ToString().Trim()).Distinct().ToList();

            logger.Info("All Customers count: " + allCount);
            logger.Info("Customers Without Companies count: " + GrpsProp.Count());
            logger.Info("All Carriers in sheet: " + CarProp.Count());
            int newcustcount = 0;


          //  Dictionary<long, BMCarrierCodes_View> abc = new Dictionary<long, BMCarrierCodes_View>();
           // List<string> eBNCarrierPlans = new List<string>();
            Dictionary<string, string> mCCDic = new Dictionary<string, string>();
            SLDocument mCCDoc = new SLDocument(@"C:\eBNServer\Applications\Web\bulkOrder\MCarrierCodes.xlsx");
            SLWorksheetStatistics mCCst = mCCDoc.GetWorksheetStatistics();

            int mrowno = mCCst.NumberOfRows;

            for (int i = 1; i <= mrowno; i++)
            {
                mCCDic.Add(mCCDoc.GetCellValueAsString(i, 1).Trim().ToLower(), mCCDoc.GetCellValueAsString(i, 2).Trim().ToLower());
            }

            using (eBNPortal_ProductionEntities ebnDB = new eBNPortal_ProductionEntities())
            {
            //    abc = ebnDB.BMCarrierCodes_View.Select(cc => new { id = cc.CarrierId, bmcc = cc })
              //      .ToDictionary(d => d.id, d => d.bmcc);

              //  eBNCarrierPlans = ebnDB.PlanTypes.Select(c => c.PlanTypeName.Trim().ToLower()).ToList();
            }


            #region 
            foreach (string custCode in customerCodes)
            {

                var grpItem = GrpsProp.Where(c => c["CustomerCode"].ToString().Trim() == custCode).FirstOrDefault();

                if (grpItem == null)
                {
                    logger.Error("Not Exist: This customer code does not exist in the valid code list " + custCode);
                    continue;
                }

                string custName = grpItem["CustomerName"].ToString();
                string allCompId = grpItem["oldvalueid"].ToString();
                long orderId = 0;
                List<long> CarIds = new List<long>();
                string bMallEnv = ConfigurationManager.AppSettings["bMallEnv"];
                bool updatedCustomer = false;
                long customerID = 0;
                logger.Info("custCode");

                using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required,
                                                                         new TransactionOptions()
                                                                         {
                                                                             IsolationLevel = IsolationLevel.ReadUncommitted
                                                                         }))
                {
                    using (ebnd = new eBNPortal_ProductionEntities())
                    {
                        try
                        {
                            bool newcustomer = false;
                            Customer cs = ebnd.Customers.FirstOrDefault(c => c.CustomerCode == custCode && c.Order.PartnerID == 10291);
                            Order order;
                            ChangeRequest CH = null;
                            if (cs != null)
                            {
                                newcustomer = false;
                                logger.Info("customer already exist: " + custName + " , customercode: " + custCode);

                                if (cs.CustomerName.Trim().ToLower() != custName.Trim().ToLower()) //update customername
                                {
                                    customerID = cs.CustomerID;
                                    updatedCustomer = true;
                                    //updatre customer name in QB, vip and in qbcarrierphases table, need to create a new method for this update
                                }

                                order = ebnd.Orders.Where(o => o.OrderID == cs.OrderID).FirstOrDefault();
                                CH = AddChangeRequest(order.OrderID, 3);
                                ebnd.ChangeRequests.Add(CH);
                                ebnd.SaveChanges();
                                // for now
                                //continue;

                            }
                            else
                            {
                                newcustomer = true;
                                logger.Info("New Customer: " + custName);
                                order = CreateOrder(10291);
                                ebnd.Orders.Add(order);
                                ebnd.SaveChanges();

                                if (order != null)
                                {
                                    Order_History oh = CreateHistory(order.OrderID);
                                    ebnd.Order_History.Add(oh);
                                    ebnd.SaveChanges();

                                    CH = AddChangeRequest(order.OrderID);
                                    ebnd.ChangeRequests.Add(CH);
                                    ebnd.SaveChanges();
                                    #region customer
                                    cs = new Customer();
                                    cs.CustomerName = custName;

                                    JToken fTax = grpItem["FedralTaxID"];

                                    if (fTax != null)
                                        cs.FTaxID = fTax.ToString();

                                    cs.CustomerCode = custCode;
                                    cs.EmployeesNumber = Convert.ToInt32(grpItem["EmployeesNumber"]);//.ToString();
                                    cs.OrderEmployeesNo = Convert.ToInt32(grpItem["EmployeesNumber"]);
                                    cs.OrderID = order.OrderID;
                                    ebnd.Customers.Add(cs);
                                    ebnd.SaveChanges();

                                    //                        //custom Field Group ID
                                    OrderCustomField Oc = new OrderCustomField();
                                    Oc.OrderID = order.OrderID;
                                    Oc.CutomfieldID = 98;  //testing 106 , demo 98
                                    Oc.CustomFieldValue = custCode;
                                    ebnd.OrderCustomFields.Add(Oc);
                                    ebnd.SaveChanges();

                                    //Partner contacts
                                    CustomerContact pcs = new CustomerContact();
                                    pcs.CustomerID = cs.CustomerID;
                                    pcs.IsPartner = false;

                                    JToken fNToken = grpItem["firstname"];
                                    JToken lNToken = grpItem["lastname"];
                                    JToken eToken = grpItem["email"];

                                    string fulName = string.Empty;
                                    fulName = fNToken != null ? lNToken != null ? fNToken.ToString() + " " + lNToken.ToString()
                                              : (fNToken != null ? fNToken.ToString() : lNToken.ToString()) : string.Empty;


                                    pcs.CustomerContactName = fulName;

                                    if (eToken != null)
                                        pcs.CustomerContactEmail = ConfigurationManager.AppSettings["BMallsupportmail"].ToString();// eToken.ToString();

                                    ebnd.CustomerContacts.Add(pcs);
                                    ebnd.SaveChanges();
                                    #endregion

                                    #region companies
                                    //check if  custcompany exist b4 
                                    CustomerCompany custComp = ebnd.CustomerCompanies.FirstOrDefault(c => c.CustomerID == cs.CustomerID && c.CompName == custName);
                                    if (custComp == null)
                                    {
                                        custComp = new CustomerCompany();
                                        custComp.CustomerID = cs.CustomerID;
                                        custComp.CompName = custName;

                                        ebnd.CustomerCompanies.Add(custComp);
                                        ebnd.SaveChanges();
                                    }
                                    var customercomp = cobraGrps.Where(c => c["oldvalueid"].ToString().ToUpper() == "CA" + custCode).ToList();
                                    foreach (var childcomp in customercomp)
                                    {
                                        string childcompName = childcomp["CustomerName"].ToString();
                                        CustomerCompany custCompany = ebnd.CustomerCompanies.FirstOrDefault(c => c.CustomerID == cs.CustomerID && c.CompName == childcompName);
                                        if (custCompany != null)
                                        {
                                            custCompany = new CustomerCompany();
                                            custComp.CustomerID = cs.CustomerID;
                                            custComp.CompName = childcompName;
                                            ebnd.CustomerCompanies.Add(custCompany);
                                            ebnd.SaveChanges();

                                            Oc.CustomFieldValue += "," + childcomp["CustomerCode"].ToString();
                                        }
                                    }

                                }
                                #endregion

                            }
                            #region carriers
                            #region main carriers
                            int savedcarcount = 0;
                            var custCars = CarProp.Where(c => c["GroupNumber"].ToString() == custCode)
                                .GroupBy(c => c["CarrierCode"].ToString().Trim())
                                .ToList();

                            foreach (var carItem in custCars)
                            {
                                bool mCC = false;
                                string mCarrierCode = string.Empty;
                                string mCarrierCodeGrp = string.Empty;
                                string carPlan = carItem.First()["CarrierCode"].ToString().ToLower().Trim();
                                string carName = carItem.First()["CarrierName"].ToString().Trim();
                                string vendorId = carItem.First()["CarrierCode"].ToString().Trim();

                                try
                                {
                                    #region check mcc
                                    if (mCCDic.ContainsKey(carItem.First()["CarrierCode"].ToString().ToLower().Trim()))
                                    {
                                        mCC = true;
                                        mCarrierCode = vendorId;//carItem.First()["CarrierCode"].ToString().Trim();
                                        mCarrierCodeGrp = mCCDic[mCarrierCode.ToLower()];
                                        var mCarrierCodeArr = mCarrierCodeGrp.Split('|');
                                        string planName = carItem.First()["CarrierPlans"].ToString().Trim();

                                        long carierId = ebnd.BMCarrierCodes_View.ToList().Where(c => c.CustomerCode == custCode &&
                                            ((c.CarrierCode.Contains("|") && c.CarrierCode.Split('|').Any(cc => mCarrierCodeArr.Contains(cc.ToLower()))) || mCarrierCodeArr.Contains(c.CarrierCode.ToLower())))
                                            .Select(c => c.CarrierId).FirstOrDefault();
                                        if (carierId != 0) // just add the new carriercode
                                        {
                                            OrderCustomField carVendor = ebnd.OrderCustomFields.Where(c => c.CarrierID == carierId && c.CutomfieldID == 99).FirstOrDefault();
                                            if (carVendor.CustomFieldValue.Contains('|'))
                                            {
                                                var carVendorArr = carVendor.CustomFieldValue.Split('|');
                                                if (!carVendorArr.Contains(vendorId))
                                                {
                                                    carVendor.CustomFieldValue = carVendor.CustomFieldValue + "|" + mCarrierCode;

                                                    logger.Info(string.Format("Adding MCC only for carrierid: {0}, carrir code value: {1}", carierId, mCarrierCode));
                                                    ebnd.SaveChanges();
                                                }
                                            }
                                            else if (carVendor.CustomFieldValue.ToLower() != vendorId.ToLower())
                                            {
                                                carVendor.CustomFieldValue = carVendor.CustomFieldValue + "|" + mCarrierCode;

                                                logger.Info(string.Format("Adding MCC only for carrierid: {0}, carrir code value: {1}", carierId, mCarrierCode));
                                                ebnd.SaveChanges();
                                            }

                                            #region carrierpltypes

                                            string[] mPlansArray = carItem.Select(c => c["CarrierPlans"].ToString().Trim()).ToArray();
                                            if (mPlansArray.Count() > 0)
                                                foreach (var planchild in mPlansArray)
                                                {
                                                    try
                                                    {
                                                        int mplanid = ebnd.PlanTypes.Where(p => p.PlanTypeName == planchild).Select(p => p.PlanID).FirstOrDefault();
                                                        if (mplanid != 0)
                                                        {
                                                            CarrierPlaneType carplans = ebnd.CarrierPlaneTypes.Where(c => c.carrierID == carierId && c.PlanID == mplanid).FirstOrDefault();

                                                            if (carplans == null)
                                                            {
                                                                carplans = new CarrierPlaneType();
                                                                carplans.PlanID = mplanid;
                                                                carplans.carrierID = carierId;
                                                                ebnd.CarrierPlaneTypes.Add(carplans);
                                                                ebnd.SaveChanges();
                                                                logger.Info(string.Format("Adding PT only for carrierid: {0}, carrir code value: {1}", carierId, mCarrierCode));

                                                            }
                                                        }

                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        logger.Error("Error adding plan types for Order: " + custName);
                                                        if (ex.InnerException != null) logger.Error("Inner Exce=: " + ex.InnerException);
                                                        logger.Error("Err mes=: " + ex.Message);
                                                        logger.Error("End exception details");
                                                    }
                                                }
                                            #endregion

                                            continue;
                                        }
                                    }
                                    #endregion
                                    // Carrier car = null;
                                    //   if (newcustomer)
                                    // need to check if a MT already exist for the same carrier code , do not create a new single connection.
                                    //get carrier VenodrId
                                    BMCarrierCodes_View OC = ebnd.BMCarrierCodes_View.Where(c => c.CustomerCode == cs.CustomerCode).ToList().Where(c => (c.CarrierCode.Trim().ToLower() == vendorId || (c.CarrierCode.Contains("|") && c.CarrierCode.Split('|').Any(v => v.ToLower() == vendorId.ToLower())))).FirstOrDefault();

                                    Carrier car = ebnd.Carriers.Where(c => c.CarrierName.Trim() == carName.Trim() && c.OrderID == cs.OrderID).FirstOrDefault();

                                    if (car != null || OC != null)
                                    {
                                        logger.Info("Connection already exist " + car.ProjectName);
                                        // ts.Dispose();
                                        continue;
                                    }

                                    eBNCarrier eBNcar = ebnd.eBNCarriers.FirstOrDefault(c => c.CarrierName == carName);
                                    if (eBNcar == null)
                                    {
                                        eBNcar = new eBNCarrier();
                                        eBNcar.CarrierName = carName;
                                        ebnd.eBNCarriers.Add(eBNcar);
                                        ebnd.SaveChanges();
                                    }

                                    #region carrier
                                    car = new Carrier();
                                    car.CarrierName = carName.Trim();

                                    car.ProjectName = cs.CustomerName.Trim() + "_" + car.CarrierName.Trim();
                                    car.OrderID = cs.OrderID;
                                    car.Allcomp = true;
                                    car.SubmitDate = DateTime.Now.Date;
                                    try
                                    {
                                        car.FedralTaxID = carItem.First()["FedralTaxID"].ToString().Trim();
                                    }
                                    catch (Exception exc)
                                    {
                                        logger.Error("error adding car ftax, ignored for car: " + carName);
                                    }
                                    car.StatusId = 1;
                                    car.MultitenantStatus = false;
                                    car.eBNCarrierId = eBNcar.CarrierId;

                                    ebnd.Carriers.Add(car);
                                    ebnd.SaveChanges();
                                    car.CarrierCode = car.CarrierID.ToString();
                                    ebnd.SaveChanges();
                                    #endregion
                                    #region carrierpltypes

                                    string[] plansArray = carItem.Select(c => c["CarrierPlans"].ToString().Trim()).ToArray();
                                    if (plansArray.Count() > 0)
                                        foreach (var planchild in plansArray)
                                        {
                                            try
                                            {
                                                int planid = ebnd.PlanTypes.Where(p => p.PlanTypeName == planchild).Select(p => p.PlanID).FirstOrDefault();
                                                if (planid != 0)
                                                {
                                                    CarrierPlaneType carplans = new CarrierPlaneType();
                                                    carplans.PlanID = planid;
                                                    carplans.carrierID = car.CarrierID;
                                                    ebnd.CarrierPlaneTypes.Add(carplans);
                                                    ebnd.SaveChanges();
                                                }
                                                else //other plan type
                                                {
                                                    //check if other was added before 
                                                    CarrierPlaneType carplans = ebnd.CarrierPlaneTypes.Where(c => c.carrierID == car.CarrierID && c.PlanID == 10).FirstOrDefault();
                                                    if (carplans == null)
                                                    {
                                                        carplans.PlanID = 10;  //otherplan type
                                                        carplans.carrierID = car.CarrierID;
                                                        ebnd.CarrierPlaneTypes.Add(carplans);
                                                        ebnd.SaveChanges();
                                                        logger.Info(string.Format("Adding PT only for carrierid: {0}, carrir code value: {1}", car.CarrierID, mCarrierCode));
                                                    }
                                                    if (string.IsNullOrEmpty(car.CarrierType))
                                                       car.CarrierType = planchild;
                                                    else
                                                        car.CarrierType += ","+planchild;

                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                logger.Error("Error adding plan types for Order: " + custName);
                                                if (ex.InnerException != null) logger.Error("Inner Exce=: " + ex.InnerException);
                                                logger.Error("Err mes=: " + ex.Message);
                                                logger.Error("End exception details");
                                            }
                                        }
                                    #endregion


                                    #region carriercustomfields //vendor id
                                    OrderCustomField octvendor = new OrderCustomField();
                                    octvendor.CarrierID = car.CarrierID;
                                    octvendor.OrderID = car.OrderID;
                                    octvendor.CustomFieldValue = vendorId;
                                    octvendor.CutomfieldID = 99;  //107 testing , 99 demo
                                    ebnd.OrderCustomFields.Add(octvendor);
                                    ebnd.SaveChanges();
                                    #endregion

                                    #region carrier com
                                    List<CustomerCompany> custcomp = ebnd.CustomerCompanies.Where(c => c.CustomerID == cs.CustomerID).ToList();
                                    foreach (CustomerCompany comp in custcomp)
                                    {
                                        CompCarrier compcarrier = new CompCarrier();
                                        compcarrier.CarrierID = car.CarrierID;
                                        compcarrier.CompID = comp.CompID;
                                        ebnd.CompCarriers.Add(compcarrier);
                                        ebnd.SaveChanges();
                                    }
                                    #endregion


                                    #region chreqproj
                                    if (!newcustomer)
                                    {
                                        ChangeRequestProject CRP = new ChangeRequestProject();

                                        CRP.ChRequestId = CH.ID;
                                        CRP.ProjectId = car.CarrierID;
                                        ebnd.ChangeRequestProjects.Add(CRP);
                                        ebnd.SaveChanges();
                                    }
                                    #endregion


                                    ebnd.SaveChanges();
                                    savedcarcount++;
                                    logger.Info("Connection added : " + car.ProjectName);
                                    CarIds.Add(car.CarrierID);

                                }
                                catch (DbEntityValidationException e)
                                {

                                    logger.Error("Error: adding Order for Customer: " + custName);

                                    foreach (var eve in e.EntityValidationErrors)
                                    {
                                        logger.Error(string.Format("Error: Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                               eve.Entry.Entity.GetType().Name, eve.Entry.State));
                                        foreach (var ve in eve.ValidationErrors)
                                        {
                                            logger.Error(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                               ve.PropertyName, ve.ErrorMessage));
                                        }
                                    }
                                    // ts.Dispose();
                                }
                                catch (Exception ex)
                                {
                                    logger.Error("Error: adding Order : " + custName);
                                    if (ex.InnerException != null) logger.Error("Inner Exce=: " + ex.InnerException);
                                    logger.Error("Err mes=: " + ex.Message);
                                    logger.Error("Stack trace=: " + ex.StackTrace);
                                    //   ts.Dispose();
                                }
                            }
                            #endregion

                            #region Cobra ExternalEntity
                           // var cobraCustCars = hSACobraCarProp.Where(c => c["GroupNumber"].ToString() == custCode)
                           //.GroupBy(c => c["COBRAExternalEntity"].ToString().Trim())
                           //.ToList();
                           // logger.Info(string.Format("cobracount for customer {0} {1}",  custCode , cobraCustCars.Count()));
                           // foreach (var carItem in cobraCustCars)
                           // {

                           //     logger.Info("cobra: " );

                           //     string carPlan = carItem.First()["CarrierCode"].ToString().ToLower().Trim();
                           //     string cobraexternalEntity = carItem.First()["COBRAExternalEntity"].ToString().Trim();
                           //     string carName = "Cobra-" + cobraexternalEntity; //carItem.First()["CarrierName"].ToString().Trim();
                           //     string vendorId = carItem.First()["CarrierCode"].ToString().Trim();

                           //     try
                           //     {

                           //         long carierId =  ebnd.BMCarrierCodes_View.Where(c => c.CustomerCode == cs.CustomerCode && c.CarrierName.Trim().ToLower() == carName.ToLower()).ToList().Where(c => (c.CarrierCode.Trim().ToLower() == vendorId || (c.CarrierCode.Contains("|") && c.CarrierCode.Split('|').Any(v => v.ToLower() == vendorId.ToLower())))).Select(c=>c.CarrierId).FirstOrDefault();

                           //         if (carierId != 0) // just add the new carriercode
                           //         {
                           //             logger.Info("Connection already exist: "+ custName+"_"+ carName);
                           //             OrderCustomField carVendor = ebnd.OrderCustomFields.Where(c => c.CarrierID == carierId && c.CutomfieldID == 99).FirstOrDefault();
                           //             if (carVendor.CustomFieldValue.Contains('|'))
                           //             {
                           //                 var carVendorArr = carVendor.CustomFieldValue.Split('|');
                           //                 if (!carVendorArr.Contains(vendorId))
                           //                 {
                           //                     carVendor.CustomFieldValue = carVendor.CustomFieldValue + "|" + vendorId;

                           //                     logger.Info(string.Format("Adding MCC only for cobra carrierid: {0}, carrir code value: {1}", carierId, vendorId));
                           //                     ebnd.SaveChanges();
                           //                 }
                           //             }
                           //             else if (carVendor.CustomFieldValue.ToLower() != vendorId.ToLower())
                           //             {
                           //                 carVendor.CustomFieldValue = carVendor.CustomFieldValue + "|" + vendorId;

                           //                 logger.Info(string.Format("Adding MCC only for carrierid: {0}, carrir code value: {1}", carierId, vendorId));
                           //                 ebnd.SaveChanges();
                           //             }
                           //             else
                           //                 logger.Info(string.Format("cobra already exist:", custName + "_" + carName));


                           //             // ts.Dispose();
                           //             continue;
                           //         }
                                   

                           //         eBNCarrier eBNcar = ebnd.eBNCarriers.FirstOrDefault(c => c.CarrierName == "CobraAdmin");
                           //         if (eBNcar == null)
                           //         {
                           //             eBNcar = new eBNCarrier();
                           //             eBNcar.CarrierName = "CobraAdmin";
                           //             ebnd.eBNCarriers.Add(eBNcar);
                           //             ebnd.SaveChanges();
                           //         }

                           //         #region carrier
                           //         Carrier car = new Carrier();
                           //         car.CarrierName = carName.Trim();

                           //         car.ProjectName = custName.Trim() + "_" + car.CarrierName.Trim();
                           //         car.OrderID = cs.OrderID;
                           //         car.Allcomp = true;
                           //         car.SubmitDate = DateTime.Now.Date;
                           //         try
                           //         {
                           //             car.FedralTaxID = carItem.First()["FedralTaxID"].ToString().Trim();
                           //         }
                           //         catch (Exception exc)
                           //         {
                           //             logger.Error("error adding car ftax, ignored for car: " + carName);
                           //         }
                           //         car.StatusId = 1;
                           //         car.MultitenantStatus = false;
                           //         car.eBNCarrierId = eBNcar.CarrierId;

                           //         ebnd.Carriers.Add(car);
                           //         ebnd.SaveChanges();
                           //         car.CarrierCode = car.CarrierID.ToString();
                           //         ebnd.SaveChanges();
                           //         #endregion
                           //         #region carrierpltypes

                           //         string[] plansArray = carItem.Select(c => c["CarrierPlans"].ToString().Trim()).ToArray();
                           //         if (plansArray.Count() > 0)
                           //             foreach (var planchild in plansArray)
                           //             {
                           //                 try
                           //                 {
                           //                     int planid = ebnd.PlanTypes.Where(p => p.PlanTypeName == planchild).Select(p => p.PlanID).FirstOrDefault();
                           //                     if (planid != 0)
                           //                     {
                           //                         CarrierPlaneType carplans = new CarrierPlaneType();
                           //                         carplans.PlanID = planid;
                           //                         carplans.carrierID = car.CarrierID;
                           //                         ebnd.CarrierPlaneTypes.Add(carplans);
                           //                         ebnd.SaveChanges();
                           //                     }
                           //                     else //other plan type
                           //                     {
                           //                         //check if other was added before 
                           //                         CarrierPlaneType carplans = ebnd.CarrierPlaneTypes.Where(c => c.carrierID == car.CarrierID && c.PlanID == 10).FirstOrDefault();
                           //                         if (carplans == null)
                           //                         {
                           //                             carplans.PlanID = 10;  //otherplan type
                           //                             carplans.carrierID = car.CarrierID;
                           //                             ebnd.CarrierPlaneTypes.Add(carplans);
                           //                             ebnd.SaveChanges();
                           //                             logger.Info(string.Format("Adding PT only for carrierid: {0}, carrir code value: {1}", car.CarrierID, vendorId));
                           //                         }
                           //                         if (string.IsNullOrEmpty(car.CarrierType))
                           //                             car.CarrierType = planchild;
                           //                         else
                           //                             car.CarrierType += "," + planchild;

                           //                     }

                           //                 }
                           //                 catch (Exception ex)
                           //                 {
                           //                     logger.Error("Error adding plan types for Order: " + custName);
                           //                     if (ex.InnerException != null) logger.Error("Inner Exce=: " + ex.InnerException);
                           //                     logger.Error("Err mes=: " + ex.Message);
                           //                     logger.Error("End exception details");
                           //                 }
                           //             }
                           //         #endregion


                           //         #region carriercustomfields //vendor id
                           //         OrderCustomField octvendor = new OrderCustomField();
                           //         octvendor.CarrierID = car.CarrierID;
                           //         octvendor.OrderID = car.OrderID;
                           //         octvendor.CustomFieldValue = vendorId;
                           //         octvendor.CutomfieldID = 99;  //107 testing , 99 demo
                           //         ebnd.OrderCustomFields.Add(octvendor);
                           //         ebnd.SaveChanges();
                           //         #endregion


                           //         #region carriercustomfields //cobraEntity
                           //         OrderCustomField cobraEntity = new OrderCustomField();
                           //         cobraEntity.CarrierID = car.CarrierID;
                           //         cobraEntity.OrderID = car.OrderID;
                           //         cobraEntity.CustomFieldValue = cobraexternalEntity;
                           //         cobraEntity.CutomfieldID = 10110;  //107 testing , 99 demo
                           //         ebnd.OrderCustomFields.Add(cobraEntity);
                           //         ebnd.SaveChanges();
                           //         #endregion

                           //         #region carrier com
                           //         List<CustomerCompany> custcomp = ebnd.CustomerCompanies.Where(c => c.CustomerID == cs.CustomerID).ToList();
                           //         foreach (CustomerCompany comp in custcomp)
                           //         {
                           //             CompCarrier compcarrier = new CompCarrier();
                           //             compcarrier.CarrierID = car.CarrierID;
                           //             compcarrier.CompID = comp.CompID;
                           //             ebnd.CompCarriers.Add(compcarrier);
                           //             ebnd.SaveChanges();
                           //         }
                           //         #endregion


                           //         #region chreqproj
                           //         if (!newcustomer)
                           //         {
                           //             ChangeRequestProject CRP = new ChangeRequestProject();

                           //             CRP.ChRequestId = CH.ID;
                           //             CRP.ProjectId = car.CarrierID;
                           //             ebnd.ChangeRequestProjects.Add(CRP);
                           //             ebnd.SaveChanges();
                           //         }
                           //         #endregion


                           //         ebnd.SaveChanges();
                           //         savedcarcount++;
                           //         logger.Info("Connection added : " + car.ProjectName);
                           //         CarIds.Add(car.CarrierID);

                           //     }
                           //     catch (DbEntityValidationException e)
                           //     {

                           //         logger.Error("Error: adding Order for Customer: " + custName);

                           //         foreach (var eve in e.EntityValidationErrors)
                           //         {
                           //             logger.Error(string.Format("Error: Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                           //                    eve.Entry.Entity.GetType().Name, eve.Entry.State));
                           //             foreach (var ve in eve.ValidationErrors)
                           //             {
                           //                 logger.Error(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                           //                    ve.PropertyName, ve.ErrorMessage));
                           //             }
                           //         }
                           //         // ts.Dispose();
                           //     }
                           //     catch (Exception ex)
                           //     {
                           //         logger.Error("Error: adding Order : " + custName);
                           //         if (ex.InnerException != null) logger.Error("Inner Exce=: " + ex.InnerException);
                           //         logger.Error("Err mes=: " + ex.Message);
                           //         logger.Error("Stack trace=: " + ex.StackTrace);
                           //         //   ts.Dispose();
                           //     }
                           // }
                            #endregion


                            #region Cobra  HSA
                             var hsaCustCars = hSACobraCarProp.Where(c => c["GroupNumber"].ToString() == custCode)
                            .GroupBy(c => c["HSAAdminCarrier"].ToString().Trim())
                            .ToList();


                            foreach (var carItem in hsaCustCars)
                            {
                                bool mCC = false;
                                string mCarrierCode = string.Empty;
                                string mCarrierCodeGrp = string.Empty;
                                string carPlan = carItem.First()["CarrierCode"].ToString().ToLower().Trim();
                                string hsa = carItem.First()["HSAAdminCarrier"].ToString().Trim();
                                string carName = "HSAAdmin-" + hsa;



                                try
                                {
                                    long carierId = ebnd.BMCarrierCodes_View.Where(c => c.CustomerCode == cs.CustomerCode &&  c.SCarrierName.Trim().ToLower().Contains(carName)).Select(c => c.CarrierId).FirstOrDefault();

                                    if (carierId != 0) continue;

                                    eBNCarrier eBNcar = ebnd.eBNCarriers.FirstOrDefault(c => c.CarrierName == "HSAAdmin");
                                    if (eBNcar == null)
                                    {
                                        eBNcar = new eBNCarrier();
                                        eBNcar.CarrierName = "HSAAdmin";
                                        ebnd.eBNCarriers.Add(eBNcar);
                                        ebnd.SaveChanges();
                                    }

                                    #region carrier
                                    Carrier car = new Carrier();
                                    car.CarrierName = carName.Trim();

                                    car.ProjectName = custName.Trim() + "_" + car.CarrierName.Trim();
                                    car.OrderID = cs.OrderID;
                                    car.Allcomp = true;
                                    car.SubmitDate = DateTime.Now.Date;
                                    try
                                    {
                                        car.FedralTaxID = carItem.First()["FedralTaxID"].ToString().Trim();
                                    }
                                    catch (Exception exc)
                                    {
                                        logger.Error("error adding car ftax, ignored for car: " + carName);
                                    }
                                    car.StatusId = 1;
                                    car.MultitenantStatus = false;
                                    car.eBNCarrierId = eBNcar.CarrierId;

                                    ebnd.Carriers.Add(car);
                                    ebnd.SaveChanges();
                                    car.CarrierCode = car.CarrierID.ToString();
                                    ebnd.SaveChanges();
                                    #endregion
                                    #region carrierpltypes

                                    string[] plansArray = carItem.Select(c => c["CarrierPlans"].ToString().Trim()).ToArray();
                                    if (plansArray.Count() > 0)
                                        foreach (var planchild in plansArray)
                                        {
                                            try
                                            {
                                                int planid = ebnd.PlanTypes.Where(p => p.PlanTypeName == planchild).Select(p => p.PlanID).FirstOrDefault();
                                                if (planid != 0)
                                                {
                                                    CarrierPlaneType carplans = new CarrierPlaneType();
                                                    carplans.PlanID = planid;
                                                    carplans.carrierID = car.CarrierID;
                                                    ebnd.CarrierPlaneTypes.Add(carplans);
                                                    ebnd.SaveChanges();
                                                }
                                                else //other plan type
                                                {
                                                    //check if other was added before 
                                                    CarrierPlaneType carplans = ebnd.CarrierPlaneTypes.Where(c => c.carrierID == car.CarrierID && c.PlanID == 10).FirstOrDefault();
                                                    if (carplans == null)
                                                    {
                                                        carplans.PlanID = 10;  //otherplan type
                                                        carplans.carrierID = car.CarrierID;
                                                        ebnd.CarrierPlaneTypes.Add(carplans);
                                                        ebnd.SaveChanges();
                                                        logger.Info(string.Format("Adding PT only for carrierid: {0}, carrir hsa value: {1}", car.CarrierID, hsa));
                                                    }
                                                    if (string.IsNullOrEmpty(car.CarrierType))
                                                        car.CarrierType = planchild;
                                                    else
                                                        car.CarrierType += "," + planchild;

                                                }

                                            }
                                            catch (Exception ex)
                                            {
                                                logger.Error("Error adding plan types for Order: " + custName);
                                                if (ex.InnerException != null) logger.Error("Inner Exce=: " + ex.InnerException);
                                                logger.Error("Err mes=: " + ex.Message);
                                                logger.Error("End exception details");
                                            }
                                        }
                                    #endregion





                                    #region carriercustomfields //cobraEntity
                                    OrderCustomField hSAObj = new OrderCustomField();
                                    hSAObj.CarrierID = car.CarrierID;
                                    hSAObj.OrderID = car.OrderID;
                                    hSAObj.CustomFieldValue = hsa;
                                    hSAObj.CutomfieldID = 10111;
                                    ebnd.OrderCustomFields.Add(hSAObj);
                                    ebnd.SaveChanges();
                                    #endregion

                                    #region carrier com
                                    List<CustomerCompany> custcomp = ebnd.CustomerCompanies.Where(c => c.CustomerID == cs.CustomerID).ToList();
                                    foreach (CustomerCompany comp in custcomp)
                                    {
                                        CompCarrier compcarrier = new CompCarrier();
                                        compcarrier.CarrierID = car.CarrierID;
                                        compcarrier.CompID = comp.CompID;
                                        ebnd.CompCarriers.Add(compcarrier);
                                        ebnd.SaveChanges();
                                    }
                                    #endregion


                                    #region chreqproj
                                    if (!newcustomer)
                                    {
                                        ChangeRequestProject CRP = new ChangeRequestProject();

                                        CRP.ChRequestId = CH.ID;
                                        CRP.ProjectId = car.CarrierID;
                                        ebnd.ChangeRequestProjects.Add(CRP);
                                        ebnd.SaveChanges();
                                    }
                                    #endregion


                                    ebnd.SaveChanges();
                                    savedcarcount++;
                                    logger.Info("Connection added : " + car.ProjectName);
                                    CarIds.Add(car.CarrierID);

                                }
                                catch (DbEntityValidationException e)
                                {

                                    logger.Error("Error: adding Order for Customer: " + custName);

                                    foreach (var eve in e.EntityValidationErrors)
                                    {
                                        logger.Error(string.Format("Error: Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                               eve.Entry.Entity.GetType().Name, eve.Entry.State));
                                        foreach (var ve in eve.ValidationErrors)
                                        {
                                            logger.Error(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                               ve.PropertyName, ve.ErrorMessage));
                                        }
                                    }
                                    // ts.Dispose();
                                }
                                catch (Exception ex)
                                {
                                    logger.Error("Error: adding Order : " + custName);
                                    if (ex.InnerException != null) logger.Error("Inner Exce=: " + ex.InnerException);
                                    logger.Error("Err mes=: " + ex.Message);
                                    logger.Error("Stack trace=: " + ex.StackTrace);
                                    //   ts.Dispose();
                                }

                                #endregion


                                ebnd.SaveChanges();
                                cs.ConnectionsNumber = ebnd.Carriers.Where(c => c.OrderID == cs.OrderID && c.StatusId == 1).Count();
                                ebnd.SaveChanges();

                                logger.Error("no. of saved carriers: " + savedcarcount);
                                if (newcustomer && savedcarcount > 0)
                                {
                                    ts.Complete();
                                    logger.Info("New Customer: successfuly saved new customer: " + custName + " , custcode: " + custCode);
                                    newcustcount++;
                                }
                                else if ((!newcustomer) && savedcarcount > 0)
                                {
                                    ts.Complete();
                                    logger.Info("Add Connection(s): successfuly saved new connection(s): " + custName + " , custcode: " + custCode);
                                    newcustcount++;
                                }
                                // ts.Complete();
                                else
                                {
                                    logger.Error("Error: order was not saved for customer: " + custName);
                                    ts.Dispose();
                                }
                                #endregion

                                orderId = order.OrderID;
                            }
                        }
                        catch (Exception ex)
                        {
                            logger.Error("Error: adding Order : " + custName);
                            if (ex.InnerException != null) logger.Error("Inner Exce=: " + ex.InnerException);
                            logger.Error("Err mes=: " + ex.Message);
                            logger.Error("Stack trace=: " + ex.StackTrace);
                            ts.Dispose();
                        }
                    }

                }

                #region updated customer 

                if (updatedCustomer && customerID != 0)
                    updateCustomer(customerID, custName);
                #endregion

                #region QB
                foreach (var carId in CarIds)
                {
                    try
                    {

                        bool QBcreated = QBase.AddNewProject_NewLib(orderId, carId);
                        if (QBcreated)
                            logger.Info("successfully added in QB: orderId: " + orderId + " , for carrier ID: " + carId);
                        else
                            logger.Info("Error adding in QB: orderId: " + orderId + " , for carrier ID: " + carId);
                    }

                    catch (Exception ex)
                    {
                        logger.Error("Error: adding in QB: orderId: " + orderId + " , for carrier ID: " + carId);
                        if (ex.InnerException != null) logger.Error("Inner Exce=: " + ex.InnerException);
                        logger.Error("Err mes=: " + ex.Message);
                        logger.Error("Stack trace=: " + ex.StackTrace);
                        //   ts.Dispose();
                    }
                }
                #endregion


             

            }
            logger.Info("new Customers = " + newcustcount);
            #endregion


            #region  // cobra groups
            foreach (var cobraGrpItem in cobraGrps)
            {
                using (ebnd = new eBNPortal_ProductionEntities())
                {
                    string cobraGrp = cobraGrpItem["CustomerCode"].ToString();
                    string childcompName = cobraGrpItem["CustomerName"].ToString();


                    string mainCustCode = cobraGrpItem["oldvalueid"].ToString();
                    int cobraEmpNo = 0;

                    if (cobraGrpItem["EmployeesNumber"] != null)
                     cobraEmpNo = Convert.ToInt32( cobraGrpItem["EmployeesNumber"].ToString());

                    mainCustCode = mainCustCode.Substring(2, mainCustCode.Length - 2);
                    Customer cs = ebnd.Customers.Where(c => c.CustomerCode == mainCustCode).FirstOrDefault();

                    if (cs != null)
                    {
                        logger.Info("cobraGrpItem: " + cobraGrp);
                        logger.Info("childcompName: " + childcompName);
                        if (ebnd.CustomerCompanies.Where(c => c.CustomerID == cs.CustomerID && c.CompName.Trim().ToLower() == childcompName.Trim().ToLower()).Count() == 0)
                        {


                            CustomerCompany custCompany = new CustomerCompany();
                            custCompany.CustomerID = cs.CustomerID;
                            custCompany.CompName = childcompName;
                            custCompany.CobraEmployeesNumber = cobraEmpNo;
                            ebnd.CustomerCompanies.Add(custCompany);
                            ebnd.SaveChanges();


                            OrderCustomField Oc = ebnd.OrderCustomFields.Where(c => c.CutomfieldID == 98 && c.OrderID == cs.OrderID).FirstOrDefault();
                            if (Oc != null)
                            {
                                Oc.CustomFieldValue += "," + cobraGrp.ToString();
                                ebnd.SaveChanges();
                                logger.Info("Successfully added: " + Oc.CustomFieldValue);
                            }
                            else
                            {
                                Oc = new OrderCustomField();
                                Oc.CustomFieldValue = cobraGrp.ToString();
                                Oc.CutomfieldID = 98;
                                Oc.OrderID = cs.OrderID;
                                ebnd.OrderCustomFields.Add(Oc);
                                ebnd.SaveChanges();
                                logger.Info(" Null custom field value");
                            }

                            //if(cs.CobraEmployeesNumber == 0 || cs.CobraEmployeesNumber == null)
                            //cs.CobraEmployeesNumber = cobraEmpNo;
                            //else
                            //    cs.CobraEmployeesNumber = cs.CobraEmployeesNumber + cobraEmpNo;

                            ebnd.SaveChanges();

                        }
                        else if (ebnd.CustomerCompanies.Where(c => c.CustomerID == cs.CustomerID && c.CompName.Trim().ToLower() == childcompName.Trim().ToLower()).Count()  > 0 )
                        {
                            var cobracomp = ebnd.CustomerCompanies.Where(c => c.CustomerID == cs.CustomerID && c.CompName.Trim().ToLower() == childcompName.Trim().ToLower() && c.CobraEmployeesNumber == null).FirstOrDefault();
                            //get old cobra emp no.
                            if (cobracomp != null)
                                cobracomp.CobraEmployeesNumber = cobraEmpNo;
                            ebnd.SaveChanges();
                            logger.Info("cobra already added:, childcompName: " + childcompName);
                        }


                        cs.CobraEmployeesNumber = ebnd.CustomerCompanies.Where(c => c.CustomerID == cs.CustomerID).Sum(c => c.CobraEmployeesNumber);
                        ebnd.SaveChanges();

                    }
                    else
                        logger.Info("main customer does not exist: " + mainCustCode);
                }

            }
            #endregion

            #region // terminate grps
            logger.Info("term grp count: " + termGrps.Count());
            foreach (var termGrpItem in termGrps)
            {
                JToken grpTerm = termGrpItem["GroupTermDate"];
                DateTime termDate = Convert.ToDateTime(grpTerm.ToString());
                using (ebnd = new eBNPortal_ProductionEntities())
                {
                    string mainCustCode = termGrpItem["CustomerCode"].ToString();
                    Customer cs = ebnd.Customers.Where(c => c.CustomerCode == mainCustCode && c.Order.StatusId == 2).FirstOrDefault();

                    if (cs != null)
                    {
                        if (termDate.Date <= DateTime.Now.Date)
                        {
                            CancelOrder(cs.OrderID, termDate);
                            logger.Info(string.Format("Grp Cancelled {0} cust code {1} , ", cs.CustomerName, mainCustCode));
                        }
                    }
                    else
                    {
                        logger.Info(string.Format("Not exist in VIP Grp Cancelled {0} , ", mainCustCode));
                    }
                }
            }
            #endregion


        }



        public static JObject GetGroupsAndCarriersJson()
        {
            logger.Info("Calling Nabil's API");

            var expirationDate = DateTime.Today.AddDays(1);
            var buffer = System.Text.Encoding.ASCII.GetBytes(expirationDate.ToString("yyyyMMdd"));
            buffer = MiscHelpers.EncryptData(buffer);
            var authorization = Convert.ToBase64String(buffer);

            using (var client = new HttpClient())
            {
                string url = ConfigurationManager.AppSettings["GrpCarrAPI"];
                url += "GetGroupsAndCarriers";
                UriBuilder uriBuilder = new UriBuilder(url);


                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Authorization", "eBN " + authorization);


                var response = client.PostAsync(uriBuilder.Uri, null).Result;
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    dynamic jsonResponse = JsonConvert.DeserializeObject(response.Content.ReadAsStringAsync().Result);

                    var jsonResArr = (Newtonsoft.Json.Linq.JObject)jsonResponse;
                    logger.Info("getgroupsandcarriers Response " + jsonResponse);

                    return jsonResArr;
                }
                else
                {
                    logger.Info(response.ReasonPhrase);
                    return null;
                }
            }


        }
        public static void UpdateMT()
        {
            logger.Info("Start updating MTs");
            string bMallEnv = ConfigurationManager.AppSettings["bMallEnv"];

            for (int y = 1; y < 2; y++) //CreateMTorder loop on the 1st 2 MT sheets
            {
                string sheetname = string.Empty;
                if (y == 0) sheetname = "Multi Tenant";
                if (y == 1) sheetname = "Not Stated";

                logger.Info("sheet: " + sheetname);

                SLDocument sl = new SLDocument(@"C:\eBNServer\Applications\Web\bulkOrder - Copy\MTs.xlsx", sheetname);
                //   SLDocument sl = new SLDocument(@"D:\MTstestxlsx.xlsx", sheetname);

                SLWorksheetStatistics customerstatistics = sl.GetWorksheetStatistics();

                int rowno = customerstatistics.NumberOfRows;

                for (int i = 2; i <= rowno; i++)
                {
                    using (eBNPortal_ProductionEntities ebnd = new eBNPortal_ProductionEntities())
                    {
                        bool mCarrierCode = false;
                        logger.Info("record: " + i);
                        logger.Info("record: " + i + ", start at: " + DateTime.Now);
                        //MTRec mTRec = new MTRec
                        //{
                        //    carrierCode = sl.GetCellValueAsString(i, 1),
                        //    templateId = sl.GetCellValueAsString(i, 2),
                        //    fileType = sl.GetCellValueAsString(i, 5),
                        //    gexcludedGrpNos = sl.GetCellValueAsString(i, 9)
                        //};
                        string carrierCodeStr = sl.GetCellValueAsString(i, 1).Trim();
                        if (carrierCodeStr.Contains("|"))
                        {
                            mCarrierCode = true;
                            if (carrierCodeStr.Contains("||"))
                                carrierCodeStr = carrierCodeStr.Replace("||", "|");
                        }
                        string templateId = sl.GetCellValueAsString(i, 2);

                        if (templateId.ToLower().Contains(".xfer"))
                            templateId = templateId.Substring(0, templateId.LastIndexOf(".")).Trim();

                        string fileType = sl.GetCellValueAsString(i, 5);
                        string grpNos = sl.GetCellValueAsString(i, 9);
                        bool allCustomers = false;

                        if (y == 1 && grpNos.Trim().ToLower() == "not stated")
                            allCustomers = true;

                        grpNos = grpNos.ToLower().Replace("'", "").Replace("not", "").Replace("in", "").Replace("(", "").Replace(")", "");
                        string[] grpNosArr = grpNos.Split(',');
                        logger.Info(String.Format("Info,  template ID: {0}, grp no: {1}", templateId, grpNos));


                        long? mtCarrierId = null;
                        try
                        {
                            mtCarrierId = ebnd.Customers.Where(c => c.CustomerName.Trim().ToLower() == templateId.Trim().ToLower() && c.Order.PartnerID == 10292 && c.Order.StatusId == 2).Select(c => c.Order.Carriers.FirstOrDefault().CarrierID).FirstOrDefault();
                        }
                        catch (Exception ex)
                        {
                            logger.Error("Error in getting mtCarrierID");
                            if (ex.InnerException != null) logger.Error("Inner Exce=: " + ex.InnerException);
                            logger.Error("Err mes=: " + ex.Message);
                            logger.Error("End exception details");
                        }
                        if (mtCarrierId == null || mtCarrierId == 0)
                        {
                            logger.Error(String.Format("Error, could not find parent feed for template ID: {0}", templateId));
                            continue;
                        }

                        //get carrier id from carrier Code
                        //   foreach (var carrierCode in carrierCodeArr)
                        //{
                        List<OrderCustomField> connections = ebnd.OrderCustomFields.Where(c => c.Order.PartnerID == 10291 && c.Order.StatusId == 2).ToList();

                        connections = connections.Where(c =>
                        ((mCarrierCode && c.CustomFieldValue.Contains("|") && c.CustomFieldValue.Split('|').Any(x => carrierCodeStr.ToLower().Split('|').Contains(x.ToLower())))
                        || ((!mCarrierCode) && c.CustomFieldValue.Contains("|") && c.CustomFieldValue.ToLower().Split('|').Contains(carrierCodeStr.ToLower()))
                        || ((!mCarrierCode) && (!c.CustomFieldValue.Contains("|")) && c.CustomFieldValue.Trim().ToLower() == carrierCodeStr.ToLower())
                        || ((mCarrierCode) && (!c.CustomFieldValue.Contains("|")) && carrierCodeStr.Split('|').Any(x => x.Trim().ToLower() == c.CustomFieldValue.Trim().ToLower()))

                      )).ToList();

                        //just for nca
                        //  List<BMCarrierCodes_View> connectionsBM = ebnd.BMCarrierCodes_View.Where(c=>c.CustomFieldValue.ToLower() == "nca").ToList();

                        //  connectionsBM = connectionsBM.Where(c =>
                        //  ((mCarrierCode && c.CustomFieldValue.Contains("|") && c.CustomFieldValue.Split('|').Any(x => carrierCodeStr.ToLower().Split('|').Contains(x.ToLower())))
                        //  || ((!mCarrierCode) && c.CustomFieldValue.Contains("|") && c.CustomFieldValue.ToLower().Split('|').Contains(carrierCodeStr.ToLower()))
                        //  || ((!mCarrierCode) && (!c.CustomFieldValue.Contains("|")) && c.CustomFieldValue.Trim().ToLower() == carrierCodeStr.ToLower())
                        //  || ((mCarrierCode) && (!c.CustomFieldValue.Contains("|")) && carrierCodeStr.Split('|').Any(x => x.Trim().ToLower() == c.CustomFieldValue.Trim().ToLower()))

                        //)).ToList();


                        if (connections.Count > 0)
                        {
                            List<long> carrierIds = connections.Select(c => (long)c.CarrierID).ToList();//ebnd.OrderCustomFields.Where(c => c.CustomFieldValue == carrierCode).Select(c => (long)c.CarrierID).ToList();
                            List<long> orderIds = connections.Select(c => c.OrderID).ToList();

                            //  List<string> bmCustCode = connectionsBM.Select(c => c.CustomerCode).ToList();

                            logger.Info("info, OrderIds: " + orderIds);
                            //  logger.Info("info, bmCustCode: " + bmCustCode.Count);


                            List<long> statedOrderIds = new List<long>();
                            try
                            {
                                logger.Info("info, " + allCustomers);
                                if (y == 1 && allCustomers)
                                    statedOrderIds = ebnd.Customers.Where(c => orderIds.Contains(c.OrderID)).Select(c => c.OrderID).ToList();
                                else if (y == 1)
                                    statedOrderIds = ebnd.Customers.Where(c => (!grpNosArr.Contains(c.CustomerCode.Trim())) && orderIds.Contains(c.OrderID)).Select(c => c.OrderID).ToList();
                                else
                                    statedOrderIds = ebnd.Customers.Where(c => grpNosArr.Contains(c.CustomerCode.Trim()) && orderIds.Contains(c.OrderID)).Select(c => (long)c.OrderID).ToList();




                                //logger.Info("info, " + allCustomers);
                                //if (y == 1 && allCustomers)
                                //    statedOrderIds = ebnd.Customers.Where(c => bmCustCode.Contains(c.CustomerCode)).Select(c => c.OrderID).ToList();
                                //else if (y == 1)
                                //    statedOrderIds = ebnd.Customers.Where(c => (!grpNosArr.Contains(c.CustomerCode.Trim())) && bmCustCode.Contains(c.CustomerCode)).Select(c => c.OrderID).ToList();
                                //else
                                //    statedOrderIds = ebnd.Customers.Where(c => grpNosArr.Contains(c.CustomerCode.Trim()) && bmCustCode.Contains(c.CustomerCode)).Select(c => (long)c.OrderID).ToList();

                            }
                            catch (Exception ex)
                            {
                                logger.Error("Error in getting stated orderids");
                                if (ex.InnerException != null) logger.Error("Inner Exce=: " + ex.InnerException);
                                logger.Error("Err mes=: " + ex.Message);
                                logger.Error("End exception details");

                                break;
                            }
                            logger.Info("info, statedorderid couns: " + statedOrderIds.Count());


                            foreach (long orderId in statedOrderIds)
                            {
                                bool updatedMts = false;
                                string customerName = ebnd.Customers.Where(c => c.OrderID == orderId).Select(c => c.CustomerName).FirstOrDefault();
                                //   string customerCode = ebnd.Customers.Where(c => c.OrderID == orderId).Select(c => c.CustomerCode).FirstOrDefault();
                                // logger.Info("customerCode" + customerCode);
                                logger.Info("orderId" + orderId);



                                // carrierIds = connectionsBM.Where(c => c.CustomerCode.ToLower() == customerCode.ToLower()).Select(c => c.CarrierId).ToList();

                                //the below line has been commented to fix An expression services limit has been reachede exc
                                List<Carrier> cars = ebnd.Carriers.Where(c => c.OrderID == orderId && carrierIds.Contains(c.CarrierID) && c.StatusId == 1).ToList();
                                // List<Carrier> cars = ebnd.Carriers.Where(c => carrierIds.Contains(c.CarrierID)).ToList();
                                //instead divide the no.

                                logger.Info("carrierIdscount" + carrierIds.Count());

                                #region nca 
                                //List<Carrier> cars = new List<Carrier>();
                                //List<long> tempCars = new List<long>();
                                //tempCars.AddRange(carrierIds);

                                //int startIndex = 1000;
                                //int endindex = 0;
                                //logger.Info("tempCars =" + tempCars.Count);
                                //logger.Info("orderId =" + orderId);

                                //while (tempCars.Count() > 0)
                                //{
                                //    logger.Info("whilecarrieridscount =" + tempCars.Count);

                                //    List<Carrier> pCars = new List<Carrier>();

                                //    int partitionedCarriersCount = tempCars.Count() > startIndex ? startIndex : tempCars.Count();
                                //    logger.Info("partitionedCarriersCount =" + partitionedCarriersCount);


                                //    IEnumerable<long> partitionedCarriers = tempCars.Take(partitionedCarriersCount);
                                //    logger.Info("partitionedCarriers =" + partitionedCarriers.Count());

                                //    pCars = ebnd.Carriers.Where(c => c.OrderID == orderId && partitionedCarriers.Contains(c.CarrierID) && c.StatusId == 1).ToList();
                                //    logger.Info("pCars =" + pCars.Count);


                                //    tempCars.RemoveRange(0, partitionedCarriersCount);
                                //    cars.AddRange(pCars);
                                //    logger.Info("cars =" + cars.Count);

                                //}

                                #endregion

                                if (cars != null && cars.Count > 0)
                                {


                                    string carName = cars[0].CarrierName;
                                    long carId = cars[0].CarrierID;
                                    string carrierCode = ebnd.OrderCustomFields.Where(c => c.CarrierID == carId && c.CutomfieldID == 99
                                                           && c.OrderID == orderId).Select(c => c.CustomFieldValue).FirstOrDefault();

                                    logger.Info("carrierIds" + carId);

                                    if (carName.Contains("_"))
                                        carName = carName.Substring(0, carName.IndexOf("_"));

                                    if (cars.Where(c => c.CarrierName.ToLower().LastIndexOf(templateId.ToLower()) > 0 || c.MultitenantId == mtCarrierId).Count() > 0)
                                    {
                                        long foundcar = cars.Where(c => c.CarrierName.ToLower().LastIndexOf(templateId.ToLower()) > 0 || c.MultitenantId == mtCarrierId).Select(c => c.CarrierID).FirstOrDefault();
                                        logger.Info("found: " + foundcar);

                                        continue;
                                    }
                                    foreach (var car in cars)
                                    {
                                        logger.Info(String.Format("Updating MT Connection for template ID: {0}, OrderID: {1}, CarrierCode: {2}, CarrierID: {3}", templateId, orderId, carrierCodeStr, car.CarrierID));

                                        // if template is created create a new connection
                                        if (car.MultitenantId == null || car.MultitenantId == 0) // update
                                        {
                                            //  string qBRecId = QBase.QBget_projid(1, customerName, car.CarrierName);

                                            carName = car.CarrierName;
                                            logger.Info("1");
                                            car.CarrierName = carName + "_" + templateId;
                                            car.ProjectName = customerName + "_" + carName + "_" + templateId;
                                            car.MultitenantStatus = true;
                                            car.MultitenantId = mtCarrierId;
                                            car.FileType = fileType;
                                            car.SubmitDate = DateTime.Now;
                                            ebnd.SaveChanges();
                                            updatedMts = true;

                                            string Environment = ConfigurationManager.AppSettings["Environment"];

                                            logger.Info("2");

                                            #region ff
                                            OrderCustomField FF = new OrderCustomField();
                                            FF.CarrierID = car.CarrierID;
                                            FF.OrderID = car.OrderID;
                                            FF.CustomFieldValue = fileType == "Full" ? "Yes" : "NO";

                                            if (bMallEnv == "Sync")  //10105 sync is full file 
                                                FF.CutomfieldID = 10105;
                                            else
                                                FF.CutomfieldID = 10108; //10108 test is full file 

                                            ebnd.OrderCustomFields.Add(FF);

                                            ebnd.SaveChanges();

                                            #endregion

                                            logger.Info(String.Format("Success : Updating MT Connection for template ID: {0}, OrderID: {1}, CarrierCode: {2}, CarrierID: {3}", templateId, orderId, carrierCode, car.CarrierID));

                                            //if (!string.IsNullOrEmpty(qBRecId))
                                            //{
                                            //    QBase.UpdateQB(orderId, car.CarrierID, qBRecId);
                                            //}
                                            //else
                                            //{

                                            //    bool createdinQB = QBase.AddNewProject_NewLib(orderId, car.CarrierID);
                                            //    logger.Info(String.Format("Create MT Connection in QB for template ID: {0}, OrderID: {1}, CarrierCode: {2}, CarrierID: {3}", templateId, orderId, carrierCode, car.CarrierID));
                                            //}

                                        }

                                    }

                                    if (cars.Count() > 0)
                                    {
                                        if (!updatedMts) // Needs to add a connection and associate it with the template
                                        {
                                            logger.Info(String.Format("Adding MT Connection for template ID: {0}, OrderID: {1}, CarrierCode: {2}", templateId, orderId, carrierCodeStr));
                                            string currentTemplateId = string.Empty;

                                            if (cars[0].MultitenantId.HasValue)
                                            {
                                                long mtId = cars[0].MultitenantId.Value;
                                                currentTemplateId = ebnd.Carriers.Where(c => c.CarrierID == mtId).Select(c => c.CarrierName).FirstOrDefault();
                                            }

                                            //if (cars[0].CarrierName.LastIndexOf("_"+currentTemplateId) > 0)
                                            //    carName = carName.Substring(0, carName.IndexOf("_"));

                                            logger.Info(String.Format("Carrier Name: {0}", carName));

                                            try
                                            {
                                                #region Add MT Carrier

                                                #region carrier

                                                string ftax = cars[0].FedralTaxID;
                                                Carrier car = new Carrier();
                                                car.CarrierName = carName.Trim() + "_" + templateId;

                                                car.ProjectName = customerName.Trim() + "_" + car.CarrierName.Trim();
                                                car.OrderID = orderId;
                                                car.Allcomp = true;

                                                if (!string.IsNullOrEmpty(ftax))
                                                    car.FedralTaxID = ftax.ToString().Trim();

                                                car.StatusId = 1;
                                                car.MultitenantStatus = true;
                                                car.eBNCarrierId = cars[0].eBNCarrierId;
                                                car.SubmitDate = DateTime.Now;
                                                car.MultitenantId = mtCarrierId;
                                                car.FileType = fileType;
                                                ebnd.Carriers.Add(car);
                                                ebnd.SaveChanges();
                                                car.CarrierCode = car.CarrierID.ToString();
                                                ebnd.SaveChanges();
                                                #endregion
                                                #region carrierpltypes
                                                var carPlans = cars[0].CarrierPlaneTypes;
                                                int[] plansArray = carPlans.Select(c => c.PlanID).ToArray();
                                                if (plansArray.Count() > 0)
                                                    foreach (var planid in plansArray)
                                                    {
                                                        try
                                                        {
                                                            CarrierPlaneType carplans = new CarrierPlaneType();
                                                            carplans.PlanID = planid;
                                                            carplans.carrierID = car.CarrierID;
                                                            ebnd.CarrierPlaneTypes.Add(carplans);
                                                            ebnd.SaveChanges();
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            logger.Error("Error adding plan types for Order: " + customerName);
                                                            if (ex.InnerException != null) logger.Error("Inner Exce=: " + ex.InnerException);
                                                            logger.Error("Err mes=: " + ex.Message);
                                                            logger.Error("End exception details");
                                                        }
                                                    }
                                                #endregion
                                                #region carriercustomfields //vendor id
                                                OrderCustomField octvendor = new OrderCustomField();
                                                octvendor.CarrierID = car.CarrierID;
                                                octvendor.OrderID = car.OrderID;
                                                octvendor.CustomFieldValue = carrierCode;
                                                octvendor.CutomfieldID = 99;  //107 testing , 99 demo
                                                ebnd.OrderCustomFields.Add(octvendor);
                                                ebnd.SaveChanges();
                                                #endregion

                                                #region ff
                                                OrderCustomField FF = new OrderCustomField();
                                                FF.CarrierID = car.CarrierID;
                                                FF.OrderID = car.OrderID;
                                                FF.CustomFieldValue = fileType == "Full" ? "Yes" : "NO";

                                                if (bMallEnv == "Sync")  //10105 sync is full file 
                                                    FF.CutomfieldID = 10105;
                                                else
                                                    FF.CutomfieldID = 10108; //10108 test is full file 


                                                ebnd.OrderCustomFields.Add(FF);
                                                ebnd.SaveChanges();

                                                #endregion

                                                #region carrier com

                                                List<CompCarrier> carscomp = ebnd.CompCarriers.Where(c => c.CarrierID == car.CarrierID).ToList();
                                                foreach (CompCarrier comp in carscomp)
                                                {
                                                    CompCarrier compcarrier = new CompCarrier();
                                                    compcarrier.CarrierID = car.CarrierID;
                                                    compcarrier.CompID = comp.CompID;
                                                    ebnd.CompCarriers.Add(compcarrier);
                                                    ebnd.SaveChanges();
                                                }
                                                #endregion


                                                ebnd.SaveChanges();
                                                int connectionsNo = ebnd.Carriers.Where(c => c.OrderID == orderId).Count();
                                                Customer cs = ebnd.Customers.Where(c => c.OrderID == orderId).FirstOrDefault();
                                                cs.ConnectionsNumber = connectionsNo;
                                                ebnd.SaveChanges();

                                                logger.Info("Connection added : " + car.ProjectName);
                                                #endregion

                                                //bool qbcreated = QBase.AddNewProject_NewLib(orderId, car.CarrierID);
                                                //logger.Info(String.Format("QB: Adding MT Connection for template ID: {0}, OrderID: {1}, CarrierCode: {2}", templateId, orderId, carrierCode));

                                            }
                                            catch (DbEntityValidationException e)
                                            {
                                                logger.Error(String.Format("Error: Adding MT Connection for template ID: {0}, OrderID: {1}, CarrierCode: {2}", templateId, orderId, carrierCode));


                                                foreach (var eve in e.EntityValidationErrors)
                                                {
                                                    logger.Error(string.Format("Error: Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                                        eve.Entry.Entity.GetType().Name, eve.Entry.State));
                                                    foreach (var ve in eve.ValidationErrors)
                                                    {
                                                        logger.Error(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                                            ve.PropertyName, ve.ErrorMessage));
                                                    }
                                                }

                                            }
                                            catch (Exception ex)
                                            {
                                                logger.Error(String.Format("Error: Adding MT Connection for template ID: {0}, OrderID: {1}, CarrierCode: {2}", templateId, orderId, carrierCode));
                                                if (ex.InnerException != null) logger.Error("Inner Exce=: " + ex.InnerException);
                                                logger.Error("Err mes=: " + ex.Message);
                                                logger.Error("Stack trace=: " + ex.StackTrace);
                                            }
                                        }
                                    }
                                    else
                                        logger.Info("Error: no carriers were found for carriercode: " + carrierCode + ", record no " + i);
                                }
                                else
                                    logger.Info("Error: no carriers were found for orderid: " + orderId);


                            }
                        }
                        else
                            logger.Info("Error: carrier Id was not found for carriercode: " + carrierCodeStr + ", record no " + i);
                        // }
                        logger.Info("records end at: " + DateTime.Now);
                    }
                }
            }
        }

        public static void UpdateMTFromDB()
        {
            logger.Info("Start updating MTs");
            string bMallEnv = ConfigurationManager.AppSettings["bMallEnv"];



            using (eBNPortal_ProductionEntities ebnd = new eBNPortal_ProductionEntities())
            {
                long? mtCarrierId = null;
                List<Carrier> mtCarriers = null;


                mtCarriers = ebnd.Carriers.Where(c => c.OrderID == 50651 && c.Order.PartnerID == 10292 && c.Order.StatusId == 2).ToList();
                mtCarrierId = ebnd.Customers.Where(c => c.OrderID == 50651 && c.Order.PartnerID == 10292 && c.Order.StatusId == 2).Select(c => c.Order.Carriers.FirstOrDefault().CarrierID).FirstOrDefault();
               
                    foreach (var mT in mtCarriers.Where(c => c.OrderID == 50651))
                    {
                    try
                    {
                        logger.Info("Proj: " + mT.ProjectName);

                        bool mCarrierCode = false;
                        
                        string carrierCodeStr = ebnd.OrderCustomFields.Where(c => c.CarrierID == mT.CarrierID && c.OrderID == mT.OrderID && c.CutomfieldID == 10112).Select(c => c.CustomFieldValue).FirstOrDefault();
                        if (!string.IsNullOrEmpty(carrierCodeStr))
                        {
                            logger.Info("carrierCodeStr: " + carrierCodeStr);
                            string templateId = ebnd.Customers.Where(c => c.OrderID == mT.OrderID).Select(c => c.CustomerName).FirstOrDefault();
                            List<long> orders = ebnd.OrderCustomFields.Where(c => c.CutomfieldID == 10110 && c.CustomFieldValue == carrierCodeStr).Select(c => (long)c.OrderID).ToList();
                            List<long> cariers = ebnd.OrderCustomFields.Where(c => c.CutomfieldID == 10110 && c.CustomFieldValue == carrierCodeStr).Select(c => (long)c.CarrierID).ToList();

                            if (orders.Count > 0)
                            {
                                foreach (var order in orders)
                                {
                                    bool updatedMts = false;

                                    List<Carrier> cars = ebnd.Carriers.Where(c => cariers.Contains(c.CarrierID) && c.Order.PartnerID == 10291 && c.OrderID == order).ToList();
                                    string customerName = ebnd.Customers.Where(c => c.OrderID == order).Select(c => c.CustomerName).FirstOrDefault();
                                    if (cars != null && cars.Count > 0)
                                    {
                                        if (cars.Where(c => c.MultitenantId == mtCarrierId).Count() > 0)
                                        {
                                            long foundcar = cars.Where(c => c.CarrierName.ToLower().LastIndexOf(templateId.ToLower()) > 0 || c.MultitenantId == mtCarrierId).Select(c => c.CarrierID).FirstOrDefault();
                                            logger.Info("found: " + foundcar);

                                            continue;
                                        }
                                        string carName = string.Empty;
                                        foreach (var car in cars)
                                        {
                                            long carId = car.CarrierID;
                                            try
                                            {
                                                //   string carrierCode = ebnd.OrderCustomFields.Where(c => c.CarrierID == carId && c.CutomfieldID == 10110).Select(c => c.CustomFieldValue).FirstOrDefault();
                                                logger.Info("carrierIds" + carId);
                                                carName = car.CarrierName;
                                                if (carName.Contains("_"))
                                                    carName = carName.Substring(0, carName.IndexOf("_"));

                                                logger.Info(String.Format("Updating MT Connection for template ID: {0}, OrderID: {1}, CarrierCode: {2}, CarrierID: {3}", templateId, order, carrierCodeStr, car.CarrierID));

                                                // if template is created create a new connection
                                                if (car.MultitenantId == null || car.MultitenantId == 0) // update
                                                {
                                                    string qBRecId = QBase.QBget_projid(1, customerName, car.CarrierName);

                                                    carName = car.CarrierName;
                                                    logger.Info("1");
                                                    car.CarrierName = carName + "_" + templateId;
                                                    car.ProjectName = customerName + "_" + carName + "_" + templateId;
                                                    car.MultitenantStatus = true;
                                                    car.MultitenantId = mtCarrierId;
                                                    car.FileType = "CH";
                                                    car.SubmitDate = DateTime.Now;
                                                    ebnd.SaveChanges();
                                                    updatedMts = true;

                                                    string Environment = ConfigurationManager.AppSettings["Environment"];

                                                    logger.Info("2");

                                                    #region ff
                                                    OrderCustomField FF = new OrderCustomField();
                                                    FF.CarrierID = car.CarrierID;
                                                    FF.OrderID = car.OrderID;
                                                    FF.CustomFieldValue = "NO";

                                                    if (bMallEnv == "Sync")  //10105 sync is full file 
                                                        FF.CutomfieldID = 10105;
                                                    else
                                                        FF.CutomfieldID = 10108; //10108 test is full file 

                                                    ebnd.OrderCustomFields.Add(FF);

                                                    ebnd.SaveChanges();

                                                    #endregion

                                                    logger.Info(String.Format("Success : Updating MT Connection for template ID: {0}, OrderID: {1}, CarrierCode: {2}, CarrierID: {3}", templateId, order, carrierCodeStr, car.CarrierID));

                                                    if (!string.IsNullOrEmpty(qBRecId))
                                                    {
                                                        QBase.UpdateQB(order, car.CarrierID, qBRecId);
                                                    }
                                                    else
                                                    {

                                                        bool createdinQB = QBase.AddNewProject_NewLib(order, car.CarrierID);
                                                        logger.Info(String.Format("Create MT Connection in QB for template ID: {0}, OrderID: {1}, CarrierCode: {2}, CarrierID: {3}", templateId, order, carrierCodeStr, car.CarrierID));
                                                    }

                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                logger.Error("error updating carrier: " + carId);
                                                logger.Info(ex.Message);
                                                // logger.log.Info("User=: " + uu.Identity.Name);
                                                if (ex.InnerException != null) logger.Error("Inner Exce=: " + ex.InnerException);
                                                var st = new StackTrace(ex, true);
                                                var frame = st.GetFrame(0);
                                                var line = frame.GetFileLineNumber();
                                                logger.Error("Err mes=: " + ex.Message);
                                                logger.Error("Stack trace=: " + ex.StackTrace);
                                            }


                                        }

                                        if (cars.Count() > 0)
                                        {
                                            #region comment first round
                                            //if (!updatedMts) // Needs to add a connection and associate it with the template
                                            //{
                                            //    logger.Info(String.Format("Adding MT Connection for template ID: {0}, OrderID: {1}, CarrierCode: {2}", templateId, order, carrierCodeStr));
                                            //    string currentTemplateId = string.Empty;

                                            //    if (cars[0].MultitenantId.HasValue)
                                            //    {
                                            //        long mtId = cars[0].MultitenantId.Value;
                                            //        currentTemplateId = ebnd.Carriers.Where(c => c.CarrierID == mtId).Select(c => c.CarrierName).FirstOrDefault();
                                            //    }

                                            //    if (cars[0].CarrierName.LastIndexOf("_" + currentTemplateId) > 0)
                                            //        carName = carName.Substring(0, carName.IndexOf("_"));

                                            //    logger.Info(String.Format("Carrier Name: {0}", carName));

                                            //    try
                                            //    {
                                            //        #region Add MT Carrier

                                            //        #region carrier

                                            //        string ftax = cars[0].FedralTaxID;
                                            //        Carrier car = new Carrier();
                                            //        car.CarrierName = carName.Trim() + "_" + templateId;

                                            //        car.ProjectName = customerName.Trim() + "_" + car.CarrierName.Trim();
                                            //        car.OrderID = order;
                                            //        car.Allcomp = true;

                                            //        if (!string.IsNullOrEmpty(ftax))
                                            //            car.FedralTaxID = ftax.ToString().Trim();

                                            //        car.StatusId = 1;
                                            //        car.MultitenantStatus = true;
                                            //        car.eBNCarrierId = cars[0].eBNCarrierId;
                                            //        car.SubmitDate = DateTime.Now;
                                            //        car.MultitenantId = mtCarrierId;
                                            //        car.FileType = "CH";
                                            //        ebnd.Carriers.Add(car);
                                            //        ebnd.SaveChanges();
                                            //        car.CarrierCode = car.CarrierID.ToString();
                                            //        ebnd.SaveChanges();
                                            //        #endregion
                                            //        #region carrierpltypes
                                            //        var carPlans = cars[0].CarrierPlaneTypes;
                                            //        int[] plansArray = carPlans.Select(c => c.PlanID).ToArray();
                                            //        if (plansArray.Count() > 0)
                                            //            foreach (var planid in plansArray)
                                            //            {
                                            //                try
                                            //                {
                                            //                    CarrierPlaneType carplans = new CarrierPlaneType();
                                            //                    carplans.PlanID = planid;
                                            //                    carplans.carrierID = car.CarrierID;
                                            //                    ebnd.CarrierPlaneTypes.Add(carplans);
                                            //                    ebnd.SaveChanges();
                                            //                }
                                            //                catch (Exception ex)
                                            //                {
                                            //                    logger.Error("Error adding plan types for Order: " + customerName);
                                            //                    if (ex.InnerException != null) logger.Error("Inner Exce=: " + ex.InnerException);
                                            //                    logger.Error("Err mes=: " + ex.Message);
                                            //                    logger.Error("End exception details");
                                            //                }
                                            //            }
                                            //        #endregion
                                            //        #region carriercustomfields //vendor id
                                            //        OrderCustomField octvendor = new OrderCustomField();
                                            //        octvendor.CarrierID = car.CarrierID;
                                            //        octvendor.OrderID = car.OrderID;
                                            //        octvendor.CustomFieldValue = carrierCodeStr;
                                            //        octvendor.CutomfieldID = 10110;  //107 testing , 99 demo 10110 && c.CustomFieldValue == carrierCodeStr
                                            //        ebnd.OrderCustomFields.Add(octvendor);
                                            //        ebnd.SaveChanges();
                                            //        #endregion

                                            //        #region ff
                                            //        OrderCustomField FF = new OrderCustomField();
                                            //        FF.CarrierID = car.CarrierID;
                                            //        FF.OrderID = car.OrderID;
                                            //        FF.CustomFieldValue = "NO";

                                            //        if (bMallEnv == "Sync")  //10105 sync is full file 
                                            //            FF.CutomfieldID = 10105;
                                            //        else
                                            //            FF.CutomfieldID = 10108; //10108 test is full file 


                                            //        ebnd.OrderCustomFields.Add(FF);
                                            //        ebnd.SaveChanges();

                                            //        #endregion

                                            //        #region carrier com

                                            //        List<CompCarrier> carscomp = ebnd.CompCarriers.Where(c => c.CarrierID == car.CarrierID).ToList();
                                            //        foreach (CompCarrier comp in carscomp)
                                            //        {
                                            //            CompCarrier compcarrier = new CompCarrier();
                                            //            compcarrier.CarrierID = car.CarrierID;
                                            //            compcarrier.CompID = comp.CompID;
                                            //            ebnd.CompCarriers.Add(compcarrier);
                                            //            ebnd.SaveChanges();
                                            //        }
                                            //        #endregion


                                            //        ebnd.SaveChanges();
                                            //        int connectionsNo = ebnd.Carriers.Where(c => c.OrderID == order).Count();
                                            //        Customer cs = ebnd.Customers.Where(c => c.OrderID == order).FirstOrDefault();
                                            //        cs.ConnectionsNumber = connectionsNo;
                                            //        ebnd.SaveChanges();

                                            //        #region changeReq
                                            //        ChangeRequest addconn = new ChangeRequest();
                                            //        addconn.AdminUserID = 51;
                                            //        addconn.ChRequestStatusId = 1;
                                            //        addconn.ChRequestTypeId = 3;
                                            //        addconn.ChRequestDate = DateTime.Now;
                                            //        addconn.OrderId = order;
                                            //        addconn.UserId = 51;
                                            //        addconn.ChRequestSubmitDate = DateTime.Now;
                                            //        ebnd.ChangeRequests.Add(addconn);
                                            //        ebnd.SaveChanges();

                                            //        ChangeRequestProject CRP = new ChangeRequestProject();
                                            //        CRP.ChRequestId = addconn.ID;
                                            //        CRP.ProjectId = car.CarrierID;
                                            //        ebnd.ChangeRequestProjects.Add(CRP);
                                            //        ebnd.SaveChanges();
                                            //        #endregion


                                            //        logger.Info("Connection added : " + car.ProjectName);
                                            //        #endregion

                                            //        bool qbcreated = QBase.AddNewProject_NewLib(order, car.CarrierID);
                                            //        logger.Info(String.Format("QB: Adding MT Connection for template ID: {0}, OrderID: {1}, CarrierCode: {2}", templateId, order, carrierCodeStr));

                                            //    }
                                            //    catch (DbEntityValidationException e)
                                            //    {
                                            //        logger.Error(String.Format("Error: Adding MT Connection for template ID: {0}, OrderID: {1}, CarrierCode: {2}", templateId, order, carrierCodeStr));


                                            //        foreach (var eve in e.EntityValidationErrors)
                                            //        {
                                            //            logger.Error(string.Format("Error: Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                            //                eve.Entry.Entity.GetType().Name, eve.Entry.State));
                                            //            foreach (var ve in eve.ValidationErrors)
                                            //            {
                                            //                logger.Error(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                            //                    ve.PropertyName, ve.ErrorMessage));
                                            //            }
                                            //        }

                                            //    }
                                            //    catch (Exception ex)
                                            //    {
                                            //        logger.Error(String.Format("Error: Adding MT Connection for template ID: {0}, OrderID: {1}, CarrierCode: {2}", templateId, order, carrierCodeStr));
                                            //        if (ex.InnerException != null) logger.Error("Inner Exce=: " + ex.InnerException);
                                            //        logger.Error("Err mes=: " + ex.Message);
                                            //        logger.Error("Stack trace=: " + ex.StackTrace);
                                            //    }
                                            //}

                                            #endregion
                                        }
                                        else
                                            logger.Info("Error: no carriers were found for orderid: " + order);
                                    }
                                    else
                                        logger.Info("Error: no carriers were found for orderid: " + order);
                                }

                            }
                            else
                                logger.Info("Error: carrier Id was not found for carriercode: " + carrierCodeStr);
                            // }
                            logger.Info("records end at: " + DateTime.Now);
                        }
                        else 
                            logger.Error(string.Format("carrierCodeStr: {0}  is null for MT carrier {1}" , carrierCodeStr, mT.CarrierID));

                    }


                    catch (Exception ex)
                {
                    logger.Error("error updating for job id: " + mT.CarrierID);
                    logger.Info(ex.Message);
                    // logger.log.Info("User=: " + uu.Identity.Name);
                    if (ex.InnerException != null) logger.Error("Inner Exce=: " + ex.InnerException);
                    var st = new StackTrace(ex, true);
                    var frame = st.GetFrame(0);
                    var line = frame.GetFileLineNumber();
                    logger.Error("Err mes=: " + ex.Message);
                    logger.Error("Stack trace=: " + ex.StackTrace);
                }
            }
            }
        }

        public static void CreateMTorder()
        {
            logger.Info("start adding MT feed");

            string custName = string.Empty;
            string ebncar = string.Empty;
            string custFedTax = string.Empty;
            string vednorId = string.Empty;
            string customerCode = string.Empty;
            long ebncarId = 0;
            SLDocument sl = new SLDocument(@"C:\eBNServer\Applications\Web\bulkOrder\SyncMT.xlsx");
            // SLDocument sl = new SLDocument(@"d:\SyncMT.xlsx");

            SLWorksheetStatistics customerstatistics = sl.GetWorksheetStatistics();

            int rowno = customerstatistics.NumberOfRows;

            for (int i = 2; i <= rowno; i++)
            {
                using (TransactionScope ts = new TransactionScope())
                {
                    using (ebnd = new eBNPortal_ProductionEntities())
                    {

                        try
                        {
                            custName = sl.GetCellValueAsString(i, 1).Trim();
                            custFedTax = sl.GetCellValueAsString(i, 2).Trim();
                            customerCode = sl.GetCellValueAsString(i, 3).Trim();
                            vednorId = sl.GetCellValueAsString(i, 4).Trim();
                            ebncar = sl.GetCellValueAsString(i, 5).Trim();
                            ebncarId = sl.GetCellValueAsInt64(i, 6);



                            Customer cs = ebnd.Customers.FirstOrDefault(c => c.CustomerName.ToLower().Trim() == custName.ToLower().Trim() && c.FTaxID == custFedTax);
                            Carrier car = null;
                            if (cs != null)
                            {
                                //{
                                //    car = ebnd.Carriers.Where(c => c.CarrierName == ebncar && c.OrderID == cs.CustomerID).FirstOrDefault();
                                //    if (car != null)
                                //    {
                                //        logger.Info("this MT feed: " + custName + ", for carrier name: " + ebncar + " already exist");
                                //        ts.Dispose();
                                //        return;
                                //    }
                                //    else
                                //        logger.Info("this MT feed: " + custName + "already exists, only adding carrier: " + ebncar);
                                ts.Dispose();
                                return;
                            }


                            if (cs == null)
                            {
                                Order order = CreateOrder(10292);  //Multitenant partner testting 10362, demo 10292
                                ebnd.Orders.Add(order);
                                ebnd.SaveChanges();

                                Order_History oh = CreateHistory(order.OrderID);
                                ebnd.Order_History.Add(oh);
                                ebnd.SaveChanges();

                                ChangeRequest CH = AddChangeRequest(order.OrderID);
                                ebnd.ChangeRequests.Add(CH);
                                ebnd.SaveChanges();

                                #region customer
                                cs = new Customer();
                                cs.CustomerName = custName.Trim();
                                cs.FTaxID = custFedTax;
                                cs.OrderID = order.OrderID;
                                cs.ConnectionsNumber = 1;
                                cs.CustomerCode = order.OrderID.ToString();
                                ebnd.Customers.Add(cs);
                                ebnd.SaveChanges();

                                // //custom Field
                                // OrderCustomField Oc = new OrderCustomField();
                                // Oc.OrderID = order.OrderID;
                                // Oc.CutomfieldID = 22;
                                //// Oc.CustomFieldValue = 
                                // ebnd.OrderCustomFields.Add(Oc);
                                // ebnd.SaveChanges();


                                //Partner contacts
                                CustomerContact pcs = new CustomerContact
                                {
                                    CustomerID = cs.CustomerID,
                                    IsPartner = true,
                                    // pcs.CustomerContactName =
                                    CustomerContactPhone = "(410) 512-3000",
                                    CustomerContactEmail = "sharon.simms@benefitmall.com"
                                };
                                ebnd.CustomerContacts.Add(pcs);
                                ebnd.SaveChanges();
                                #endregion
                                #region companies
                                #endregion
                            }

                            if (car == null)
                            {

                                #region carriers

                                //eBNCarrier eBNcar = ebnd.eBNCarriers.Where(c => c.CarrierName.ToLower().Trim() == ebncar.ToLower().Trim()).FirstOrDefault();
                                //if (eBNcar == null)
                                //{
                                //    eBNcar = new eBNCarrier();
                                //    eBNcar.CarrierName = ebncar;
                                //    ebnd.eBNCarriers.Add(eBNcar);
                                //    ebnd.SaveChanges();
                                //}

                                car = new Carrier();
                                car.CarrierName = ebncar.Trim();
                                car.ProjectName = custName + "_" + ebncar;
                                car.OrderID = cs.OrderID;
                                car.StatusId = 1;
                                car.FedralTaxID = custFedTax; ;
                                car.eBNCarrierId = ebncarId;
                                ebnd.Carriers.Add(car);
                                ebnd.SaveChanges();
                                car.CarrierCode = car.CarrierID.ToString();
                                ebnd.SaveChanges();


                                #region carriercustomfields //vendor id
                                OrderCustomField octvendor = new OrderCustomField();
                                octvendor.CarrierID = car.CarrierID;
                                octvendor.OrderID = car.OrderID;
                                octvendor.CustomFieldValue = vednorId;
                                octvendor.CutomfieldID = 10104;  //107 testing , 99 demo
                                ebnd.OrderCustomFields.Add(octvendor);
                                ebnd.SaveChanges();
                                #endregion

                                #endregion
                            }

                            logger.Info("successfully: added MT feed: " + custName + " , for carrier name: " + ebncar);


                            #region QB
                            //bool QBcreated = AddNewProject_NewLib(car.OrderID, car.CarrierID);
                            //if (QBcreated)
                            //    logger.Info("successfully added in QB: MT feed: " + custName + " , for carrier name: " + ebncar);
                            //else
                            //    logger.Info("Error adding in QB: MT feed: " + custName + " , for carrier name: " + ebncar);

                            #endregion
                            ts.Complete();
                            //  }
                        }
                        catch (DbEntityValidationException e)
                        {
                            if (!string.IsNullOrEmpty(custName))
                                logger.Error("Error: adding MT feed: " + custName + " , for carrier name: " + ebncar);

                            //else
                            //    logger.Error("Error: adding MT Outer XML: " + outerNode);

                            foreach (var eve in e.EntityValidationErrors)
                            {
                                logger.Error(string.Format("Error: Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                    eve.Entry.Entity.GetType().Name, eve.Entry.State));
                                foreach (var ve in eve.ValidationErrors)
                                {
                                    logger.Error(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                        ve.PropertyName, ve.ErrorMessage));
                                }
                            }
                            ts.Dispose();
                        }
                        catch (Exception ex)
                        {
                            if (!string.IsNullOrEmpty(custName))
                                logger.Error("Error: adding MT feed: " + custName + " , for carrier name: " + ebncar);

                            else
                                logger.Error("Error: adding MT Outer XML: " + custName);


                            if (ex.InnerException != null) logger.Error("Inner Exce=: " + ex.InnerException);
                            logger.Error("Err mes=: " + ex.Message);
                            logger.Error("Stack trace=: " + ex.StackTrace);
                            ts.Dispose();

                        }
                    }
                }
            }
        }


        #region Validations
        public static List<string> ChkCarrierValidations(List<Carrier> cars)
        {
            try
            {
                List<string> errors = new List<string>();
                var cutRequiredFiledsIds = new List<long?>();


                using (eBNPortal_ProductionEntities ebnDB = new eBNPortal_ProductionEntities())
                {
                    cutRequiredFiledsIds = ebnDB.BusinessRules.Where(r => r.Required == true && r.PartnerId == 229).Select(r => r.FieldId).ToList();

                    foreach (int field in cutRequiredFiledsIds)
                    {
                        switch (field)
                        {

                            case 23: ////Fedral Tax ID
                                if (cars.Where(c => c.FedralTaxID == null).Count() > 0)// if (.Length > 50)
                                {
                                    foreach (Carrier car in cars.Where(c => c.FedralTaxID == null).ToList())
                                    {
                                        errors.Add(car.CarrierName + "'s Fedt tax id is erquired");
                                    }
                                }
                                break;
                            case 24: ////ContactName *
                                if (cars.Where(c => c.ContactName == null).Count() > 0)// if (.Length > 50)
                                {
                                    foreach (Carrier car in cars.Where(c => c.ContactName == null).ToList())
                                    {
                                        errors.Add(car.CarrierName + "'s ContactName is erquired");
                                    }
                                }
                                break;

                            case 25: ////Phone
                                if (cars.Where(c => c.ContactPhone == null).Count() > 0)// if (.Length > 50)
                                {
                                    foreach (Carrier car in cars.Where(c => c.ContactPhone == null).ToList())
                                    {
                                        errors.Add(car.CarrierName + "'s ContactPhone is erquired");
                                    }
                                }
                                break;

                            case 26: ////Email
                                if (cars.Where(c => c.ContactEmail == null).Count() > 0)// if (.Length > 50)
                                {
                                    foreach (Carrier car in cars.Where(c => c.ContactEmail == null).ToList())
                                    {
                                        errors.Add(car.CarrierName + "'s Contact Email is erquired");
                                    }
                                }
                                break;
                            case 53: ////Current year last production file date
                                if (cars.Where(c => c.CurrentYearLastProductionFileDate == null).Count() > 0)// if (.Length > 50)
                                {
                                    foreach (Carrier car in cars.Where(c => c.CurrentYearLastProductionFileDate == null).ToList())
                                    {
                                        errors.Add(car.CarrierName + "'s Current Year Last Production File Date is erquired");
                                    }
                                }
                                break;

                        }
                    }

                    // group number is always srequired
                    if (cars.Where(c => c.GroupNumber != null && c.GroupNumber.Length > 50).Count() > 0)// if (.Length > 50)
                    {
                        foreach (Carrier car in cars.Where(c => c.GroupNumber != null && c.GroupNumber.Length > 50).ToList())
                        {
                            errors.Add(car.CarrierName + "'s Group number should not exceed length of '50'");
                        }
                    }

                    if (cars.Where(c => c.CarrierName != null && c.CarrierName.Length > 50).Count() > 0)// if (.Length > 50)
                    {
                        foreach (Carrier car in cars.Where(c => c.CarrierName != null && c.CarrierName.Length > 50).ToList())
                        {
                            errors.Add(car.CarrierName + "'s  name should not exceed length of '50'");
                        }
                    }

                    if (cars.Where(c => c.ContactEmail != null).Count() > 0)
                    {
                        foreach (Carrier car in cars.Where(c => c.CarrierName != null && c.CarrierName.Length > 50).ToList())
                        {
                            errors.Add(car.CarrierName + "'s  name should not exceed length of '50'");


                            Match match = Regex.Match(car.ContactEmail, "^([\\w-]+(?:\\.[\\w-]+)*)@((?:[\\w-]+\\.)*\\w[\\w-]{0,66})((?:[\\w-]+\\.)*\\w[\\w-]{0,66})\\.([a-zA-Z]{2,6}(?:\\.[a-zA-Z]{2})?)$");
                            if (!match.Success)
                            {
                                errors.Add(car.CarrierName + "'s email should match the following format: name@gmail.com.");
                            }
                        }
                    }

                }

                if (errors.Count > 0) return errors;

                return null;
            }
            catch
            {
                return null;

            }
        }
        public static List<string> ChkCustomerValidations(Customer customer)
        {
            try
            {
                List<string> errors = new List<string>();

                //Validate customercode
                using (eBNPortal_ProductionEntities ebnDB = new eBNPortal_ProductionEntities())
                {
                    #region customer code
                    if (ebnDB.Customers.Where(c => c.CustomerCode == customer.CustomerCode).Count() > 0)
                    {
                        List<Order> orderslist = ebnDB.Orders.Where(o => o.StatusId != 3 && o.OrderID == o.Customers.Where(c => c.CustomerCode == customer.CustomerCode).Select(c => c.OrderID).FirstOrDefault()).ToList();
                        Order order = ebnDB.Orders.Where(o => o.OrderID == customer.OrderID).FirstOrDefault();
                        bool Invalid = false;

                        if (order != null) // not new order
                        {
                            foreach (Order or in orderslist)
                            {
                                if (or.OrderType == 1 & or.OrderID != customer.OrderID)
                                {
                                    if (or.StatusId != 3)
                                    {
                                        Invalid = true;
                                    }
                                }
                            }

                        }
                        if ((order != null && customer.OrderID != order.OrderID) || (Invalid == true))
                        {
                            Order Or;
                            Or = orderslist.Where(o => o.OrderType == 1).FirstOrDefault();
                            if (Or != null)
                            {
                                if (Or.StatusId != 3)
                                {
                                    errors.Add("Customer ID: " + customer.CustomerCode + "for customer: " + customer.CustomerName + " already exist, customer ID should be unique");
                                }
                            }
                        }
                    }
                    #endregion
                    #region customer Name
                    if (ebnDB.Customers.Where(c => c.CustomerName == customer.CustomerName).Count() > 0)
                    {
                        List<Order> orderslist = ebnDB.Orders.Where(o => o.StatusId != 3 && o.OrderID == o.Customers.Where(c => c.CustomerName == customer.CustomerName).Select(c => c.OrderID).FirstOrDefault()).ToList();
                        Order order = ebnDB.Orders.Where(o => o.OrderID == customer.OrderID).FirstOrDefault();
                        bool Invalid = false;

                        if (order != null) // not new order
                        {
                            foreach (Order or in orderslist)
                            {
                                if (or.OrderType == 1 & or.OrderID != customer.OrderID)
                                {
                                    if (or.StatusId != 3)
                                    {
                                        Invalid = true;
                                    }
                                }
                            }

                        }
                        if ((order != null && customer.OrderID != order.OrderID) || (Invalid == true))
                        {
                            Order Or;
                            Or = orderslist.Where(o => o.OrderType == 1).FirstOrDefault();
                            if (Or != null)
                            {
                                if (Or.StatusId != 3)
                                {
                                    errors.Add(" Customer Name: " + customer.CustomerName + " already exist, customer Name should be unique");
                                }
                            }
                        }
                    }
                    #endregion
                    #region Fedtax
                    if (ebnDB.Customers.Where(c => c.FTaxID == customer.FTaxID).Count() > 0)
                    {
                        List<Order> orderslist = ebnDB.Orders.Where(o => o.StatusId != 3 && o.OrderID == o.Customers.Where(c => c.FTaxID == customer.FTaxID).Select(c => c.OrderID).FirstOrDefault()).ToList();
                        Order order = ebnDB.Orders.Where(o => o.OrderID == customer.OrderID).FirstOrDefault();
                        bool Invalid = false;

                        if (order != null) // not new order
                        {
                            foreach (Order or in orderslist)
                            {
                                if (or.OrderType == 1 & or.OrderID != customer.OrderID)
                                {
                                    if (or.StatusId != 3)
                                    {
                                        Invalid = true;
                                    }
                                }
                            }

                        }
                        if ((order != null && customer.OrderID != order.OrderID) || (Invalid == true))
                        {
                            Order Or;
                            Or = orderslist.Where(o => o.OrderType == 1).FirstOrDefault();
                            if (Or != null)
                            {
                                if (Or.StatusId != 3)
                                {
                                    errors.Add("Customer FedTaxID " + customer.FTaxID + "for customer: " + customer.CustomerName + " already exist, Fed Tax ID Should be unique");
                                }
                            }
                        }
                    }
                    #endregion

                }

                if (errors.Count > 0) return errors;

                return null;
            }
            catch
            {
                return null;

            }
        }

        #endregion


        #region old unneeded
        public static void ReadXMLOrd()
        {

            XmlDocument orderDoc = new XmlDocument();
            XmlNodeList orderNodeList;
            XmlNodeList oMTNodeList;
            int i = 0;
            string str = null;
            FileStream oFS = new FileStream(@"C:\eBNServer\Applications\Web\bulkOrder\BMallOrders.xml", FileMode.Open, FileAccess.Read);

            //FileStream oFS = new FileStream(@"D:\bmall\5March\BMallOrders (1)\BMallOrders.xml", FileMode.Open, FileAccess.Read);
            orderDoc.Load(oFS);

            #region MT
            oMTNodeList = orderDoc.GetElementsByTagName("MTOrder");
            int mtcount = oMTNodeList.Count;


            foreach (XmlNode oMTNode in oMTNodeList)
            {
                CreateMTorder(oMTNode);
            }
            #endregion


            #region Order
            orderNodeList = orderDoc.GetElementsByTagName("Order");
            int orderCount = orderNodeList.Count;
            foreach (XmlNode oNode in orderNodeList)
            {
                Createorder(oNode);
            }
            #endregion
        }
        public static void ReadXMLJobs()
        {
            SLDocument sl = new SLDocument(@"C:\eBNServer\Applications\Web\bulkOrder\BMjobs.xlsx");
            // SLDocument sl = new SLDocument(@"D:\BMjobs.xlsx");
            SLWorksheetStatistics customerstatistics = sl.GetWorksheetStatistics();

            int rowno = customerstatistics.NumberOfRows;

            for (int r = 1; r <= rowno; r++)
            {
                XmlDocument orderDoc = new XmlDocument();
                XmlNodeList orderNodeList;
                // XmlNodeList oMTNodeList;

                int i = 0;
                string str = null;
                FileStream oFS = new FileStream(@"C:\eBNServer\Applications\Web\bulkOrder\BMallOrders.xml", FileMode.Open, FileAccess.Read);
                // FileStream oFS = new FileStream(@"D:\bmall\5March\BMallOrders (1)\BMallOrders.xml", FileMode.Open, FileAccess.Read);

                orderDoc.Load(oFS);

                #region MT
                //  oMTNodeList = orderDoc.GetElementsByTagName("MTOrder");
                string job = sl.GetCellValueAsString(r, 1).ToLower();
                // string selector = "MTOrder[ . ='" + job + "']"; //author[. = "Matthew Bob"]
                //string job=   "deltadental_co_834_5010_prod_full_file.xfer";

                string selector = "MTOrder[translate(.," + "'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='" + job + "']";

                XmlNodeList oMTNodeList = orderDoc.DocumentElement.SelectNodes(selector);


                int mtcount = oMTNodeList.Count;
                logger.Info("mtCount " + mtcount + " for job: " + job);
                //foreach (XmlNode oMTNode in oMTNodeList)
                //{
                //    createMTorder(oMTNode);
                //}
                #endregion

                #region Order
                //  string vendor = sl.GetCellValueAsString(r, 2).ToLower();
                // string vselector = "/Orders/Order/Carrier[VendorId='" + vendor + "' and TemplateID='" + job + "' ]"; // person[@name = 'shawn' and hair / @style = 'bald']
                // string vselector = "/Orders/Order/Carrier[translate(vendorid," + "'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='" + vendor + "' and translate(templateid," + "'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='" + job + "' ]"; // person[@name = 'shawn' and hair / @style = 'bald']
                string vselector = "/Orders/Order/Carrier[translate(TemplateID," + "'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')='" + job + "']";


                //string  jselector = "/Orders/Order/Carrier[TemplateID='" + job + "']";

                orderNodeList = orderDoc.DocumentElement.SelectNodes(vselector);


                int orderCount = orderNodeList.Count;
                logger.Info("orderCount " + orderCount + " for job: " + job);
                foreach (XmlNode oNode in orderNodeList)
                {
                    // oNode.ParentNode
                    Createorder(oNode.ParentNode);
                }
                #endregion

            }
        }
        public static void CreateMTorder(XmlNode oMTNode)
        {
            logger.Info("start adding MT feed");

            string outerNode = oMTNode.OuterXml;
            string custName = string.Empty;
            string ebncar = string.Empty;
            string custFedTax = string.Empty;
            string vednorId = string.Empty;
            using (TransactionScope ts = new TransactionScope())
            {
                using (ebnd = new eBNPortal_ProductionEntities())
                {

                    try
                    {
                        custName = oMTNode.FirstChild.Value.Substring(0, oMTNode.FirstChild.Value.LastIndexOf(".")).Trim();
                        //ebncar = oMTNode.Attributes[0].Value;
                        //custFedTax = oMTNode.Attributes[1].Value;
                        //vednorId = oMTNode.Attributes[3].Value;


                        for (int i = 0; i < oMTNode.Attributes.Count; i++)
                        {
                            if (oMTNode.Attributes[i].Name == "CarrierName")
                                ebncar = oMTNode.Attributes[i].Value.Trim();

                            if (oMTNode.Attributes[i].Name == "TaxId")
                                custFedTax = oMTNode.Attributes[i].Value.Trim();

                            if (oMTNode.Attributes[i].Name == "VindorId")
                                vednorId = oMTNode.Attributes[i].Value.Trim();
                        }


                        // using (eBNPortal_ProductionEntities ebnd = new eBNPortal_ProductionEntities())
                        //{
                        Customer cs = ebnd.Customers.FirstOrDefault(c => c.CustomerName.ToLower().Trim() == custName.ToLower().Trim() && c.FTaxID == custFedTax);
                        Carrier car = null;
                        if (cs != null)
                        {
                            //{
                            //    car = ebnd.Carriers.Where(c => c.CarrierName == ebncar && c.OrderID == cs.CustomerID).FirstOrDefault();
                            //    if (car != null)
                            //    {
                            //        logger.Info("this MT feed: " + custName + ", for carrier name: " + ebncar + " already exist");
                            //        ts.Dispose();
                            //        return;
                            //    }
                            //    else
                            //        logger.Info("this MT feed: " + custName + "already exists, only adding carrier: " + ebncar);
                            ts.Dispose();
                            return;
                        }


                        if (cs == null)
                        {
                            Order order = CreateOrder(10292);  //Multitenant partner testting 10362, demo 10292
                            ebnd.Orders.Add(order);
                            ebnd.SaveChanges();

                            Order_History oh = CreateHistory(order.OrderID);
                            ebnd.Order_History.Add(oh);
                            ebnd.SaveChanges();

                            ChangeRequest CH = AddChangeRequest(order.OrderID);
                            ebnd.ChangeRequests.Add(CH);
                            ebnd.SaveChanges();

                            #region customer
                            cs = new Customer();
                            cs.CustomerName = custName.Trim();
                            cs.FTaxID = custFedTax;
                            cs.OrderID = order.OrderID;
                            cs.ConnectionsNumber = 1;
                            cs.CustomerCode = order.OrderID.ToString();
                            ebnd.Customers.Add(cs);
                            ebnd.SaveChanges();

                            // //custom Field
                            // OrderCustomField Oc = new OrderCustomField();
                            // Oc.OrderID = order.OrderID;
                            // Oc.CutomfieldID = 22;
                            //// Oc.CustomFieldValue = 
                            // ebnd.OrderCustomFields.Add(Oc);
                            // ebnd.SaveChanges();


                            //Partner contacts
                            CustomerContact pcs = new CustomerContact
                            {
                                CustomerID = cs.CustomerID,
                                IsPartner = true,
                                // pcs.CustomerContactName =
                                CustomerContactPhone = "(410) 512-3000",
                                CustomerContactEmail = "sharon.simms@benefitmall.com"
                            };
                            ebnd.CustomerContacts.Add(pcs);
                            ebnd.SaveChanges();
                            #endregion
                            #region companies
                            #endregion
                        }

                        if (car == null)
                        {

                            #region carriers

                            eBNCarrier eBNcar = ebnd.eBNCarriers.Where(c => c.CarrierName.ToLower().Trim() == ebncar.ToLower().Trim()).FirstOrDefault();
                            if (eBNcar == null)
                            {
                                eBNcar = new eBNCarrier();
                                eBNcar.CarrierName = ebncar;
                                ebnd.eBNCarriers.Add(eBNcar);
                                ebnd.SaveChanges();
                            }

                            car = new Carrier();
                            car.CarrierName = ebncar.Trim();
                            car.ProjectName = custName + "_" + ebncar;
                            car.OrderID = cs.OrderID;
                            car.StatusId = 1;
                            car.FedralTaxID = custFedTax; ;
                            car.eBNCarrierId = eBNcar.CarrierId;
                            ebnd.Carriers.Add(car);
                            ebnd.SaveChanges();
                            car.CarrierCode = car.CarrierID.ToString();
                            ebnd.SaveChanges();


                            #region carriercustomfields //vendor id
                            OrderCustomField octvendor = new OrderCustomField();
                            octvendor.CarrierID = car.CarrierID;
                            octvendor.OrderID = car.OrderID;
                            octvendor.CustomFieldValue = vednorId;
                            octvendor.CutomfieldID = 10104;  //107 testing , 99 demo
                            ebnd.OrderCustomFields.Add(octvendor);
                            ebnd.SaveChanges();
                            #endregion

                            #endregion
                        }

                        logger.Info("successfully: added MT feed: " + custName + " , for carrier name: " + ebncar);


                        #region QB
                        //bool QBcreated = AddNewProject_NewLib(car.OrderID, car.CarrierID);
                        //if (QBcreated)
                        //    logger.Info("successfully added in QB: MT feed: " + custName + " , for carrier name: " + ebncar);
                        //else
                        //    logger.Info("Error adding in QB: MT feed: " + custName + " , for carrier name: " + ebncar);

                        #endregion
                        ts.Complete();
                        //  }
                    }
                    catch (DbEntityValidationException e)
                    {
                        if (!string.IsNullOrEmpty(custName))
                            logger.Error("Error: adding MT feed: " + custName + " , for carrier name: " + ebncar);

                        //else
                        //    logger.Error("Error: adding MT Outer XML: " + outerNode);

                        foreach (var eve in e.EntityValidationErrors)
                        {
                            logger.Error(string.Format("Error: Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                eve.Entry.Entity.GetType().Name, eve.Entry.State));
                            foreach (var ve in eve.ValidationErrors)
                            {
                                logger.Error(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                    ve.PropertyName, ve.ErrorMessage));
                            }
                        }
                        ts.Dispose();
                    }
                    catch (Exception ex)
                    {
                        if (!string.IsNullOrEmpty(custName))
                            logger.Error("Error: adding MT feed: " + custName + " , for carrier name: " + ebncar);

                        else
                            logger.Error("Error: adding MT Outer XML: " + outerNode);


                        if (ex.InnerException != null) logger.Error("Inner Exce=: " + ex.InnerException);
                        logger.Error("Err mes=: " + ex.Message);
                        logger.Error("Stack trace=: " + ex.StackTrace);
                        ts.Dispose();

                    }
                }
            }
        }
        public static void Createorder(XmlNode oNode)
        {

            logger.Info("start adding order");
            string outerNode = oNode.OuterXml;
            string custName = string.Empty;
            string ftax = string.Empty;
            string custCode = string.Empty;
            string TechGroupID = string.Empty;
            int empno = 0;


            DateTime start = DateTime.Now;
            TimeSpan duration = new TimeSpan(1, 12, 23, 62);

            using (TransactionScope ts = new TransactionScope())
            {
                using (ebnd = new eBNPortal_ProductionEntities())
                {
                    try
                    {
                        for (int i = 0; i < oNode.Attributes.Count; i++)
                        {
                            if (oNode.Attributes[i].Name == "Name")
                                custName = oNode.Attributes[i].Value.Trim();

                            if (oNode.Attributes[i].Name == "SystemId")
                                custCode = oNode.Attributes[i].Value.Trim();

                            if (oNode.Attributes[i].Name == "EmpNo")
                                empno = Convert.ToInt32(oNode.Attributes[i].Value.Trim());
                        }

                        TechGroupID = oNode.ChildNodes.Item(0).FirstChild.Value;
                        //using (eBNPortal_ProductionEntities ebnd = new eBNPortal_ProductionEntities())
                        //{

                        //BM testing 292, demo 10291
                        // Customer cs = ebnd.Customers.Where(c => c.CustomerName == custName && c.Order.PartnerID == 292).FirstOrDefault();

                        //demo 10291
                        Customer cs = ebnd.Customers.FirstOrDefault(c => c.CustomerName.ToLower().Trim() == custName.ToLower().Trim() && c.Order.PartnerID == 10291);
                        #region comments to add missing customers
                        //if (cs != null )
                        //{
                        //    Order or = ebnd.Orders.Where(c => c.OrderID == cs.OrderID).FirstOrDefault();

                        //    if (or != null)
                        //    {
                        //        List<string> carNames = ebnd.Carriers.Where(c => c.OrderID == cs.OrderID && c.StatusId == 1).Select(c => c.CarrierName).ToList();
                        //        List<string> childcars = new List<string>();

                        //        foreach (XmlNode child in oNode)
                        //        {
                        //            if (child.Name.ToLower() == "carrier")
                        //                childcars.Add(child.Attributes[0].Value);
                        //        }

                        //        if (childcars.Except(carNames, StringComparer.OrdinalIgnoreCase).Count() > 0)
                        //            logger.Info("New carriers to be added for an existing order: " + custName);
                        //        else
                        //        {
                        //            logger.Info("order: " + custName + " wiz all carriers already exist");
                        //            return;

                        //        }
                        //    }
                        //    else
                        //    {
                        //        ebnd.Customers.Remove(cs);
                        //        ebnd.SaveChanges();
                        //        removedcd = true;
                        //    }
                        //}
                        #endregion

                        if (cs != null)
                        {
                            logger.Info("order: " + custName + " wiz all carriers already exist");
                            ts.Dispose();
                            return;
                        }

                        //if (removedcd) cs = null;
                        if (cs == null)
                        {
                            logger.Info("New Customer: " + custName);
                            Order order = /* createOrder(10291);*/  CreateOrder(10291); //bm testting 292, demo 10291
                            ebnd.Orders.Add(order);
                            ebnd.SaveChanges();

                            if (order != null)
                            {
                                Order_History oh = CreateHistory(order.OrderID);
                                ebnd.Order_History.Add(oh);
                                ebnd.SaveChanges();

                                ChangeRequest CH = AddChangeRequest(order.OrderID);
                                ebnd.ChangeRequests.Add(CH);
                                ebnd.SaveChanges();
                                #region customer
                                cs = new Customer();
                                cs.CustomerName = custName;
                                cs.CustomerCode = custCode;
                                cs.EmployeesNumber = empno;
                                cs.OrderEmployeesNo = empno;
                                cs.OrderID = order.OrderID;
                                ebnd.Customers.Add(cs);
                                ebnd.SaveChanges();

                                //custom Field Group ID
                                OrderCustomField Oc = new OrderCustomField();
                                Oc.OrderID = order.OrderID;
                                Oc.CutomfieldID = 98;  //testing 106 , demo 98
                                Oc.CustomFieldValue = TechGroupID;
                                ebnd.OrderCustomFields.Add(Oc);
                                ebnd.SaveChanges();


                                //Partner contacts
                                CustomerContact pcs = new CustomerContact();
                                pcs.CustomerID = cs.CustomerID;
                                pcs.IsPartner = true;
                                // pcs.CustomerContactName =
                                pcs.CustomerContactPhone = "(410) 512-3000";
                                pcs.CustomerContactEmail = "sharon.simms@benefitmall.com";
                                ebnd.CustomerContacts.Add(pcs);
                                ebnd.SaveChanges();
                                #endregion
                                #region companies

                                foreach (XmlNode child in oNode)
                                {
                                    if (child.Name.ToLower() == "company")
                                    {
                                        CustomerCompany custComp = new CustomerCompany();
                                        custComp.CustomerID = cs.CustomerID;
                                        custComp.CompName = child.FirstChild.Value;

                                        ebnd.CustomerCompanies.Add(custComp);
                                        ebnd.SaveChanges();
                                    }

                                }
                                #endregion


                                #region carriers
                                int savedcarcount = 0;
                                foreach (XmlNode child in oNode)
                                {
                                    try
                                    {
                                        string templateId = string.Empty;
                                        string VendorId = string.Empty;
                                        string custtempId = string.Empty;
                                        string appendedCarName = string.Empty;

                                        if (child.Name.ToLower() == "carrier")
                                        {
                                            string carname = child.Attributes[0].Value.Trim();


                                            foreach (XmlNode carchild in child)
                                            {
                                                if (carchild.Name.ToLower() == "templateid")
                                                    templateId = carchild.FirstChild.Value.Trim();
                                                if (carchild.Name == "VendorId")
                                                    VendorId = carchild.FirstChild.Value.Trim();
                                            }
                                            long orderid = 0;
                                            long MTCarId = 0;
                                            if (!string.IsNullOrEmpty(templateId))
                                            {
                                                try
                                                {
                                                    custtempId = templateId.Substring(0, templateId.LastIndexOf(".")).Trim();
                                                    appendedCarName = carname + "_" + custtempId;

                                                    orderid = ebnd.Customers.Where(c => c.CustomerName == custtempId).Select(c => c.OrderID).FirstOrDefault();
                                                    if (orderid != 0) MTCarId = ebnd.Carriers.Where(c => c.OrderID == orderid).Select(c => c.CarrierID).FirstOrDefault();
                                                }
                                                catch
                                                {
                                                    logger.Info("Single: " + carname);

                                                }
                                            }



                                            Carrier car = ebnd.Carriers.FirstOrDefault(c => (appendedCarName != null ? c.CarrierName == appendedCarName : c.CarrierName == carname) && c.OrderID == cs.OrderID && c.StatusId == 1);

                                            if (car == null)
                                            {
                                                eBNCarrier eBNcar = ebnd.eBNCarriers.FirstOrDefault(c => c.CarrierName == carname);
                                                if (eBNcar == null)
                                                {
                                                    eBNcar = new eBNCarrier();
                                                    eBNcar.CarrierName = carname;
                                                    ebnd.eBNCarriers.Add(eBNcar);
                                                    ebnd.SaveChanges();
                                                }

                                                #region carrier
                                                car = new Carrier();
                                                if (!string.IsNullOrEmpty(custtempId))
                                                    car.CarrierName = carname.Trim() + "_" + custtempId.Trim();
                                                else
                                                    car.CarrierName = carname.Trim();

                                                car.ProjectName = custName.Trim() + "_" + car.CarrierName.Trim();
                                                car.OrderID = cs.OrderID;
                                                car.Allcomp = true;

                                                try
                                                {
                                                    car.FedralTaxID = child.Attributes[1].Value.Trim();
                                                }
                                                catch (Exception exc)
                                                {
                                                    logger.Error("error adding car ftax, ignored for car: " + carname);
                                                }
                                                car.StatusId = 1;
                                                if (MTCarId != 0)
                                                {
                                                    logger.Info("MTid= " + MTCarId.ToString() + " for carrier " + car.CarrierName);
                                                    car.MultitenantId = MTCarId;
                                                    car.MultitenantStatus = true;
                                                }
                                                else
                                                    car.MultitenantStatus = false;

                                                car.eBNCarrierId = eBNcar.CarrierId;

                                                ebnd.Carriers.Add(car);
                                                ebnd.SaveChanges();
                                                car.CarrierCode = car.CarrierID.ToString();
                                                ebnd.SaveChanges();
                                                #endregion
                                                #region carrierpltypes

                                                foreach (XmlNode planchild in child)
                                                {
                                                    if (planchild.Name == "BenefitType")
                                                    {
                                                        try
                                                        {
                                                            int planid = ebnd.PlanTypes.Where(p => p.PlanTypeName == planchild.FirstChild.Value).Select(p => p.PlanID).FirstOrDefault();

                                                            if (planid != 0)
                                                            {
                                                                CarrierPlaneType carplans = new CarrierPlaneType();
                                                                carplans.PlanID = planid;
                                                                carplans.carrierID = car.CarrierID;
                                                                ebnd.CarrierPlaneTypes.Add(carplans);
                                                                ebnd.SaveChanges();
                                                            }

                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            logger.Error("Error adding plan types for Order: " + custName);
                                                            if (!string.IsNullOrEmpty(custName))
                                                            {
                                                                logger.Error("Error: adding Order: " + custName);
                                                                logger.Error("Error: adding Order XML: " + outerNode);
                                                            }
                                                            else
                                                                logger.Error("Error: adding Order Outer XML: " + outerNode);

                                                            //if (ex.InnerException != null) logger.Error("Inner Exce=: " + ex.InnerException);
                                                            //logger.Error("Err mes=: " + ex.Message);
                                                            //logger.Error("End exception details");
                                                        }
                                                    }
                                                }

                                                #endregion


                                                #region carriercustomfields //vendor id
                                                OrderCustomField octvendor = new OrderCustomField();
                                                octvendor.CarrierID = car.CarrierID;
                                                octvendor.OrderID = car.OrderID;
                                                octvendor.CustomFieldValue = VendorId;
                                                octvendor.CutomfieldID = 99;  //107 testing , 99 demo
                                                ebnd.OrderCustomFields.Add(octvendor);
                                                ebnd.SaveChanges();
                                                #endregion


                                                #region carriercustomfields //template id
                                                OrderCustomField octtemplate = new OrderCustomField
                                                {
                                                    CarrierID = car.CarrierID,
                                                    OrderID = car.OrderID,
                                                    CustomFieldValue = templateId,
                                                    CutomfieldID = 102 //testing 108
                                                };
                                                ebnd.OrderCustomFields.Add(octtemplate);
                                                ebnd.SaveChanges();
                                                #endregion


                                                #region carrier com
                                                List<CustomerCompany> custcomp = ebnd.CustomerCompanies.Where(c => c.CustomerID == cs.CustomerID).ToList();
                                                foreach (CustomerCompany comp in custcomp)
                                                {
                                                    CompCarrier compcarrier = new CompCarrier();
                                                    compcarrier.CarrierID = car.CarrierID;
                                                    compcarrier.CompID = comp.CompID;
                                                    ebnd.CompCarriers.Add(compcarrier);
                                                    ebnd.SaveChanges();
                                                }
                                                #endregion


                                                ebnd.SaveChanges();
                                                savedcarcount++;
                                                #region QB
                                                //bool QBcreated = AddNewProject_NewLib(car.OrderID, car.CarrierID);
                                                //if (QBcreated)
                                                //    logger.Info("successfully added in QB: order: " + custName + " , for carrier name: " + carname);
                                                //else
                                                //    logger.Info("Error adding in QB: order: " + custName + " , for carrier name: " + carname);

                                                #endregion

                                            }
                                            else
                                                logger.Info("the carrier: " + carname + "  , for the customer: " + custName + " already exist");

                                        }
                                    }

                                    catch (DbEntityValidationException e)
                                    {
                                        if (!string.IsNullOrEmpty(custName))
                                            logger.Error("Error: adding Order : " + custName);

                                        else
                                            logger.Error("Error: adding Order XML: " + outerNode);

                                        foreach (var eve in e.EntityValidationErrors)
                                        {
                                            logger.Error(string.Format("Error: Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                                eve.Entry.Entity.GetType().Name, eve.Entry.State));
                                            foreach (var ve in eve.ValidationErrors)
                                            {
                                                logger.Error(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                                    ve.PropertyName, ve.ErrorMessage));
                                            }
                                        }
                                        ts.Dispose();
                                    }
                                    catch (Exception ex)
                                    {
                                        if (!string.IsNullOrEmpty(custName))
                                        {
                                            logger.Error("Error: adding Order: " + custName);
                                            logger.Error("Error: adding Order XML: " + outerNode);
                                        }
                                        else
                                            logger.Error("Error: adding Order Outer XML: " + outerNode);

                                        if (ex.InnerException != null) logger.Error("Inner Exce=: " + ex.InnerException);
                                        logger.Error("Err mes=: " + ex.Message);
                                        logger.Error("Stack trace=: " + ex.StackTrace);
                                        ts.Dispose();
                                    }
                                }
                                #endregion
                                ebnd.SaveChanges();
                                cs.ConnectionsNumber = ebnd.Carriers.Where(c => c.OrderID == cs.OrderID && c.StatusId == 1).Count();
                                ebnd.SaveChanges();

                                logger.Error("no. of saved carriers: " + savedcarcount);
                                if (savedcarcount > 0) ts.Complete();
                                // ts.Complete();
                            }
                            else
                            {
                                logger.Error("order was not saved");
                                ts.Dispose();
                                return;
                            }

                        }
                        //  }
                    }
                    catch (DbEntityValidationException e)
                    {

                        logger.Error("Error: adding Order Outer XML: " + outerNode);

                        foreach (var eve in e.EntityValidationErrors)
                        {
                            logger.Error(string.Format("Error: Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                eve.Entry.Entity.GetType().Name, eve.Entry.State));
                            foreach (var ve in eve.ValidationErrors)
                            {
                                logger.Error(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                    ve.PropertyName, ve.ErrorMessage));
                            }
                        }
                        ts.Dispose();
                    }
                    catch (Exception ex)
                    {
                        if (!string.IsNullOrEmpty(custName))
                        {
                            logger.Error("Error: adding Order : " + custName);
                            logger.Error("Error: adding Order Outer XML: " + outerNode);
                        }

                        else
                            logger.Error("Error: adding MT Outer XML: " + outerNode);


                        if (ex.InnerException != null) logger.Error("Inner Exce=: " + ex.InnerException);
                        logger.Error("Err mes=: " + ex.Message);
                        logger.Error("Stack trace=: " + ex.StackTrace);
                        ts.Dispose();
                    }
                }
            }
            duration = DateTime.Now - start;
            string durationformat = duration.ToString();
            logger.Info("creating Order in : " + durationformat.Substring(0, durationformat.LastIndexOf(".")));

        }
        public static void CreateBulkOrder()
        {
            try
            {

                SLDocument sl = new SLDocument(@"D:\Bulkorderstemplate.xlsx", "customers");
                SLWorksheetStatistics customerstatistics = sl.GetWorksheetStatistics();

                int rowno = customerstatistics.NumberOfRows;

                for (int i = 3; i <= rowno; i++)
                {
                    long orderid;
                    List<Carrier> carriers = new List<Carrier>();
                    List<string> errors = new List<string>();
                    using (TransactionScope ts = new TransactionScope())
                    {
                        try
                        {
                            using (eBNPortal_ProductionEntities ebnd = new eBNPortal_ProductionEntities())
                            {
                                if (sl.GetCellValueAsString(i, 1) == "" || sl.GetCellValueAsString(i, 1) == null) continue;
                                SLDocument sl2 = new SLDocument(@"D:\Bulkorderstemplate.xlsx", "Customer Benefit Information");

                                Order or = CreateOrder(22);  // dummy
                                string comments = sl2.GetCellValueAsString(i, 7);
                                if (!string.IsNullOrEmpty(comments) && comments.Length > 300) //.Substring(0,299)
                                    or.Comments = comments.Substring(0, 299);
                                else
                                    or.Comments = comments;

                                ebnd.Orders.Add(or);
                                ebnd.SaveChanges();

                                sl2.Dispose();

                                Order_History oh = CreateHistory(or.OrderID);
                                ebnd.Order_History.Add(oh);

                                ChangeRequest CH = AddChangeRequest(or.OrderID);
                                ebnd.ChangeRequests.Add(CH);


                                Customer ordercustomerpersheet = GetCustomerinfo(sl, i, or.OrderID);
                                if (ordercustomerpersheet != null)
                                {
                                    ebnd.Customers.Add(ordercustomerpersheet);
                                    ebnd.SaveChanges();
                                }
                                else errors.Add("Customer info is missing");

                                CustomerContact PartnerContacts = GetPartnerContacts(sl, i, ordercustomerpersheet.CustomerID);
                                if (PartnerContacts != null)
                                {
                                    ebnd.CustomerContacts.Add(PartnerContacts);
                                    ebnd.SaveChanges();
                                }
                                else errors.Add("Partner contact info is missing");

                                CustomerContact CustomerrContacts = GetCustomerContacts(sl, i, ordercustomerpersheet.CustomerID);
                                if (CustomerrContacts != null)
                                {
                                    ebnd.CustomerContacts.Add(CustomerrContacts);
                                    ebnd.SaveChanges();
                                }
                                else errors.Add("Customer contact info is missing");


                                OrderMappingContact mappingcontact = GetMappingcontact(sl, i, or.OrderID);
                                if (mappingcontact != null)
                                {
                                    ebnd.OrderMappingContacts.Add(mappingcontact);
                                    ebnd.SaveChanges();
                                }


                                #region companies
                                //use customerinformationfor tyhe first company
                                SLDocument CompanySheet = new SLDocument(@"D:\Bulkorderstemplate.xlsx", "Companies");


                                //first comopany
                                CustomerCompany cc1;
                                List<CustomerCompany> companies = new List<CustomerCompany>();
                                if (CompanySheet.GetCellValueAsString(i, 2) == "Yes")
                                {
                                    cc1 = GetCompanyfromCustomer(sl, i, ordercustomerpersheet.CustomerID);
                                }
                                else
                                {
                                    cc1 = GetCompanyH(CompanySheet, i, ordercustomerpersheet.CustomerID, 3);
                                }

                                //second company
                                CustomerCompany cc2 = GetCompanyH(CompanySheet, i, ordercustomerpersheet.CustomerID, 8);
                                CustomerCompany cc3 = GetCompanyH(CompanySheet, i, ordercustomerpersheet.CustomerID, 13);
                                CustomerCompany cc4 = GetCompanyH(CompanySheet, i, ordercustomerpersheet.CustomerID, 18);
                                CustomerCompany cc5 = GetCompanyH(CompanySheet, i, ordercustomerpersheet.CustomerID, 23);
                                CustomerCompany cc6 = GetCompanyH(CompanySheet, i, ordercustomerpersheet.CustomerID, 28);

                                if (cc1 != null) { ebnd.CustomerCompanies.Add(cc1); companies.Add(cc1); }
                                if (cc2 != null) { ebnd.CustomerCompanies.Add(cc2); companies.Add(cc2); }
                                if (cc3 != null) { ebnd.CustomerCompanies.Add(cc3); companies.Add(cc3); }
                                if (cc4 != null) { ebnd.CustomerCompanies.Add(cc4); companies.Add(cc4); }
                                if (cc5 != null) { ebnd.CustomerCompanies.Add(cc5); companies.Add(cc5); }
                                if (cc6 != null) { ebnd.CustomerCompanies.Add(cc6); companies.Add(cc6); }

                                /*
                                Console.WriteLine("company " + cc1.CompName + "created forCustomer " + ordercustomerpersheet.CustomerName + "created  for order ID " + or.OrderID);
                                logger.Info("company " + cc1.CompName + "created forCustomer " + ordercustomerpersheet.CustomerName + "created  for order ID " + or.OrderID);

                                */
                                if (companies != null && companies.Count != 0)
                                {
                                    if (companies.Where(c => c.CompName != null).Select(c => c.CompName).Distinct().Count() != companies.Where(c => c.CompName != null).Count())
                                        errors.Add("duplicate company names for customer: " + ordercustomerpersheet.CustomerName);
                                }
                                else
                                    errors.Add("At least one company should be added for customer: " + ordercustomerpersheet.CustomerName);

                                ebnd.SaveChanges();

                                CompanySheet.Dispose();
                                #endregion

                                #region carrier
                                SLDocument carriersheet = new SLDocument(@"D:\Bulkorderstemplate.xlsx", "Carriers");
                                //First carrier
                                //get column index for first carrier                          
                                Carrier Ca = GetCarriers(carriersheet, i, or.OrderID, 2);

                                if (Ca != null)
                                {
                                    ebnd.Carriers.Add(Ca);
                                    ebnd.SaveChanges();
                                    carriers.Add(Ca);
                                    Ca.CarrierCode = Ca.CarrierID.ToString();
                                    //Carplans
                                    List<CarrierPlaneType> carplans = GetCarrierPlans(carriersheet, i, Ca.CarrierID, 21);
                                    if (carplans != null && carplans.Count() != 0)
                                    {
                                        foreach (var plan in carplans)
                                        {
                                            if (plan.PlanID != 0)
                                            {
                                                ebnd.CarrierPlaneTypes.Add(plan);
                                                if (plan.PlanID == 10)
                                                {
                                                    Ca.CarrierType = carriersheet.GetCellValueAsString(i + 1, 30);
                                                    if (Ca.CarrierType == null)
                                                        errors.Add("Other plan type was not specified for carrier" + Ca.CarrierName);
                                                }
                                                ebnd.SaveChanges();
                                            }
                                        }
                                    }
                                    else errors.Add(Ca.CarrierName + "Plan type is required");
                                    //Car companies

                                    if (Ca.Allcomp != true)
                                    {
                                        List<int> carcomps = GetCarrierCompanies(carriersheet, i, Ca.CarrierID, 15);
                                        foreach (int num in carcomps)
                                        {
                                            CompCarrier compcarrier = new CompCarrier();
                                            compcarrier.CarrierID = Ca.CarrierID;
                                            if (cc1 != null && num == 1)
                                            {
                                                compcarrier.CompID = cc1.CompID;
                                            }

                                            if (cc2 != null && num == 2)
                                            {
                                                compcarrier.CompID = cc2.CompID;
                                            }
                                            if (cc3 != null && num == 3)
                                            {
                                                compcarrier.CompID = cc3.CompID;
                                            }
                                            if (cc4 != null && num == 4)
                                            {
                                                compcarrier.CompID = cc4.CompID;
                                            }
                                            if (cc5 != null && num == 5)
                                            {
                                                compcarrier.CompID = cc5.CompID;
                                            }
                                            if (cc6 != null && num == 6)
                                            {
                                                compcarrier.CompID = cc6.CompID;
                                            }
                                            ebnd.CompCarriers.Add(compcarrier);
                                            ebnd.SaveChanges();
                                        }
                                    }
                                    else
                                    {
                                        foreach (CustomerCompany comp in companies)
                                        {
                                            CompCarrier compcarrier = new CompCarrier();
                                            compcarrier.CarrierID = Ca.CarrierID;
                                            compcarrier.CompID = comp.CompID;
                                            ebnd.CompCarriers.Add(compcarrier);
                                            ebnd.SaveChanges();
                                        }

                                    }
                                }

                                //Second carrier (col 22)
                                Carrier Ca2 = GetCarriers(carriersheet, i, or.OrderID, 31);
                                if (Ca2 != null)
                                {
                                    ebnd.Carriers.Add(Ca2);
                                    ebnd.SaveChanges();
                                    Ca2.CarrierCode = Ca2.CarrierID.ToString();
                                    carriers.Add(Ca2);
                                    List<CarrierPlaneType> carplans2 = GetCarrierPlans(carriersheet, i, Ca2.CarrierID, 50);
                                    if (carplans2 != null && carplans2.Count() != 0)
                                    {
                                        foreach (var plan in carplans2)
                                        {
                                            if (plan.PlanID != 0)
                                            {
                                                ebnd.CarrierPlaneTypes.Add(plan);
                                                if (plan.PlanID == 10)
                                                {
                                                    Ca2.CarrierType = carriersheet.GetCellValueAsString(i + 1, 59);
                                                    ebnd.SaveChanges();
                                                    if (Ca2.CarrierType == null)
                                                        errors.Add("Other plan type was not specified for carrier" + Ca2.CarrierName);
                                                }
                                            }
                                        }
                                    }
                                    else
                                        errors.Add(Ca2.CarrierName + "Plan type is required");

                                    if (Ca2.Allcomp != true)
                                    {
                                        List<int> carcomps = GetCarrierCompanies(carriersheet, i, Ca2.CarrierID, 44);
                                        foreach (int num in carcomps)
                                        {
                                            CompCarrier compcarrier = new CompCarrier();
                                            compcarrier.CarrierID = Ca2.CarrierID;
                                            if (cc1 != null && num == 1)
                                            {
                                                compcarrier.CompID = cc1.CompID;
                                            }
                                            if (cc2 != null && num == 2)
                                            {
                                                compcarrier.CompID = cc2.CompID;
                                            }
                                            if (cc3 != null && num == 3)
                                            {
                                                compcarrier.CompID = cc3.CompID;
                                            }
                                            if (cc4 != null && num == 4)
                                            {
                                                compcarrier.CompID = cc4.CompID;
                                            }
                                            if (cc5 != null && num == 5)
                                            {
                                                compcarrier.CompID = cc5.CompID;
                                            }
                                            if (cc6 != null && num == 6)
                                            {
                                                compcarrier.CompID = cc6.CompID;
                                            }
                                            ebnd.CompCarriers.Add(compcarrier);
                                            ebnd.SaveChanges();
                                        }
                                    }
                                    else
                                    {
                                        foreach (CustomerCompany comp in companies)
                                        {
                                            CompCarrier compcarrier = new CompCarrier();
                                            compcarrier.CarrierID = Ca2.CarrierID;
                                            compcarrier.CompID = comp.CompID;
                                            ebnd.CompCarriers.Add(compcarrier);
                                            ebnd.SaveChanges();
                                        }

                                    }

                                }


                                //3rd carrier
                                Carrier Ca3 = GetCarriers(carriersheet, i, or.OrderID, 60);
                                if (Ca3 != null)
                                {
                                    ebnd.Carriers.Add(Ca3);
                                    ebnd.SaveChanges();
                                    Ca3.CarrierCode = Ca3.CarrierID.ToString();
                                    carriers.Add(Ca3);
                                    List<CarrierPlaneType> carplans3 = GetCarrierPlans(carriersheet, i, Ca3.CarrierID, 79);
                                    if (carplans3 != null && carplans3.Count() != 0)
                                    {
                                        foreach (var plan in carplans3)
                                        {
                                            if (plan.PlanID != 0)
                                            {
                                                ebnd.CarrierPlaneTypes.Add(plan);
                                                if (plan.PlanID == 10)
                                                {
                                                    Ca3.CarrierType = carriersheet.GetCellValueAsString(i + 1, 88);
                                                    ebnd.SaveChanges();
                                                    if (Ca3.CarrierType == null)
                                                        errors.Add("Other plan type was not specified for carrier" + Ca3.CarrierName);
                                                }
                                            }
                                        }
                                    }
                                    else
                                        errors.Add(Ca3.CarrierName + "Plan type is required");

                                    if (Ca3.Allcomp != true)
                                    {
                                        List<int> carcomps = GetCarrierCompanies(carriersheet, i, Ca3.CarrierID, 73);
                                        foreach (int num in carcomps)
                                        {
                                            CompCarrier compcarrier = new CompCarrier();
                                            compcarrier.CarrierID = Ca3.CarrierID;
                                            if (cc1 != null && num == 1)
                                            {
                                                compcarrier.CompID = cc1.CompID;
                                            }
                                            if (cc2 != null && num == 2)
                                            {
                                                compcarrier.CompID = cc2.CompID;
                                            }
                                            if (cc3 != null && num == 3)
                                            {
                                                compcarrier.CompID = cc3.CompID;
                                            }
                                            if (cc4 != null && num == 4)
                                            {
                                                compcarrier.CompID = cc4.CompID;
                                            }
                                            if (cc5 != null && num == 5)
                                            {
                                                compcarrier.CompID = cc5.CompID;
                                            }
                                            if (cc6 != null && num == 6)
                                            {
                                                compcarrier.CompID = cc6.CompID;
                                            }
                                            ebnd.CompCarriers.Add(compcarrier);
                                            ebnd.SaveChanges();
                                        }
                                    }
                                    else
                                    {
                                        foreach (CustomerCompany comp in companies)
                                        {
                                            CompCarrier compcarrier = new CompCarrier();
                                            compcarrier.CarrierID = Ca3.CarrierID;
                                            compcarrier.CompID = comp.CompID;
                                            ebnd.CompCarriers.Add(compcarrier);
                                            ebnd.SaveChanges();
                                        }

                                    }
                                }

                                //4th carrier
                                Carrier Ca4 = GetCarriers(carriersheet, i, or.OrderID, 89);
                                if (Ca4 != null)
                                {
                                    ebnd.Carriers.Add(Ca4);
                                    ebnd.SaveChanges();
                                    Ca4.CarrierCode = Ca4.CarrierID.ToString();
                                    carriers.Add(Ca4);
                                    List<CarrierPlaneType> carplans4 = GetCarrierPlans(carriersheet, i, Ca4.CarrierID, 108);
                                    if (carplans4 != null && carplans4.Count() != 0)
                                    {
                                        foreach (var plan in carplans4)
                                        {
                                            if (plan.PlanID != 0)
                                            {
                                                ebnd.CarrierPlaneTypes.Add(plan);
                                                ebnd.SaveChanges();
                                                if (plan.PlanID == 10)
                                                {
                                                    Ca4.CarrierType = carriersheet.GetCellValueAsString(i + 1, 117);
                                                    ebnd.SaveChanges();
                                                    if (Ca4.CarrierType == null)
                                                        errors.Add("Other plan type was not specified for carrier" + Ca4.CarrierName);
                                                }
                                            }
                                        }
                                    }
                                    else errors.Add(Ca4.CarrierName + "Plan type is required");

                                    if (Ca4.Allcomp != true)
                                    {
                                        List<int> carcomps = GetCarrierCompanies(carriersheet, i, Ca4.CarrierID, 102);
                                        foreach (int num in carcomps)
                                        {
                                            CompCarrier compcarrier = new CompCarrier();
                                            compcarrier.CarrierID = Ca4.CarrierID;
                                            if (cc1 != null && num == 1)
                                            {
                                                compcarrier.CompID = cc1.CompID;
                                            }
                                            if (cc2 != null && num == 2)
                                            {
                                                compcarrier.CompID = cc2.CompID;
                                            }
                                            if (cc3 != null && num == 3)
                                            {
                                                compcarrier.CompID = cc3.CompID;
                                            }
                                            if (cc4 != null && num == 4)
                                            {
                                                compcarrier.CompID = cc4.CompID;
                                            }
                                            if (cc5 != null && num == 5)
                                            {
                                                compcarrier.CompID = cc5.CompID;
                                            }
                                            if (cc6 != null && num == 6)
                                            {
                                                compcarrier.CompID = cc6.CompID;
                                            }
                                            ebnd.CompCarriers.Add(compcarrier);
                                            ebnd.SaveChanges();
                                        }
                                    }
                                    else
                                    {
                                        foreach (CustomerCompany comp in companies)
                                        {
                                            CompCarrier compcarrier = new CompCarrier();
                                            compcarrier.CarrierID = Ca4.CarrierID;
                                            compcarrier.CompID = comp.CompID;
                                            ebnd.CompCarriers.Add(compcarrier);
                                            ebnd.SaveChanges();
                                        }

                                    }
                                }


                                //5th carrier
                                Carrier Ca5 = GetCarriers(carriersheet, i, or.OrderID, 94);
                                if (Ca5 != null)
                                {
                                    ebnd.Carriers.Add(Ca5);
                                    ebnd.SaveChanges();
                                    Ca5.CarrierCode = Ca5.CarrierID.ToString();
                                    carriers.Add(Ca5);
                                    List<CarrierPlaneType> carplans5 = GetCarrierPlans(carriersheet, i, Ca5.CarrierID, 137);
                                    if (carplans5 != null && carplans5.Count() != 0)
                                    {
                                        foreach (var plan in carplans5)
                                        {
                                            if (plan.PlanID != 0)
                                            {
                                                ebnd.CarrierPlaneTypes.Add(plan);
                                                if (plan.PlanID == 10)
                                                {
                                                    Ca5.CarrierType = carriersheet.GetCellValueAsString(i + 1, 146);
                                                    ebnd.SaveChanges();
                                                    if (Ca5.CarrierType == null)
                                                        errors.Add("Other plan type was not specified for carrier" + Ca5.CarrierName);
                                                }
                                            }
                                        }
                                    }
                                    else errors.Add(Ca5.CarrierName + "Plan type is required");
                                    if (Ca5.Allcomp != true)
                                    {
                                        List<int> carcomps = GetCarrierCompanies(carriersheet, i, Ca5.CarrierID, 131);
                                        foreach (int num in carcomps)
                                        {
                                            CompCarrier compcarrier = new CompCarrier();
                                            compcarrier.CarrierID = Ca5.CarrierID;
                                            if (cc1 != null && num == 1)
                                            {
                                                compcarrier.CompID = cc1.CompID;
                                            }
                                            if (cc2 != null && num == 2)
                                            {
                                                compcarrier.CompID = cc2.CompID;
                                            }
                                            if (cc3 != null && num == 3)
                                            {
                                                compcarrier.CompID = cc3.CompID;
                                            }
                                            if (cc4 != null && num == 4)
                                            {
                                                compcarrier.CompID = cc4.CompID;
                                            }
                                            if (cc5 != null && num == 5)
                                            {
                                                compcarrier.CompID = cc5.CompID;
                                            }
                                            if (cc6 != null && num == 6)
                                            {
                                                compcarrier.CompID = cc6.CompID;
                                            }
                                            ebnd.CompCarriers.Add(compcarrier);
                                            ebnd.SaveChanges();
                                        }
                                    }
                                    else
                                    {
                                        foreach (CustomerCompany comp in companies)
                                        {
                                            CompCarrier compcarrier = new CompCarrier();
                                            compcarrier.CarrierID = Ca5.CarrierID;
                                            compcarrier.CompID = comp.CompID;
                                            ebnd.CompCarriers.Add(compcarrier);
                                            ebnd.SaveChanges();
                                        }

                                    }
                                }



                                //6th carrier
                                Carrier Ca6 = GetCarriers(carriersheet, i, or.OrderID, 147);
                                if (Ca6 != null)
                                {
                                    ebnd.Carriers.Add(Ca6);
                                    ebnd.SaveChanges();
                                    Ca6.CarrierCode = Ca6.CarrierID.ToString();
                                    carriers.Add(Ca6);
                                    List<CarrierPlaneType> carplans6 = GetCarrierPlans(carriersheet, i, Ca6.CarrierID, 166);
                                    if (carplans6 != null && carplans6.Count() != 0)
                                    {
                                        foreach (var plan in carplans6)
                                        {
                                            if (plan.PlanID != 0)
                                            {
                                                ebnd.CarrierPlaneTypes.Add(plan);
                                                ebnd.SaveChanges();
                                                if (plan.PlanID == 10)
                                                {
                                                    Ca6.CarrierType = carriersheet.GetCellValueAsString(i + 1, 175);
                                                    ebnd.SaveChanges();
                                                    if (Ca6.CarrierType == null)
                                                        errors.Add("Other plan type was not specified for carrier" + Ca6.CarrierName);
                                                }
                                            }
                                        }
                                    }
                                    else errors.Add(Ca5.CarrierName + "Plan type is required");
                                    if (Ca6.Allcomp != true)
                                    {
                                        List<int> carcomps = GetCarrierCompanies(carriersheet, i, Ca6.CarrierID, 160);
                                        foreach (int num in carcomps)
                                        {
                                            CompCarrier compcarrier = new CompCarrier();
                                            compcarrier.CarrierID = Ca6.CarrierID;
                                            if (cc1 != null && num == 1)
                                            {
                                                compcarrier.CompID = cc1.CompID;
                                            }
                                            if (cc2 != null && num == 2)
                                            {
                                                compcarrier.CompID = cc2.CompID;
                                            }
                                            if (cc3 != null && num == 3)
                                            {
                                                compcarrier.CompID = cc3.CompID;
                                            }
                                            if (cc4 != null && num == 4)
                                            {
                                                compcarrier.CompID = cc4.CompID;
                                            }
                                            if (cc5 != null && num == 5)
                                            {
                                                compcarrier.CompID = cc5.CompID;
                                            }
                                            if (cc6 != null && num == 6)
                                            {
                                                compcarrier.CompID = cc6.CompID;
                                            }
                                            ebnd.CompCarriers.Add(compcarrier);
                                            ebnd.SaveChanges();
                                        }
                                    }
                                    else
                                    {
                                        foreach (CustomerCompany comp in companies)
                                        {
                                            CompCarrier compcarrier = new CompCarrier();
                                            compcarrier.CarrierID = Ca6.CarrierID;
                                            compcarrier.CompID = comp.CompID;
                                            ebnd.CompCarriers.Add(compcarrier);
                                            ebnd.SaveChanges();
                                        }
                                    }



                                }

                                if (carriers != null && carriers.Count != 0)
                                {
                                    if (carriers.Where(c => c.CarrierName != null).Select(c => c.CarrierName).Distinct().Count() != carriers.Where(c => c.CarrierName != null).Count()) errors.Add("duplicate carrier names ");
                                    errors.Add("duplicate carrier for customer: " + ordercustomerpersheet.CustomerName);
                                }
                                else
                                    errors.Add("At least one carrier should be added for customer: " + ordercustomerpersheet.CustomerName);



                                #endregion

                                #region validations
                                if (ChkCarrierValidations(carriers) != null)
                                    errors.AddRange(ChkCarrierValidations(carriers));
                                if (ChkCustomerValidations(ordercustomerpersheet) != null)
                                    errors.AddRange(ChkCustomerValidations(ordercustomerpersheet));

                                if (errors.Count() > 0)
                                {
                                    errors.Insert(0, "Order was not created for the customer" + ordercustomerpersheet.CustomerName);
                                    foreach (string er in errors)
                                    {
                                        logger.Error(er);
                                    }
                                    throw new Exception("Order was not created for the customer" + ordercustomerpersheet.CustomerName);
                                }
                                #endregion

                                ebnd.SaveChanges();
                                Console.WriteLine("order successfuly created fro customer: " + ordercustomerpersheet.CustomerName);
                                logger.Info("order successfuly created fro customer: " + ordercustomerpersheet.CustomerName);
                                ts.Complete();


                            }

                        }

                        catch (DbEntityValidationException ex)
                        {

                            if (ex.InnerException != null) logger.Error("Inner Exce=: " + ex.InnerException);
                            foreach (var eve in ex.EntityValidationErrors)
                            {
                                foreach (var ve in eve.ValidationErrors)
                                {
                                    logger.Error("validation property error in " + ve.PropertyName + ", validation error messagee=: " + ve.ErrorMessage);
                                }
                            }
                            ts.Dispose();
                        }

                        catch (Exception ex)
                        {
                            if (ex.InnerException != null) logger.Error("Inner Exce=: " + ex.InnerException);
                            logger.Error("Err mes=: " + ex.Message);
                            logger.Error("Stack trace=: " + ex.StackTrace);
                            ts.Dispose();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                if (ex.InnerException != null) logger.Error("Inner Exce=: " + ex.InnerException);
                logger.Error("Err mes=: " + ex.Message);
                logger.Error("Stack trace=: " + ex.StackTrace);
            }
        }
        #endregion

        public static void test()
        {
            Dictionary<long, BMCarrierCodes_View> abc = new Dictionary<long, BMCarrierCodes_View>();
            List<string> eBNCarrierPlans = new List<string>();
            Dictionary<string, string> mCCDic = new Dictionary<string, string>();
            int newcustcount = 0;

            var oFS = File.OpenText(@"D:\testcars.txt");
            //var oFS = File.OpenText(@"D:\bmall\Prod-GroupsAndCarriers.txt");
            string josn = oFS.ReadToEnd();

            dynamic items = JsonConvert.DeserializeObject(josn);

            SLDocument mCCDoc = new SLDocument(@"D:\MCarrierCodes.xlsx");
            SLWorksheetStatistics mCCst = mCCDoc.GetWorksheetStatistics();

            int mrowno = mCCst.NumberOfRows;

            for (int i = 1; i <= mrowno; i++)
            {
                mCCDic.Add(mCCDoc.GetCellValueAsString(i, 1).Trim().ToLower(), mCCDoc.GetCellValueAsString(i, 2).Trim().ToLower());
            }

            using (eBNPortal_ProductionEntities ebnDB = new eBNPortal_ProductionEntities())
            {
                abc = ebnDB.BMCarrierCodes_View.Select(cc => new { id = cc.CarrierId, bmcc = cc })
                    .ToDictionary(d => d.id, d => d.bmcc);

                eBNCarrierPlans = ebnDB.PlanTypes.Select(c => c.PlanTypeName.Trim().ToLower()).ToList();
            }

            var GrpsProp = ((Newtonsoft.Json.Linq.JObject)items)["Groups"].Where(g => !g["oldvalueid"].ToString().ToUpper().Contains("CA")).ToList();
            var cobraGrps = ((Newtonsoft.Json.Linq.JObject)items)["Groups"].Where(g => g["oldvalueid"].ToString().ToUpper().Contains("CA")).ToList();
            var CarProp = ((Newtonsoft.Json.Linq.JObject)items)["Carriers"];
            //  var allCars = CarProp.GroupBy(s => s["CarrierCode"]).ToList();
            // int allCount = ((Newtonsoft.Json.Linq.JObject)items)["Groups"].Count();


            //New Customers
            // var newGrpsProp = GrpsProp.Where(c =>!abc.Values.Any(bc => bc.CustomerCode == c["CustomerCode"].ToString())).ToList();

            //New Carriers || New Plans
            var newCarsProp = CarProp.Where(c => !abc.Values.Any(bc => bc.CustomerCode == c["GroupNumber"].ToString()
                                              && bc.SCarrierName.Trim().ToLower() == c["CarrierName"].ToString().Trim().ToLower()
                                              && ((bc.CarrierPlans != null
                                              && ((bc.CarrierPlans.Contains(',') && !bc.CarrierPlans.ToLower().Split(',').Contains(c["CarrierPlans"].ToString().ToLower().Trim()))
                                              || bc.CarrierPlans.Trim().ToLower() == c["CarrierPlans"].ToString().Trim().ToLower())
                                              ) || (bc.CarrierPlans == null && !eBNCarrierPlans.Contains(c["CarrierPlans"].ToString().Trim().ToLower())))

                                              && ((bc.CarrierCode.Contains('|') && bc.CarrierCode.Split('|').Contains(c["CarrierCode"].ToString().Trim()))
                                              || bc.CarrierCode.Trim().ToLower() == c["CarrierCode"].ToString().Trim().ToLower())
                                              ));

            List<string> customerCodes = newCarsProp.Select(c => c["GroupNumber"].ToString().Trim()).Distinct().ToList();


            #region 
            foreach (string custCode in customerCodes)
            {
                long orderId = 0;
                List<long> CarIds = new List<long>();

                var grpItem = GrpsProp.Where(c => c["CustomerCode"].ToString().Trim() == custCode).FirstOrDefault();

                bool newCustomer = false;
                if (grpItem == null)
                {
                    logger.Error("Not Exist: This customer code does not exist in the valid code list " + custCode);
                    continue;
                }
                string custName = grpItem["CustomerName"].ToString();
                string allCompId = grpItem["oldvalueid"].ToString();
                logger.Info("custCode");

                using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required,
                                                                        new TransactionOptions()
                                                                        {
                                                                            IsolationLevel = IsolationLevel.ReadUncommitted
                                                                        }))
                {

                    using (ebnd = new eBNPortal_ProductionEntities())
                    {
                        try
                        {
                            bool newcustomer = false;
                            Customer cs = ebnd.Customers.FirstOrDefault(c => c.CustomerCode == custCode && c.Order.PartnerID == 10291);
                            Order order;
                            ChangeRequest CH;
                            if (cs != null)
                            {
                                newcustomer = false;
                                logger.Info("customer already exist: " + custName + " , customercode: " + custCode);
                                order = ebnd.Orders.Where(o => o.OrderID == cs.OrderID).FirstOrDefault();
                                CH = AddChangeRequest(order.OrderID, 3);
                                ebnd.ChangeRequests.Add(CH);
                                ebnd.SaveChanges();
                                // for now
                                //continue;
                            }
                            else
                            {
                                newcustomer = true;
                                logger.Info("New Customer: " + custName);
                                order = CreateOrder(10291);
                                ebnd.Orders.Add(order);
                                ebnd.SaveChanges();

                                if (order != null)
                                {
                                    Order_History oh = CreateHistory(order.OrderID);
                                    ebnd.Order_History.Add(oh);
                                    ebnd.SaveChanges();

                                    CH = AddChangeRequest(order.OrderID);
                                    ebnd.ChangeRequests.Add(CH);
                                    ebnd.SaveChanges();
                                    #region customer
                                    cs = new Customer();
                                    cs.CustomerName = custName;

                                    JToken fTax = grpItem["FedralTaxID"];

                                    if (fTax != null)
                                        cs.FTaxID = fTax.ToString();

                                    cs.CustomerCode = custCode;
                                    cs.EmployeesNumber = Convert.ToInt32(grpItem["EmployeesNumber"]);//.ToString();
                                    cs.OrderEmployeesNo = Convert.ToInt32(grpItem["EmployeesNumber"]);
                                    cs.OrderID = order.OrderID;
                                    ebnd.Customers.Add(cs);
                                    ebnd.SaveChanges();

                                    //                        //custom Field Group ID
                                    OrderCustomField Oc = new OrderCustomField();
                                    Oc.OrderID = order.OrderID;
                                    Oc.CutomfieldID = 98;  //testing 106 , demo 98
                                    Oc.CustomFieldValue = custCode;
                                    ebnd.OrderCustomFields.Add(Oc);
                                    ebnd.SaveChanges();

                                    //Partner contacts
                                    CustomerContact pcs = new CustomerContact();
                                    pcs.CustomerID = cs.CustomerID;
                                    pcs.IsPartner = true;

                                    JToken fNToken = grpItem["firstname"];
                                    JToken lNToken = grpItem["lastname"];
                                    JToken eToken = grpItem["email"];

                                    string fulName = string.Empty;
                                    fulName = fNToken != null ? lNToken != null ? fNToken.ToString() + " " + lNToken.ToString()
                                              : (fNToken != null ? fNToken.ToString() : lNToken.ToString()) : string.Empty;


                                    pcs.CustomerContactName = fulName;

                                    if (eToken != null)
                                        pcs.CustomerContactEmail = eToken.ToString();

                                    ebnd.CustomerContacts.Add(pcs);
                                    ebnd.SaveChanges();
                                    #endregion

                                    #region companies
                                    //check if  custcompany exist b4 
                                    CustomerCompany custComp = ebnd.CustomerCompanies.FirstOrDefault(c => c.CustomerID == cs.CustomerID && c.CompName == custName);
                                    if (custComp == null)
                                    {
                                        custComp = new CustomerCompany();
                                        custComp.CustomerID = cs.CustomerID;
                                        custComp.CompName = custName;

                                        ebnd.CustomerCompanies.Add(custComp);
                                        ebnd.SaveChanges();
                                    }
                                    var customercomp = cobraGrps.Where(c => c["oldvalueid"].ToString().ToUpper() == "CA" + custCode).ToList();
                                    foreach (var childcomp in customercomp)
                                    {
                                        string childcompName = childcomp["CustomerName"].ToString();
                                        CustomerCompany custCompany = ebnd.CustomerCompanies.FirstOrDefault(c => c.CustomerID == cs.CustomerID && c.CompName == childcompName);
                                        if (custCompany != null)
                                        {
                                            custCompany = new CustomerCompany();
                                            custComp.CustomerID = cs.CustomerID;
                                            custComp.CompName = childcompName;
                                            ebnd.CustomerCompanies.Add(custCompany);
                                            ebnd.SaveChanges();

                                            Oc.CustomFieldValue += "," + childcomp["CustomerCode"].ToString();
                                        }
                                    }

                                }
                                #endregion

                            }
                            #region carriers
                            int savedcarcount = 0;
                            var custCars = CarProp.Where(c => c["GroupNumber"].ToString() == custCode)
                                .GroupBy(c => c["CarrierCode"].ToString().Trim())
                                .ToList();



                            foreach (var carItem in custCars)
                            {
                                bool mCC = false;
                                string mCarrierCode = string.Empty;
                                string mCarrierCodeGrp = string.Empty;
                                string carPlan = carItem.First()["CarrierCode"].ToString().ToLower().Trim();
                                string carName = carItem.First()["CarrierName"].ToString().Trim();
                                string vendorId = carItem.First()["CarrierCode"].ToString().Trim();

                                try
                                {
                                    #region check mcc
                                    if (mCCDic.ContainsKey(carItem.First()["CarrierCode"].ToString().ToLower().Trim()))
                                    {
                                        mCC = true;
                                        mCarrierCode = vendorId;//carItem.First()["CarrierCode"].ToString().Trim();
                                        mCarrierCodeGrp = mCCDic[mCarrierCode.ToLower()];
                                        var mCarrierCodeArr = mCarrierCodeGrp.Split('|');
                                        string planName = carItem.First()["CarrierPlans"].ToString().Trim();

                                        long carierId = ebnd.BMCarrierCodes_View.ToList().Where(c => c.CustomerCode == custCode &&
                                            ((c.CarrierCode.Contains("|") && c.CarrierCode.Split('|').Any(cc => mCarrierCodeArr.Contains(cc.ToLower()))) || mCarrierCodeArr.Contains(c.CarrierCode.ToLower()))).Select(c => c.CarrierId).FirstOrDefault();
                                        if (carierId != 0) // just add the new carriercode
                                        {
                                            OrderCustomField carVendor = ebnd.OrderCustomFields.Where(c => c.CarrierID == carierId && c.CutomfieldID == 99).FirstOrDefault();
                                            if (carVendor.CustomFieldValue.Contains('|'))
                                            {
                                                var carVendorArr = carVendor.CustomFieldValue.Split('|');
                                                if (!carVendorArr.Contains(vendorId))
                                                {
                                                    carVendor.CustomFieldValue = carVendor.CustomFieldValue + "|" + mCarrierCode;

                                                    logger.Info(string.Format("Adding MCC only for carrierid: {0}, carrir code value: {1}", carierId, mCarrierCode));
                                                    ebnd.SaveChanges();
                                                }
                                            }
                                            else if (carVendor.CustomFieldValue.ToLower() != vendorId.ToLower())
                                            {
                                                carVendor.CustomFieldValue = carVendor.CustomFieldValue + "|" + mCarrierCode;

                                                logger.Info(string.Format("Adding MCC only for carrierid: {0}, carrir code value: {1}", carierId, mCarrierCode));
                                                ebnd.SaveChanges();
                                            }

                                            #region carrierpltypes

                                            string[] mPlansArray = carItem.Select(c => c["CarrierPlans"].ToString().Trim()).ToArray();
                                            if (mPlansArray.Count() > 0)
                                                foreach (var planchild in mPlansArray)
                                                {
                                                    try
                                                    {
                                                        int mplanid = ebnd.PlanTypes.Where(p => p.PlanTypeName == planchild).Select(p => p.PlanID).FirstOrDefault();
                                                        if (mplanid != 0)
                                                        {
                                                            CarrierPlaneType carplans = ebnd.CarrierPlaneTypes.Where(c => c.carrierID == carierId && c.PlanID == mplanid).FirstOrDefault();

                                                            if (carplans == null)
                                                            {
                                                                carplans = new CarrierPlaneType();
                                                                carplans.PlanID = mplanid;
                                                                carplans.carrierID = carierId;
                                                                ebnd.CarrierPlaneTypes.Add(carplans);
                                                                ebnd.SaveChanges();
                                                            }
                                                        }

                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        logger.Error("Error adding plan types for Order: " + custName);
                                                        if (ex.InnerException != null) logger.Error("Inner Exce=: " + ex.InnerException);
                                                        logger.Error("Err mes=: " + ex.Message);
                                                        logger.Error("End exception details");
                                                    }
                                                }
                                            #endregion

                                            continue;
                                        }
                                    }
                                    #endregion


                                    // Carrier car = null;

                                    //   if (newcustomer)
                                    Carrier car = ebnd.Carriers.Where(c => c.CarrierName.Trim() == carName.Trim() && c.OrderID == cs.OrderID).FirstOrDefault();

                                    if (car != null)
                                    {
                                        logger.Info("Connection already exist " + car.ProjectName);
                                        // ts.Dispose();
                                        continue;
                                    }

                                    eBNCarrier eBNcar = ebnd.eBNCarriers.FirstOrDefault(c => c.CarrierName == carName);
                                    if (eBNcar == null)
                                    {
                                        eBNcar = new eBNCarrier();
                                        eBNcar.CarrierName = carName;
                                        ebnd.eBNCarriers.Add(eBNcar);
                                        ebnd.SaveChanges();
                                    }

                                    #region carrier
                                    car = new Carrier();
                                    car.CarrierName = carName.Trim();

                                    car.ProjectName = cs.CustomerName.Trim() + "_" + car.CarrierName.Trim();
                                    car.OrderID = cs.OrderID;
                                    car.Allcomp = true;

                                    try
                                    {
                                        car.FedralTaxID = carItem.First()["FedralTaxID"].ToString().Trim();
                                    }
                                    catch (Exception exc)
                                    {
                                        logger.Error("error adding car ftax, ignored for car: " + carName);
                                    }
                                    car.StatusId = 1;
                                    car.MultitenantStatus = false;
                                    car.eBNCarrierId = eBNcar.CarrierId;

                                    ebnd.Carriers.Add(car);
                                    ebnd.SaveChanges();
                                    car.CarrierCode = car.CarrierID.ToString();
                                    ebnd.SaveChanges();
                                    #endregion
                                    #region carrierpltypes

                                    string[] plansArray = carItem.Select(c => c["CarrierPlans"].ToString().Trim()).ToArray();
                                    if (plansArray.Count() > 0)
                                        foreach (var planchild in plansArray)
                                        {
                                            try
                                            {
                                                int planid = ebnd.PlanTypes.Where(p => p.PlanTypeName == planchild).Select(p => p.PlanID).FirstOrDefault();
                                                if (planid != 0)
                                                {
                                                    CarrierPlaneType carplans = new CarrierPlaneType();
                                                    carplans.PlanID = planid;
                                                    carplans.carrierID = car.CarrierID;
                                                    ebnd.CarrierPlaneTypes.Add(carplans);
                                                    ebnd.SaveChanges();
                                                }

                                            }
                                            catch (Exception ex)
                                            {
                                                logger.Error("Error adding plan types for Order: " + custName);
                                                if (ex.InnerException != null) logger.Error("Inner Exce=: " + ex.InnerException);
                                                logger.Error("Err mes=: " + ex.Message);
                                                logger.Error("End exception details");
                                            }
                                        }
                                    #endregion


                                    #region carriercustomfields //vendor id
                                    OrderCustomField octvendor = new OrderCustomField();
                                    octvendor.CarrierID = car.CarrierID;
                                    octvendor.OrderID = car.OrderID;
                                    octvendor.CustomFieldValue = vendorId;
                                    octvendor.CutomfieldID = 99;  //107 testing , 99 demo
                                    ebnd.OrderCustomFields.Add(octvendor);
                                    ebnd.SaveChanges();
                                    #endregion

                                    #region carrier com
                                    List<CustomerCompany> custcomp = ebnd.CustomerCompanies.Where(c => c.CustomerID == cs.CustomerID).ToList();
                                    foreach (CustomerCompany comp in custcomp)
                                    {
                                        CompCarrier compcarrier = new CompCarrier();
                                        compcarrier.CarrierID = car.CarrierID;
                                        compcarrier.CompID = comp.CompID;
                                        ebnd.CompCarriers.Add(compcarrier);
                                        ebnd.SaveChanges();
                                    }
                                    #endregion


                                    ebnd.SaveChanges();
                                    savedcarcount++;
                                    logger.Info("Connection added : " + car.ProjectName);
                                    CarIds.Add(car.CarrierID);

                                }
                                catch (DbEntityValidationException e)
                                {

                                    logger.Error("Error: adding Order for Customer: " + custName);

                                    foreach (var eve in e.EntityValidationErrors)
                                    {
                                        logger.Error(string.Format("Error: Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                               eve.Entry.Entity.GetType().Name, eve.Entry.State));
                                        foreach (var ve in eve.ValidationErrors)
                                        {
                                            logger.Error(string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                               ve.PropertyName, ve.ErrorMessage));
                                        }
                                    }
                                    // ts.Dispose();
                                }
                                catch (Exception ex)
                                {
                                    logger.Error("Error: adding Order : " + custName);
                                    if (ex.InnerException != null) logger.Error("Inner Exce=: " + ex.InnerException);
                                    logger.Error("Err mes=: " + ex.Message);
                                    logger.Error("Stack trace=: " + ex.StackTrace);
                                    //   ts.Dispose();
                                }

                            }

                            ebnd.SaveChanges();
                            cs.ConnectionsNumber = ebnd.Carriers.Where(c => c.OrderID == cs.OrderID && c.StatusId == 1).Count();
                            ebnd.SaveChanges();

                            logger.Error("no. of saved carriers: " + savedcarcount);
                            if (newcustomer && savedcarcount > 0)
                            {
                                ts.Complete();
                                logger.Info("New Customer: successfuly saved new customer: " + custName + " , custcode: " + custCode);
                                newcustcount++;
                            }
                            else if ((!newcustomer) && savedcarcount > 0)
                            {
                                ts.Complete();
                                logger.Info("Add Connection(s): successfuly saved new connection(s): " + custName + " , custcode: " + custCode);
                                newcustcount++;
                            }
                            // ts.Complete();
                            else
                            {
                                logger.Error("Error: order was not saved for customer: " + custName);
                                //ts.Dispose();
                            }
                            #endregion

                            orderId = order.OrderID;

                        }
                        catch (Exception ex)
                        {
                            logger.Error("Error: adding Order : " + custName);
                            if (ex.InnerException != null) logger.Error("Inner Exce=: " + ex.InnerException);
                            logger.Error("Err mes=: " + ex.Message);
                            logger.Error("Stack trace=: " + ex.StackTrace);
                            //    ts.Dispose();
                        }
                    }

                }

                #region QB
                //foreach (var carId in CarIds)
                //{
                //    try
                //    {

                //        bool QBcreated = QBase.AddNewProject_NewLib(orderId, carId);
                //        if (QBcreated)
                //            logger.Info("successfully added in QB: orderId: " + orderId + " , for carrier ID: " + carId);
                //        else
                //            logger.Info("Error adding in QB: orderId: " + orderId + " , for carrier ID: " + carId);
                //    }

                //    catch (Exception ex)
                //    {
                //        logger.Error("Error: adding in QB: orderId: " + orderId + " , for carrier ID: " + carId);
                //        if (ex.InnerException != null) logger.Error("Inner Exce=: " + ex.InnerException);
                //        logger.Error("Err mes=: " + ex.Message);
                //        logger.Error("Stack trace=: " + ex.StackTrace);
                //        //   ts.Dispose();
                //    }
                //}
                #endregion

            }
            logger.Info("new Customers = " + newcustcount);
            #endregion



        }


        public static void CancelOrder(long orderid, DateTime EffectiveDate)
        {

            using (eBNPortal_ProductionEntities ebnDB = new eBNPortal_ProductionEntities())
            {
                ChangeRequest ChReqobj = new ChangeRequest();
                ChReqobj.OrderId = orderid;
                ChReqobj.ChRequestTypeId = 1;
                ChReqobj.ChRequestStatusId = 2;
                ChReqobj.ChRequestDate = EffectiveDate;
                ChReqobj.ChRequestSubmitDate = EffectiveDate;
                ChReqobj.UserId = 51;
                ebnDB.ChangeRequests.Add(ChReqobj);
                ebnDB.SaveChanges();

                // add order cancel event in order history table
                Order_History oh = new Order_History();
                oh.OrderID = orderid;
                oh.UserID = 51;
                oh.EventTypeID = 7;
                oh.Event_Date = EffectiveDate;
                ebnDB.Order_History.Add(oh);
                ebnDB.SaveChanges();


                //long Carr
                var orderobj = ebnDB.Orders.Where(o => o.OrderID == orderid && o.StatusId == 2).FirstOrDefault();
                long partnerID = 10291;
                List<Carrier> projects = orderobj.Carriers.Where(x => x.StatusId != 3).ToList();
                foreach (Carrier proj in projects)
                {
                    proj.StatusId = 3;
                    proj.CancelReasonID = 3;
                    proj.QBCancelled = false;
                    proj.EffectiveDate = EffectiveDate;
                    ChangeRequestProject ChReqproj = new ChangeRequestProject();
                    ChReqproj.ChRequestId = ChReqobj.ID;
                    ChReqproj.ProjectId = proj.CarrierID;
                    ebnDB.ChangeRequestProjects.Add(ChReqproj);
                    ebnDB.SaveChanges();

                    bool disablesch = ProjectScheduler(false, proj.CarrierID);
                }

                //Order orderobj = ebnDB.Orders.SingleOrDefault(X => X.OrderID == orderid);
                orderobj.StatusId = 3;
                orderobj.EffectiveDate = EffectiveDate;
                ebnDB.SaveChanges();

                // add approve order cancel event in order history table
                Order_History oh8 = new Order_History();
                oh8.OrderID = orderid;
                oh8.UserID = 51;
                oh8.EventTypeID = 8;
                oh8.Event_Date = EffectiveDate;
                ebnDB.Order_History.Add(oh8);
                ebnDB.SaveChanges();

            }

            // Cancel Data in Quickbase
            QBase quickbaseobj = new QBase();
            quickbaseobj.cancelOrder(orderid);


        }

        public static bool ProjectScheduler(bool enable, long carrierID)
        {
            bool result = false;
            try
            {
                System.Net.ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(delegate { return true; });
                using (var ebnServiceClient = new ebnServiceReference.EbnServiceClient())
                {
                    var xElement = new System.Xml.Linq.XElement("User",
                        new System.Xml.Linq.XElement("VIPUserName", "bassemadmin"),
                        new System.Xml.Linq.XElement("Name", "PartnerUser"),
                        new System.Xml.Linq.XElement("Partner", "VIPAccessPartner"),
                        new System.Xml.Linq.XElement("Customer", "VIPAccessCustomer"));
                    var buffer = System.Text.Encoding.UTF8.GetBytes(xElement.ToString(System.Xml.Linq.SaveOptions.DisableFormatting));
                    buffer = EncryptData(buffer);
                    ebnServiceClient.ClientCredentials.UserName.UserName = Convert.ToBase64String(buffer);//"UKiOJ7/Ckw5NTLGZMm8S4jKwyK9vY5wPnAtW7RJq7vwUNoqFrgqFVeApgqncvjYeOX04zogOPFXMmCGOmixN1ZFxFNtUWMD5+8VIpL1QZjeG7VRKfFKqAFbF3tj72GtZGqmQV4mbC65JxrFWeWiGEwHmEOFT56kjaE3+40UIGwxfY1MuTtcK+n207k8CPPc1z6Sh72HQi40PhsJrmiGW2yY6LHrcWfaS51G2Y16mtJB/rmR2BdF2zc4nb7dHN4CcsotToSIyUt6WviEVDm5tl42sarimbx4+0hnLXNmCitXsSEQ/szf7Hum7zJcjktcTc/t5mlqG/kHoAElG+3n17Q==";
                    xElement = new System.Xml.Linq.XElement("User",
                        new System.Xml.Linq.XElement("VIPUserName", "bassemadmin"),
                        new System.Xml.Linq.XElement("Password", "P@rtnerP@ssw0rd"));
                    buffer = System.Text.Encoding.UTF8.GetBytes(xElement.ToString(System.Xml.Linq.SaveOptions.DisableFormatting));
                    buffer = EncryptData(buffer);
                    ebnServiceClient.ClientCredentials.UserName.Password = Convert.ToBase64String(buffer);
                    result = ebnServiceClient.EnableProjectScheduler(carrierID, false);
                }
                return result;
            }
            catch (Exception ex)
            {
                logger.Error("error disabling project");
                logger.Info(ex.Message);
                // logger.log.Info("User=: " + uu.Identity.Name);
                if (ex.InnerException != null) logger.Error("Inner Exce=: " + ex.InnerException);
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber();
                logger.Error("Err mes=: " + ex.Message);
                logger.Error("Stack trace=: " + ex.StackTrace);
                return result;
            }
        }
        private static byte[] EncryptData(byte[] secretData)
        {
            var rsaCryptoServiceProvider = new System.Security.Cryptography.RSACryptoServiceProvider();
            rsaCryptoServiceProvider.FromXmlString("<RSAKeyValue><Modulus>kFqqx6bomtqmvhiSobhWqHpdkAK0grMEV/FdpX6N5TpDRWaD27haJdhJozjM/pNmBWnGfCFgVy49+iArlf5XMXl8rnL9izfHuzFSr/zbnf+mMlLZqxpmGQg6HO3HiHVj5cjlG52r5PB/rrSFEjV0oDaXjH4j6gpC5tJICOA9Lx8=</Modulus><Exponent>AQAB</Exponent><P>yWa1gwZH8GaGhIwxvwdT9ydShDJgWHJJagjpaz3UKr7FMZDHFGSvsgxcJaQF4DQxbrVx2jdlbtm1if1PYKMeXw==</P><Q>t3zhVi2nt/JsGWs0i4Z5H0+kfu7oAclZnYJ5KhipuTHM3msslZzIOM0h6KVQ6O9c8sN5RPXm9oaUg+hLrA4nQQ==</Q><DP>wwS1ll4qotpkP00Rjoyl/ZkSCfhN2tcvx4FBpRqFq652e/xZCaJFjv7w63HcTrG7fBwuVsN1cNVXOHsUtdq9uQ==</DP><DQ>p9WYoCU+pmkeK9n9xCoKnHNS+bA5k3jDeemgPrs0c+tzg3bw3yD7m8k23QBqE8budDgMsuFik9jh/A39ObHwgQ==</DQ><InverseQ>qfT0qUgprsf4RoOoN8YBBFtGnZbvG1GvBhKigDzVtlGsKkXlhhdKyR3JZZ1AWR2WsmQlT287hDk/l4JzfcOz8g==</InverseQ><D>dOnp/Y/SPnEusTHHuNFK5mNM2fFG78A7mVp0ZTAtjmV0zIWt78vMv3AAnADKDrmk3GeCCVEi7RkXuzhI9M+tH74xh+rXXXV63VpziimuWrAxv9DeXqSS/0WoPmaZD10kUb+mnuP7uRblNeu83xxoQJAiocH+7Uj4/M0VhyvZ2oE=</D></RSAKeyValue>");
            var lastBlockLength = secretData.Length % 58;
            var blockCount = secretData.Length / 58;
            var hasLastBlock = false;
            if (lastBlockLength != 0)
            {
                blockCount += 1;
                hasLastBlock = true;
            }
            var result = new byte[blockCount * 128];
            var currentBlock = new byte[58];
            for (var blockIndex = 0; blockIndex <= blockCount - 1; blockIndex++)
            {
                int thisBlockLength;

                //If this is the last block and we have a remainder, 
                //then set the length accordingly

                if (blockCount == blockIndex + 1 && hasLastBlock)
                {
                    thisBlockLength = lastBlockLength;
                    currentBlock = new byte[lastBlockLength];
                }
                else
                    thisBlockLength = 58;

                var startChar = blockIndex * 58;

                //Define the block that we will be working on
                //byte[] currentBlock = new byte[thisBlockLength];
                Array.Copy(secretData, startChar, currentBlock, 0, thisBlockLength);

                //Encrypt the current block and append it to the result stream
                var encryptedBlock = rsaCryptoServiceProvider.Encrypt(currentBlock, false);

                var originalResultLength = 128 * blockIndex; //result.Length;
                                                             //Array.Resize(ref result, originalResultLength + encryptedBlock.Length);
                                                             //encryptedBlock.CopyTo(result, originalResultLength)
                encryptedBlock.CopyTo(result, originalResultLength);
            }

            return result;
        }


        public static void testaaa()
        {

            using (eBNPortal_ProductionEntities ebndb = new eBNPortal_ProductionEntities())
            {

                using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required,
                                             new TransactionOptions()
                                             {
                                                 IsolationLevel = IsolationLevel.ReadUncommitted
                                             }))
                {

                    Order order = new Order
                    {
                        OrderDate = DateTime.Now,
                        PartnerID = 150,  //bm testting 10362, demo 10292
                        IsSubmitted = true
                    };
                    long Max = ebnd.Orders.Max(C => C.OrderNumber);
                    order.OrderNumber = Max + 1;
                    order.StatusId = 2;
                    order.OrderType = 1;
                    order.SubmitDate = DateTime.Now;

                    ebndb.Orders.Add(order);
                    ebndb.SaveChanges();

                    Console.WriteLine("cc" + order.OrderID);


                }
            }

        }


        public static void updateCustomer(long customerId, string newCustomerName)
        {
            logger.Info("try updating customer: " + customerId);
            List<string> qbProjIds = new List<string>();
            try
            {
                using (eBNPortal_ProductionEntities eBNDB = new eBNPortal_ProductionEntities())
                {
                    //var connObjs  = eBNDB.Customers
                    //                    .Join(ebnd.Carriers, c => c.OrderID, ca => ca.OrderID, (c, ca) => new { ca.CarrierID , c.CustomerName , ca.CarrierName})
                    //                    .Select(s => s).ToList();

                    Customer cs = eBNDB.Customers.Where(c => c.CustomerID == customerId).FirstOrDefault();
                    List<string> carrierNames = eBNDB.Carriers.Where(c => c.OrderID == cs.OrderID && c.StatusId == 1).Select(c => c.CarrierName).ToList();

                    foreach (var connObj in carrierNames)
                    {
                        string qbProjId = QBase.QBget_projid(1, cs.CustomerName, connObj, null);

                        var updatedfields = new Dictionary<string, string>()
                                       {
                                        { "Customer Name", newCustomerName },
                                        {"Name" ,  newCustomerName+"_"+connObj}
                                       };
                        QBase.UpdateListOfProjectFields(qbProjId, LoadProp.connection, null, null, null, updatedfields, null);

                    }
                    cs.CustomerName = newCustomerName;
                    eBNDB.SaveChanges();

                    eBNDB.QBCarrierPhases.Where(c => c.CustomerName == cs.CustomerName).ToList().ForEach(x => { x.CustomerName = newCustomerName; });
                    eBNDB.Carriers.Where(c => c.OrderID == cs.OrderID).ToList().ForEach(x => { x.ProjectName = newCustomerName+"_"+ x.CarrierName; });

                    eBNDB.SaveChanges();

                }

            }
            catch (Exception ex)
            {
                logger.Error("Error: updating customer: "+ customerId);
                if (ex.InnerException != null) logger.Error("Inner Exce=: " + ex.InnerException);
                logger.Error("Err mes=: " + ex.Message);
                logger.Error("Stack trace=: " + ex.StackTrace);
                //    ts.Dispose();
            }
        }

    }
    public class MTRec
    {
        public string carrierCode { get; set; }
        public string templateId { get; set; }
        public string gexcludedGrpNos { get; set; }
        public string fileType { get; set; }
    }


    public class CarrierRec
    {
        public string carrierCode { get; set; }
        public string carrierName { get; set; }
        public string FedralTaxID { get; set; }
        public string GroupNumber { get; set; }
        public string CarrierPlans { get; set; }
    }
}
