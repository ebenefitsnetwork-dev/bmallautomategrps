﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text;
using Intuit.QuickBase.Client;
using System.Configuration;
using eBNQuickBaseLibrary;
using log4net;
using System.Data;
using System.Diagnostics;

namespace AutomateBultOrders
{
    public class QBase
    {

        static QuickBaseConnection connection = LoadProp.connection;
        static string userName = LoadProp.username;
        static string pwd = LoadProp.Password;

        static ILog logger = LoadProp.logger;
        static eBNPortal_ProductionEntities ebnd = LoadProp.ebnd;
        static string Environment = LoadProp.Environment;

        public static void CreateQB()
        {
            logger.Info("start adding in QB");

            //  List<Carrier> cars = ebnd.Carriers.Where(c => c.StatusId == 1 &&(c.MultitenantId == 259162 || c.MultitenantId == 259163 || c.MultitenantId == 259243)).ToList();// && (!c.QBCreated.HasValue || c.QBCreated.Value == false) && c.Order.OrderType == 1 && (c.Order.PartnerID == 10292 || c.Order.PartnerID == 10291) && c.MultitenantId == 308043 &&  (!c.QBCreated.HasValue || c.QBCreated.Value == false)).ToList();

            DateTime fixDate = new DateTime(2019, 09, 25);
            List<long> orderIds = ebnd.ChangeRequests.Where(c => c.ChRequestDate.Value >= fixDate).Select(c => (long)c.OrderId).ToList();

            List<Carrier> cars = ebnd.Carriers.Where(c => c.StatusId == 1 && orderIds.Contains(c.OrderID)).ToList();// && (!c.QBCreated.HasValue || c.QBCreated.Value == false) && c.Order.OrderType == 1 && (c.Order.PartnerID == 10292 || c.Order.PartnerID == 10291) && c.MultitenantId == 308043 &&  (!c.QBCreated.HasValue || c.QBCreated.Value == false)).ToList();

            foreach (Carrier car in cars)
            {                       
                bool QBcreated = AddNewProject_NewLib(car.OrderID, car.CarrierID);
                if (QBcreated)
                    logger.Info(string.Format( "successfully added in QB connection name {0} , connection id {1}: " ,  car.ProjectName, car.CarrierID));
                else
                    logger.Info("Error adding in QB connection name: " + car.ProjectName);

            }
        }
        public static bool AddNewProject_NewLib(long orderID, long carrierID)
        {
            try
            {
                using (eBNPortal_ProductionEntities ebnDB = new eBNPortal_ProductionEntities())
                {
                    List<string> Files = new List<string>();
                    bool formuploaded = false;
                    Order orderObj = ebnDB.Orders.SingleOrDefault(X => X.OrderID == orderID);
                    Carrier CarrierObj = ebnDB.Carriers.SingleOrDefault(Z => Z.CarrierID == carrierID);
                    List<CarrierPlaneType> carrplans = ebnDB.CarrierPlaneTypes.Where(V => V.carrierID == CarrierObj.CarrierID).ToList();
                    Customer CustomerObj = ebnDB.Customers.Include("CustomerContacts").SingleOrDefault(Y => Y.OrderID == orderID);
                    string partnerObjName = ebnDB.Partners.FirstOrDefault(F => F.PartnerId == orderObj.PartnerID).PartnerName;
                    string ResellerObjName = ebnDB.Partners.Where(F => F.PartnerId == orderObj.ResellerID).Select(F => F.PartnerName).FirstOrDefault();
                    //   string ResellerObjName = ebnDB.Partners.FirstOrDefault(F => F.PartnerId == orderObj.ResellerID). ?? null;

                    List<PlanType> projectplantypes = new List<PlanType>();





                    foreach (CarrierPlaneType carplanobj in carrplans)
                    {
                        PlanType plantypeobj = ebnDB.PlanTypes.SingleOrDefault(C => C.PlanID == carplanobj.PlanID);

                        projectplantypes.Add(plantypeobj);
                    }




                    ////////////// Add Data To project
                    using (var connection = new QuickBaseConnection("santeon.quickbase.com", userName, pwd))
                    {
                        #region order data
                        string proTable = ConfigurationManager.AppSettings["projecttable"];

                        string CustomerName = CustomerObj.CustomerName;
                        string CarrierName = CarrierObj.CarrierName;

                        var projectQuery = new QuickBaseMultipleQuery { AndOrOperator = QuickBaseAndOrOperator.AND };
                        projectQuery.Queries.Add(new QuickBaseSingleQuery { FieldId = 6, ComparisonOperator = QuickBaseComparisonOperator.EX, Value = CustomerObj.CustomerName.ToLower() + "_" + CarrierObj.CarrierName.ToLower() });
                        projectQuery.Queries.Add(new QuickBaseSingleQuery { FieldId = 74, ComparisonOperator = QuickBaseComparisonOperator.XCT, Value = "Cancelled" });

                        var customerCarrierQuery = new QuickBaseMultipleQuery { AndOrOperator = QuickBaseAndOrOperator.AND };
                        customerCarrierQuery.Queries.Add(new QuickBaseSingleQuery { FieldId = 18, ComparisonOperator = QuickBaseComparisonOperator.EX, Value = CustomerObj.CustomerName.ToLower() });
                        customerCarrierQuery.Queries.Add(new QuickBaseSingleQuery { FieldId = 19, ComparisonOperator = QuickBaseComparisonOperator.EX, Value = CarrierObj.CarrierName.ToLower() });
                        customerCarrierQuery.Queries.Add(new QuickBaseSingleQuery { FieldId = 74, ComparisonOperator = QuickBaseComparisonOperator.XCT, Value = "Cancelled" });


                        if (orderObj.OrderType == 1)
                        {
                            projectQuery.Queries.Add(new QuickBaseSingleQuery { FieldId = 69, ComparisonOperator = QuickBaseComparisonOperator.XCT, Value = "OE" });
                            projectQuery.Queries.Add(new QuickBaseSingleQuery { FieldId = 69, ComparisonOperator = QuickBaseComparisonOperator.XCT, Value = "DF" });

                            customerCarrierQuery.Queries.Add(new QuickBaseSingleQuery { FieldId = 69, ComparisonOperator = QuickBaseComparisonOperator.XCT, Value = "OE" });
                            customerCarrierQuery.Queries.Add(new QuickBaseSingleQuery { FieldId = 69, ComparisonOperator = QuickBaseComparisonOperator.XCT, Value = "DF" });
                        }

                        var mainQuery = new QuickBaseMultipleQuery { AndOrOperator = QuickBaseAndOrOperator.OR };
                        mainQuery.Queries.Add(projectQuery);
                        mainQuery.Queries.Add(customerCarrierQuery);


                        var datacount = connection.DoQueryCount(proTable, mainQuery);

                        if (datacount != 0)
                        {
                            CarrierObj.QBCreated = true;
                            ebnDB.SaveChanges();
                            logger.Info("connection already exist in QB " + CustomerObj.CustomerName + "_" + CarrierObj.CarrierName);
                            return true; // to emove carrier check to create QB
                        }
                        //else   {              // try to find the project that is linked

                        //         if (CarrierObj.MultitenantId != null && CarrierObj.MultitenantId != 0 )


                        //          }


                        List<QuickBaseFieldRecord> newRecord = new List<QuickBaseFieldRecord>();
                        newRecord.Add(new QuickBaseFieldRecord { Name = "Name", FieldValue = CustomerObj.CustomerName + "_" + CarrierObj.CarrierName });
                        newRecord.Add(new QuickBaseFieldRecord { Name = "Partner Name", FieldValue = partnerObjName }); //System.Security.SecurityElement.Escape(partnerObj.PartnerName); // "Please Choose..";// // Has to be The same in the QuickBase lookup field
                        newRecord.Add(new QuickBaseFieldRecord { Name = "Customer Name", FieldValue = CustomerObj.CustomerName }); //System.Security.SecurityElement.Escape(CustomerObj.CustomerName);
                        newRecord.Add(new QuickBaseFieldRecord { Name = "Carrier Name", FieldValue = CarrierObj.CarrierName });//System.Security.SecurityElement.Escape(CarrierObj.CarrierName);

                        if (!string.IsNullOrEmpty(ResellerObjName))
                            newRecord.Add(new QuickBaseFieldRecord { Name = "Sub-Partner", FieldValue = ResellerObjName });

                        if (CarrierObj.GroupNumber != null && CarrierObj.GroupNumber != "")
                            newRecord.Add(new QuickBaseFieldRecord { Name = "Group/Policy Number", FieldValue = CarrierObj.GroupNumber });

                        if (CarrierObj.CobraMembers.HasValue)
                        {
                            if (CarrierObj.CobraMembers.Value)
                                newRecord.Add(new QuickBaseFieldRecord { Name = "Include COBRA enrollment on file", FieldValue = "1" });
                        }
                        if (orderObj.Comments != null)
                            newRecord.Add(new QuickBaseFieldRecord { Name = "Notes", FieldValue = orderObj.Comments });

                        if (CustomerObj.FTaxID != null)
                            newRecord.Add(new QuickBaseFieldRecord { Name = "Customer Fed Tax ID", FieldValue = CustomerObj.FTaxID.ToString() });
                        if (!string.IsNullOrEmpty(CustomerObj.CustomerCode))
                            newRecord.Add(new QuickBaseFieldRecord { Name = "CustomerID", FieldValue = CustomerObj.CustomerCode });
                        if (CustomerObj.EmployeesNumber.HasValue)
                            newRecord.Add(new QuickBaseFieldRecord { Name = "No. of Employees", FieldValue = CustomerObj.EmployeesNumber.Value.ToString() });
                        if (CustomerObj.Companynumber.HasValue)
                            newRecord.Add(new QuickBaseFieldRecord { Name = "No. of Companies", FieldValue = CustomerObj.Companynumber.Value.ToString() });
                        if (CustomerObj.PrimayAdd != null)
                            newRecord.Add(new QuickBaseFieldRecord { Name = "Customer Street Address", FieldValue = CustomerObj.PrimayAdd });
                        if (CustomerObj.PrimaryCity != null)
                            newRecord.Add(new QuickBaseFieldRecord { Name = "Customer City", FieldValue = CustomerObj.PrimaryCity });
                        if (CustomerObj.stateID != null)
                            newRecord.Add(new QuickBaseFieldRecord { Name = "Customer State", FieldValue = ebnDB.eBN_States.SingleOrDefault(C => C.StateID == CustomerObj.stateID).State_Name });
                        if (CustomerObj.PrimaryCode != null)
                            newRecord.Add(new QuickBaseFieldRecord { Name = "Customer Zip Code", FieldValue = CustomerObj.PrimaryCode });
                        //OE req 2016 July
                        //if (CustomerObj.EnrollementTypeID != null)
                        //{
                        //    newRecord.Add(new QuickBaseFieldRecord { Name = "Enrollment Type", FieldValue = ebnDB.EnrollmentTypes.Where(c => c.ID == CustomerObj.EnrollementTypeID).Select(c => c.Type).FirstOrDefault() });
                        //}
                        if (orderObj.OrderNumber != null)
                            newRecord.Add(new QuickBaseFieldRecord { Name = "Order No", FieldValue = orderObj.OrderNumber.ToString() });
                        if (CarrierObj.CarrierCode != null)
                            newRecord.Add(new QuickBaseFieldRecord { Name = "Project No", FieldValue = CarrierObj.CarrierCode.ToString() });

                        // MT Feed
                        try
                        {
                            if (CarrierObj.MultitenantId != null)
                            {
                                string parentfeed = ebnDB.Carriers.Where(c => c.CarrierID == CarrierObj.MultitenantId).Select(c => c.ProjectName).FirstOrDefault();
                                newRecord.Add(new QuickBaseFieldRecord { Name = "Parent Feed", FieldValue = parentfeed });
                            }
                            if (CarrierObj.MultitenantStatus.HasValue && CarrierObj.MultitenantStatus.Value == true)
                            {
                                newRecord.Add(new QuickBaseFieldRecord { Name = "IsMultitenant", FieldValue = 1 });
                            }
                        }
                        catch (Exception ex)
                        {
                            logger.Error("error adding MT feed into QB");
                            logger.Error("error " + ex.Message);
                        }

                        #endregion
                        #region submit date
                        if (CarrierObj.SubmitDate.HasValue)
                            newRecord.Add(new QuickBaseFieldRecord { Name = "Order Received Date", FieldValue = CarrierObj.SubmitDate.Value.Date.ToString("MM-dd-yyyy") });  // order Date"01-09-2014"
                        else if (orderObj.SubmitDate.HasValue)
                            newRecord.Add(new QuickBaseFieldRecord { Name = "Order Received Date", FieldValue = orderObj.SubmitDate.Value.Date.ToString("MM-dd-yyyy") });  // order Date"01-09-2014"
                        #endregion
                        #region carrier status
                        string carrierstatus = "";
                        if (CarrierObj.StatusId == 3)
                            carrierstatus = "Cancelled";
                        else
                        {
                            DateTime oct = new DateTime(2019, 10, 1);
                            // for BMAll samir
                            //carrierstatus = "Production";
                            //newRecord.Add(new QuickBaseFieldRecord { Name = "Migration", FieldValue = 1 });

                            //if(CarrierObj.MultitenantId == 259243)
                            //    newRecord.Add(new QuickBaseFieldRecord { Name = "First Production File Date", FieldValue = oct.ToString("MM-dd-yyyy") });
                            //else
                            //    newRecord.Add(new QuickBaseFieldRecord { Name = "First Production File Date", FieldValue = DateTime.Now.ToString("MM-dd-yyyy") });



                            newRecord.Add(new QuickBaseFieldRecord { Name = "NonVIPStatus2", FieldValue = carrierstatus });
                        }
                        newRecord.Add(new QuickBaseFieldRecord { Name = "Status", FieldValue = carrierstatus });

                        #endregion
                        #region project type EDI
                        if (orderObj.OrderType == 1)
                        {
                            if (CarrierObj.CarrierPlaneTypes.Where(C => C.PlanType.PlanID == 2 || C.PlanType.PlanID == 7 || C.PlanType.PlanID == 8).Count() > 0)
                            {
                                newRecord.Add(new QuickBaseFieldRecord { Name = "Project Type", FieldValue = "834" });
                            }
                            else
                            {
                                newRecord.Add(new QuickBaseFieldRecord { Name = "Project Type", FieldValue = "Layout" });
                            }
                            newRecord.Add(new QuickBaseFieldRecord { Name = "Priority", FieldValue = "Normal" });

                            if (CustomerObj.PlanYearStartDate.HasValue)
                                newRecord.Add(new QuickBaseFieldRecord { Name = "Plan Year Start Date", FieldValue = CustomerObj.PlanYearStartDate.Value.Date.ToString("MM-dd-yyyy") });
                            if (CustomerObj.CustomerDataETA.HasValue)//Customer Dataset ETA
                                newRecord.Add(new QuickBaseFieldRecord { Name = "Customer Dataset Readiness ETA", FieldValue = CustomerObj.CustomerDataETA.Value.Date.ToString("MM-dd-yyyy") });
                            if (!string.IsNullOrEmpty(CustomerObj.EnrollementMonth))
                                if (CustomerObj.EnrollementMonth.ToLower() != "null")
                                {
                                    newRecord.Add(new QuickBaseFieldRecord { Name = "OE Month", FieldValue = CustomerObj.EnrollementMonth });
                                }
                        }
                        #endregion
                        #region plan types

                        bool rec401K = false;

                        foreach (PlanType plantypeobj in projectplantypes)
                        {
                            if (plantypeobj.PlanTypeName == "401k census")
                            {
                                newRecord.Add(new QuickBaseFieldRecord { id = 50, FieldValue = "1" });
                                if (orderObj.OrderType != 2)
                                {
                                    if (newRecord.Where(k => k.Name == "Project Type").Count() == 0)
                                        newRecord.Add(new QuickBaseFieldRecord { Name = "Project Type", FieldValue = "Layout" });
                                }
                                // newRecord.Add(new QuickBaseFieldRecord { Name = "401K Type", FieldValue = "Census" });
                                newRecord.Add(new QuickBaseFieldRecord { id = 161, FieldValue = "Census" });

                            }
                            else if (plantypeobj.PlanTypeName == "401k contribution")
                            {

                                newRecord.Add(new QuickBaseFieldRecord { id = 50, FieldValue = "1" });
                                if (orderObj.OrderType != 2)
                                {
                                    if (newRecord.Where(k => k.Name == "Project Type").Count() > 0)
                                    {
                                        //int valindex = newRecord.FindIndex(k => k.Name == "Project Type");
                                        //newRecord[valindex].FieldValue = "401k" ;
                                        newRecord.Where(k => k.Name == "Project Type").FirstOrDefault().FieldValue = "401k";
                                    }
                                    else
                                    {
                                        newRecord.Add(new QuickBaseFieldRecord { Name = "Project Type", FieldValue = "401k" });
                                    }
                                    rec401K = true;
                                }
                                //newRecord.Add(new QuickBaseFieldRecord { Name = "401K Type", FieldValue = "Contribution" });
                                newRecord.Add(new QuickBaseFieldRecord { id = 161, FieldValue = "Contribution" });

                            }
                            else
                            {
                                rec401K = false;
                                newRecord.Add(new QuickBaseFieldRecord { Name = plantypeobj.PlanTypeName, FieldValue = "1" });
                                if (plantypeobj.PlanTypeName == "Other")
                                {
                                    if (CarrierObj.CarrierType != null)
                                        newRecord.Add(new QuickBaseFieldRecord { Name = "Othertxt", FieldValue = CarrierObj.CarrierType });
                                }
                            }

                        }
                        #endregion

                        var projtablerec = connection.NewRecord(proTable, newRecord);
                        string recID = projtablerec.Element("rid").Value.ToString(); // get record id 

                        #region  NEw "not migrated"
                        
                                                #region customer contacts

                                                ////Add Data to customer Contact
                                                string conttable = ConfigurationManager.AppSettings["contacttable"];
                                                string[] NameList;

                                                foreach (CustomerContact custcont in CustomerObj.CustomerContacts)
                                                {
                                                    List<QuickBaseFieldRecord> contRecord = new List<QuickBaseFieldRecord>();
                                                    //contRecord = contacttable.NewRecord();
                                                    contRecord.Add(new QuickBaseFieldRecord { Name = "Related Project", FieldValue = recID });
                                                    if (custcont.CustomerContactName != null)
                                                        NameList = custcont.CustomerContactName.Split(' ');
                                                    else
                                                        NameList = new string[] { " ", " ", " " };

                                                    if (NameList[0] != null)
                                                        contRecord.Add(new QuickBaseFieldRecord { Name = "First Name", FieldValue = NameList[0] });
                                                    if (NameList[NameList.Length - 1] != null)
                                                        contRecord.Add(new QuickBaseFieldRecord { Name = "Last Name", FieldValue = NameList[NameList.Length - 1] });


                                                    if (custcont.IsPartner)
                                                        contRecord.Add(new QuickBaseFieldRecord { Name = "Title / Job Function", FieldValue = "Order" });
                                                    else
                                                        contRecord.Add(new QuickBaseFieldRecord { Name = "Title / Job Function", FieldValue = "Customer" });

                                                    if (custcont.CustomerContactPhone == null || custcont.CustomerContactPhone == "")
                                                        contRecord.Add(new QuickBaseFieldRecord { Name = "Phone", FieldValue = "0000000" });
                                                    else
                                                        contRecord.Add(new QuickBaseFieldRecord { Name = "Phone", FieldValue = custcont.CustomerContactPhone });
                                                    if (custcont.CustomerContactEmail == null || custcont.CustomerContactEmail == "")
                                                        contRecord.Add(new QuickBaseFieldRecord { Name = "Email", FieldValue = "mail@mail.com" });
                                                    else
                                                        contRecord.Add(new QuickBaseFieldRecord { Name = "Email", FieldValue = custcont.CustomerContactEmail });

                                                    var CustomerContactrec = connection.NewRecord(conttable, contRecord);
                                                }
                                                #endregion

                                            #region bmallcontacts

                                                string bMallEnv = ConfigurationManager.AppSettings["bMallEnv"];

                                                if (bMallEnv == "Sync") {
                                                    var bmContacts = ebnDB.QBContacts;
                                                    foreach (var contact in bmContacts)
                                                {
                                                    var OMcontRecord = new List<QuickBaseFieldRecord>();

                                                    OMcontRecord.Add(new QuickBaseFieldRecord { Name = "Related Project", FieldValue = recID });
                                                    OMcontRecord.Add(new QuickBaseFieldRecord { Name = "First Name", FieldValue = contact.FirstName });
                                                    OMcontRecord.Add(new QuickBaseFieldRecord { Name = "Last Name", FieldValue = contact.LastName });
                                                    OMcontRecord.Add(new QuickBaseFieldRecord { Name = "Title / Job Function", FieldValue = "Order Contact" });

                                                    if (contact.Phone == null || contact.Phone == "")
                                                        OMcontRecord.Add(new QuickBaseFieldRecord { Name = "Phone", FieldValue = "0000000" });
                                                    else
                                                        OMcontRecord.Add(new QuickBaseFieldRecord { Name = "Phone", FieldValue = contact.Phone });
                                                    if (contact.Email == null || contact.Email == "")
                                                        OMcontRecord.Add(new QuickBaseFieldRecord { Name = "Email", FieldValue = "mail@mail.com" });
                                                    else
                                                        OMcontRecord.Add(new QuickBaseFieldRecord { Name = "Email", FieldValue = contact.Email });

                                                    connection.NewRecord(conttable, OMcontRecord);
                                                }
                                                   }
                                                #endregion
                                            #region carrier contact

                                                //if (CarrierObj.ContactName != null)
                                                //{
                                                List<QuickBaseFieldRecord> carcontRecord = new List<QuickBaseFieldRecord>();
                                            //  contRecord = contacttable.NewRecord();
                                            carcontRecord.Add(new QuickBaseFieldRecord { Name = "Related Project", FieldValue = recID });

                                            if (CarrierObj.ContactName != null)
                                                NameList = CarrierObj.ContactName.Split(' ');
                                            else
                                                NameList = new string[] { " ", " ", " " };

                                            if (NameList[0] != null)
                                                carcontRecord.Add(new QuickBaseFieldRecord { Name = "First Name", FieldValue = NameList[0] });
                                            if (NameList[NameList.Length - 1] != null)
                                            {
                                                carcontRecord.Add(new QuickBaseFieldRecord { Name = "Last Name", FieldValue = NameList[NameList.Length - 1] });
                                                carcontRecord.Add(new QuickBaseFieldRecord { Name = "Title / Job Function", FieldValue = "Carrier" });
                                            }
                                            if (CarrierObj.ContactPhone == null || CarrierObj.ContactPhone == "")
                                                carcontRecord.Add(new QuickBaseFieldRecord { Name = "Phone", FieldValue = "0000000" });
                                            else
                                                carcontRecord.Add(new QuickBaseFieldRecord { Name = "Phone", FieldValue = CarrierObj.ContactPhone });
                                            if (CarrierObj.ContactEmail == null || CarrierObj.ContactEmail == "")
                                                carcontRecord.Add(new QuickBaseFieldRecord { Name = "Email", FieldValue = "mail@mail.com" });
                                            else
                                                carcontRecord.Add(new QuickBaseFieldRecord { Name = "Email", FieldValue = CarrierObj.ContactEmail });

                                            try
                                            {
                                                var carCustomerContactrec = connection.NewRecord(conttable, carcontRecord);
                                            }
                                            catch (Exception e)
                                            {
                                                logger.Error("Error add carrier contact ");
                                                if (e.InnerException != null) logger.Error("Inner Exce=: " + e.InnerException);
                                                logger.Error("Err mes=: " + e.Message);
                                                logger.Error("Stack trace=: " + e.StackTrace);
                                                return false;
                                            }
                                            //}
                                            #endregion
                                            #region Order Mapping contact
                                            ////adding mapping Contact
                                            OrderMappingContact OMC = ebnDB.OrderMappingContacts.Where(o => o.OrderId == orderID).FirstOrDefault();
                                            if (OMC != null)
                                            {
                                                List<QuickBaseFieldRecord> OMcontRecord = new List<QuickBaseFieldRecord>();
                                                OMcontRecord.Add(new QuickBaseFieldRecord { Name = "Related Project", FieldValue = recID });
                                                if (!string.IsNullOrEmpty(OMC.ContactName))
                                                    NameList = OMC.ContactName.Split(' ');

                                                if (NameList[0] != null)
                                                    OMcontRecord.Add(new QuickBaseFieldRecord { Name = "First Name", FieldValue = NameList[0] });
                                                if (NameList[NameList.Length - 1] != null && (NameList.Length - 1) != 0)
                                                {
                                                    OMcontRecord.Add(new QuickBaseFieldRecord { Name = "Last Name", FieldValue = NameList[NameList.Length - 1] });
                                                }
                                                OMcontRecord.Add(new QuickBaseFieldRecord { Name = "Title / Job Function", FieldValue = "Mapping Contact" });

                                                if (OMC.ContactPhone == null || OMC.ContactPhone == "")
                                                    OMcontRecord.Add(new QuickBaseFieldRecord { Name = "Phone", FieldValue = "0000000" });
                                                else
                                                    OMcontRecord.Add(new QuickBaseFieldRecord { Name = "Phone", FieldValue = OMC.ContactPhone });
                                                if (OMC.ContactEmail == null || OMC.ContactEmail == "")
                                                    OMcontRecord.Add(new QuickBaseFieldRecord { Name = "Email", FieldValue = "mail@mail.com" });
                                                else
                                                    OMcontRecord.Add(new QuickBaseFieldRecord { Name = "Email", FieldValue = OMC.ContactEmail });

                                                var OMContactrec = connection.NewRecord(conttable, OMcontRecord);

                                            }
                                            #endregion
                                            //////////////////
                                            //Add Data To Phase and Step
                                            #region phases and steps EDI
                                            if (orderObj.OrderType == 1)
                                            {
                                                //Phase 1 step 1A.1,2,3,4,5,6
                                                string phaseID = AddPhaseNewLib(connection, recID, "14");
                                                AddStepNewLib(connection, recID, phaseID, "28");
                                                AddStepNewLib(connection, recID, phaseID, "29");
                                                AddStepNewLib(connection, recID, phaseID, "30");
                                                AddStepNewLib(connection, recID, phaseID, "31");
                                                AddStepNewLib(connection, recID, phaseID, "32");
                                                AddStepNewLib(connection, recID, phaseID, "33");
                                                logger.Info("phase 1 added for Project: " + CarrierObj.ProjectName);

                                                //Phase 1B step 2,3,4
                                                phaseID = AddPhaseNewLib(connection, recID, "15");
                                                AddStepNewLib(connection, recID, phaseID, "34");
                                                AddStepNewLib(connection, recID, phaseID, "35");
                                                AddStepNewLib(connection, recID, phaseID, "36");
                                                logger.Info("phase 1B added for Project: " + CarrierObj.ProjectName);


                                                //Phase 2 step 5,6
                                                phaseID = AddPhaseNewLib(connection, recID, "10");
                                                AddStepNewLib(connection, recID, phaseID, "22");
                                                logger.Info("phase 2 added for Project: " + CarrierObj.ProjectName);

                                                    try
                                                    {
                                                        if (rec401K)
                                                        {
                                                            //Phase 3 
                                                            phaseID = AddPhaseNewLib(connection, recID, "11");
                                                            AddStepNewLib(connection, recID, phaseID, "23");
                                                            AddStepNewLib(connection, recID, phaseID, "24");
                                                            logger.Info("phase 3 401k added for Project: " + CarrierObj.ProjectName);

                                                            //Phase 4 
                                                            phaseID = AddPhaseNewLib(connection, recID, "12");
                                                            AddStepNewLib(connection, recID, phaseID, "37");
                                                            AddStepNewLib(connection, recID, phaseID, "38");
                                                            AddStepNewLib(connection, recID, phaseID, "39");
                                                            logger.Info("phase 4 401k added for Project: " + CarrierObj.ProjectName);


                                                            //Phase 5 step 10
                                                            phaseID = AddPhaseNewLib(connection, recID, "13");
                                                            AddStepNewLib(connection, recID, phaseID, "26");
                                                            AddStepNewLib(connection, recID, phaseID, "27");
                                                            logger.Info("phase 5 401k added for Project: " + CarrierObj.ProjectName);

                                                        }
                                                        else
                                                        {
                                                            logger.Info("!401K");

                                                            //// Pre-mapping new phases for Production
                                                            if (Environment == "production")
                                                            {
                                                                #region production phases
                                                                // Phase 3 
                                                                phaseID = AddPhaseNewLib(connection, recID, "24");
                                                                AddStepNewLib(connection, recID, phaseID, "60");
                                                                AddStepNewLib(connection, recID, phaseID, "61");
                                                                //AddStep(app, newRecord.RecordId.ToString(), phaseID, "62");
                                                                logger.Info("phase 3 added for Project: " + CarrierObj.ProjectName);


                                                                //Phase 4 step 
                                                                phaseID = AddPhaseNewLib(connection, recID, "25");
                                                                AddStepNewLib(connection, recID, phaseID, "63");
                                                                logger.Info("phase 4 added for Project: " + CarrierObj.ProjectName);


                                                                //Phase 5 
                                                                phaseID = AddPhaseNewLib(connection, recID, "26");
                                                                AddStepNewLib(connection, recID, phaseID, "64");
                                                                AddStepNewLib(connection, recID, phaseID, "65");
                                                                AddStepNewLib(connection, recID, phaseID, "66");
                                                                logger.Info("phase 5 added for Project: " + CarrierObj.ProjectName);


                                                                //Phase 6 
                                                                phaseID = AddPhaseNewLib(connection, recID, "27");
                                                                AddStepNewLib(connection, recID, phaseID, "67");
                                                                AddStepNewLib(connection, recID, phaseID, "68");
                                                                AddStepNewLib(connection, recID, phaseID, "69");
                                                                logger.Info("phase 6 added for Project: " + CarrierObj.ProjectName);


                                                                #endregion
                                                            }
                                                            else if (Environment == "testing")
                                                            {
                                                                // Pre-mapping new phases for Testing
                                                                #region testing phases
                                                                // Phase 3 
                                                                phaseID = AddPhaseNewLib(connection, recID, "23");
                                                                AddStepNewLib(connection, recID, phaseID, "60");
                                                                AddStepNewLib(connection, recID, phaseID, "61");
                                                                //AddStep(app, recID(), phaseID, "62");
                                                                logger.Info("phase 3 added for Project: " + CarrierObj.ProjectName);


                                                                //Phase 4 
                                                                phaseID = AddPhaseNewLib(connection, recID, "24");
                                                                AddStepNewLib(connection, recID, phaseID, "64");
                                                                logger.Info("phase 4 added for Project: " + CarrierObj.ProjectName);


                                                                //Phase 5 
                                                                phaseID = AddPhaseNewLib(connection, recID, "25");
                                                                AddStepNewLib(connection, recID, phaseID, "65");
                                                                AddStepNewLib(connection, recID, phaseID, "68");
                                                                AddStepNewLib(connection, recID, phaseID, "69");
                                                                logger.Info("phase 5 added for Project: " + CarrierObj.ProjectName);



                                                                //Phase 6 
                                                                phaseID = AddPhaseNewLib(connection, recID, "26");
                                                                AddStepNewLib(connection, recID, phaseID, "66");
                                                                AddStepNewLib(connection, recID, phaseID, "67");
                                                                AddStepNewLib(connection, recID, phaseID, "70");
                                                                logger.Info("phase 6 added for Project: " + CarrierObj.ProjectName);

                                                                #endregion
                                                            }
                                                        }
                                                        //update step 1A.1 & 1B.1
                                                        // updateStep(CustomerName, CarrierName, "28", "Start", orderObj.SubmitDate.ToString(), "VIP Access");
                                                        // updateStep(CustomerName, CarrierName, "28", "Complete", orderObj.SubmitDate.ToString(),"VIP Access");
                                                        //updateStepNewLib(connection, recID, CustomerName, CarrierName, "28", "Start", orderObj.SubmitDate.ToString(), "VIP Access");
                                                        //updateStepNewLib(connection, recID, CustomerName, CarrierName, "28", "Complete", orderObj.SubmitDate.ToString(), "VIP Access");
                                                        UpdateStepNewLib(connection, recID, CustomerName, CarrierName, "34", "Start", orderObj.SubmitDate.ToString(), "58491579.dezm");
                                                        UpdateStepNewLib(connection, recID, CustomerName, CarrierName, "34", "Complete", orderObj.SubmitDate.ToString(), "58491579.dezm");
                                                    }
                                                    catch (Exception e)
                                                    {
                                                        logger.Error("Err at phase 3");
                                                        if (e.InnerException != null) logger.Error("Inner Exce=: " + e.InnerException);
                                                        logger.Error("Err mes=: " + e.Message);
                                                        logger.Error("Stack trace=: " + e.StackTrace);
                                                        return false;
                                                    }
                                                }
                                            #endregion

                                            #region phases and steps OE
                                            else if (orderObj.OrderType == 2)
                                            {
                                                //Phase 1 step 1A.1,2,3,4,5,6
                                                string phaseID = AddPhaseNewLib(connection, recID, "16");
                                                AddStepNewLib(connection, recID, phaseID, "40");


                                                //Phase 1B step 2,3,4
                                                phaseID = AddPhaseNewLib(connection, recID, "17");
                                                AddStepNewLib(connection, recID, phaseID, "41", true);
                                                AddStepNewLib(connection, recID, phaseID, "42", true);
                                                AddStepNewLib(connection, recID, phaseID, "43");
                                                AddStepNewLib(connection, recID, phaseID, "44");
                                                AddStepNewLib(connection, recID, phaseID, "45");
                                                //removing the below step for new OE requirements
                                                // AddStep(app, recID(), phaseID, "46");

                                                //Phase 2 step 5,6
                                                phaseID = AddPhaseNewLib(connection, recID, "18");
                                                AddStepNewLib(connection, recID, phaseID, "47");

                                                //Phase 3 step 7
                                                phaseID = AddPhaseNewLib(connection, recID, "19");
                                                AddStepNewLib(connection, recID, phaseID, "48");
                                                AddStepNewLib(connection, recID, phaseID, "49");
                                                AddStepNewLib(connection, recID, phaseID, "50");

                                                //Phase 4 step 
                                                phaseID = AddPhaseNewLib(connection, recID, "20");
                                                AddStepNewLib(connection, recID, phaseID, "51");
                                                AddStepNewLib(connection, recID, phaseID, "52");


                                                //Phase 5 step 10
                                                phaseID = AddPhaseNewLib(connection, recID, "21");
                                                AddStepNewLib(connection, recID, phaseID, "53");
                                                AddStepNewLib(connection, recID, phaseID, "54");
                                                AddStepNewLib(connection, recID, phaseID, "55");
                                                AddStepNewLib(connection, recID, phaseID, "56");

                                                //Phase 6 step 10
                                                phaseID = AddPhaseNewLib(connection, recID, "22");
                                                AddStepNewLib(connection, recID, phaseID, "57");
                                                AddStepNewLib(connection, recID, phaseID, "58");
                                                AddStepNewLib(connection, recID, phaseID, "59");

                                                //
                                            }
                                            #endregion


                                            #region phases and steps DF
                                            else if (orderObj.OrderType == 3)
                                            {

                                                if (Environment != "production")
                                                {
                                                    //Phase 1 step 1
                                                    string phaseID = AddPhaseNewLib(connection, recID, "27"); //27. DF - Resource Assignment
                                                    AddStepNewLib(connection, recID, phaseID, "82"); //1.1 Resource assignement


                                                    //update step assigned to youmna
                                                    if (formuploaded == true)
                                                        UpdateStepNewLib(connection, recID, CustomerObj.CustomerName, CarrierObj.CarrierName, "82", "In Progress", orderObj.SubmitDate.ToString(), "58706077.dpku"); //yomna id in qb in prod.

                                                    //Phase 2 step 1,2,3,4
                                                    phaseID = AddPhaseNewLib(connection, recID, "28");
                                                    AddStepNewLib(connection, recID, phaseID, "83");
                                                    AddStepNewLib(connection, recID, phaseID, "84");
                                                    AddStepNewLib(connection, recID, phaseID, "85");
                                                    AddStepNewLib(connection, recID, phaseID, "86");


                                                    //Phase 3 
                                                    phaseID = AddPhaseNewLib(connection, recID, "29");
                                                    AddStepNewLib(connection, recID, phaseID, "87");
                                                    AddStepNewLib(connection, recID, phaseID, "88");
                                                    AddStepNewLib(connection, recID, phaseID, "96");


                                                    //Phase 4 step 7
                                                    phaseID = AddPhaseNewLib(connection, recID, "30");
                                                    AddStepNewLib(connection, recID, phaseID, "89");

                                                    //Phase 5 step 
                                                    phaseID = AddPhaseNewLib(connection, recID, "31");
                                                    AddStepNewLib(connection, recID, phaseID, "90");


                                                    //Phase 6
                                                    phaseID = AddPhaseNewLib(connection, recID, "33");
                                                    AddStepNewLib(connection, recID, phaseID, "92");
                                                    AddStepNewLib(connection, recID, phaseID, "93");
                                                    AddStepNewLib(connection, recID, phaseID, "94");

                                                    //Phase 6 step 10
                                                    phaseID = AddPhaseNewLib(connection, recID, "34");
                                                    AddStepNewLib(connection, recID, phaseID, "95");

                                                    //
                                                }
                                                else
                                                {
                                                    //Phase 1 step 1
                                                    string phaseID = AddPhaseNewLib(connection, recID, "28"); //27. DF - Resource Assignment
                                                    AddStepNewLib(connection, recID, phaseID, "82"); //1.1 Resource assignement
                                                                                                     //update step assigned to youmna
                                                    if (formuploaded == true)
                                                        UpdateStepNewLib(connection, recID, CustomerObj.CustomerName, CarrierObj.CarrierName, "82", "In Progress", orderObj.SubmitDate.ToString(), "58706077.dpku"); //yomna id in qb in prod.

                                                    //Phase 2 step 1,2,3,4
                                                    phaseID = AddPhaseNewLib(connection, recID, "29");
                                                    AddStepNewLib(connection, recID, phaseID, "83");
                                                    AddStepNewLib(connection, recID, phaseID, "84");
                                                    AddStepNewLib(connection, recID, phaseID, "85");
                                                    AddStepNewLib(connection, recID, phaseID, "86");


                                                    //Phase 3 
                                                    phaseID = AddPhaseNewLib(connection, recID, "30");
                                                    AddStepNewLib(connection, recID, phaseID, "87");
                                                    AddStepNewLib(connection, recID, phaseID, "88");
                                                    AddStepNewLib(connection, recID, phaseID, "89");


                                                    //Phase 4 step 7
                                                    phaseID = AddPhaseNewLib(connection, recID, "31");
                                                    AddStepNewLib(connection, recID, phaseID, "90");

                                                    //Phase 5 step 
                                                    phaseID = AddPhaseNewLib(connection, recID, "32");
                                                    AddStepNewLib(connection, recID, phaseID, "91");


                                                    //Phase 6
                                                    phaseID = AddPhaseNewLib(connection, recID, "33");
                                                    AddStepNewLib(connection, recID, phaseID, "92");
                                                    AddStepNewLib(connection, recID, phaseID, "93");
                                                    AddStepNewLib(connection, recID, phaseID, "94");

                                                    //Phase 7 
                                                    phaseID = AddPhaseNewLib(connection, recID, "34");
                                                    AddStepNewLib(connection, recID, phaseID, "95");

                                                    //
                                                }
                                            }
                                                #endregion
                                          
                        #endregion

                        CarrierObj.QBCreated = true;
                    ebnDB.SaveChanges();
                    return true;
                    }
                }


            }
            catch (Exception e)
            {
                if (e.InnerException != null) logger.Error("Inner Exce=: " + e.InnerException);
                logger.Error("Err mes=: " + e.Message);
                logger.Error("Stack trace=: " + e.StackTrace);
                return false;
            }
        }
        public static string AddPhaseNewLib(QuickBaseConnection Conn, string ProjectID, string PhaseType)
        {
            string phtable = ConfigurationManager.AppSettings["phasetable"];
            List<QuickBaseFieldRecord> phaseRecord = new List<QuickBaseFieldRecord>
            {
                new QuickBaseFieldRecord { Name = "Related Project", FieldValue = ProjectID },
                new QuickBaseFieldRecord { Name = "Related Phase Type", FieldValue = PhaseType }
            };

            var xdphaseTable = Conn.NewRecord(phtable, phaseRecord);

            return xdphaseTable.Element("rid").Value.ToString();
        }
        public static void AddStepNewLib(QuickBaseConnection Conn, string ProjectID, string PhaseId, string stepType, bool nastatus = false)
        {
            string sttable = ConfigurationManager.AppSettings["steptable"];
            List<QuickBaseFieldRecord> stepRecord = new List<QuickBaseFieldRecord>();
            stepRecord.Add(new QuickBaseFieldRecord { Name = "Related Project", FieldValue = ProjectID });
            stepRecord.Add(new QuickBaseFieldRecord { Name = "Related Phase", FieldValue = PhaseId });
            stepRecord.Add(new QuickBaseFieldRecord { Name = "Related Step Type", FieldValue = stepType });

            string staus = "Not Started";
            if (nastatus) staus = "N/A";

            stepRecord.Add(new QuickBaseFieldRecord { Name = "Status", FieldValue = staus });

            var xdphaseTable = Conn.NewRecord(sttable, stepRecord);

        }
        public static void UpdateStepNewLib(QuickBaseConnection Conn, string projrecid, string CustomerName, string CarrierName, string stepType, string eventTYpe, string formdate = null, string AssignedTo = null)
        {
            try
            {
                // Connect to QuickBase

                string sttable = ConfigurationManager.AppSettings["steptable"];
                string ProjectTable = ConfigurationManager.AppSettings["projecttable"];
                string activitytable = System.Configuration.ConfigurationManager.AppSettings["activitiestable"];
                string stepmatchvalue = "60";

                DateTime orderDate;
                DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0);//Unix Epoch on January 1st, 1970
                DateTime olddate;
                DateTime beforemapping;
                DateTime submissionDate, sDate;

                // string Environment = ConfigurationManager.AppSettings["environment"];

                try
                {
                    // string rec = projrecid;
                    var projectQuery = new QuickBaseSingleQuery { FieldId = 3, ComparisonOperator = QuickBaseComparisonOperator.EX, Value = projrecid };
                    DataTable ProjTable;
                    if (Environment == "Production")
                    {
                        //production
                        //nonvipstatus = 249 , onhold start date = 242 , onholdend date = 243 , reason = 241 , reason detail = 244
                        ProjTable = Conn.DoQueryAndFillTable(ProjectTable, projectQuery, new int[] { 20, 74, 249, 242, 243, 241, 244 }.ToList(), null);

                    }
                    else
                    {
                        //testing
                        //nonvipstatus = 300 ,  onhold start date = 220 , onholdend date = 221 , reason = 219 , reason detail = 294
                        ProjTable = Conn.DoQueryAndFillTable(ProjectTable, projectQuery, new int[] { 20, 74, 300, 220, 221, 219, 294 }.ToList(), null);
                    }

                    var datacount = Conn.DoQueryCount(ProjectTable, projectQuery);

                    if (datacount == 0)
                    {
                        return;
                    }

                    #region re-activating on hold project

                    DataRow rec = ProjTable.Rows[0];
                    // DataColumn rec = ProjTable.Columns["Record ID#"];

                    if (stepType == "61" && rec["Status"].ToString() == "On Hold" && formdate != null)
                    {
                        DateTime today = DateTime.Today;
                        if (DateTime.TryParse(formdate, out sDate))
                        {
                            submissionDate = sDate;//.ToString("MM-dd-yyyy"); 
                        }
                        else
                        {
                            submissionDate = origin.AddMilliseconds(Convert.ToDouble(formdate));
                        }
                        TimeSpan t = today.Date - submissionDate.Date;
                        if (t.TotalDays <= 56)
                        {
                            rec["Status"] = "In Progress";
                            rec["NonVIPStatus2"] = "In Progress";
                            rec["On Hold Start Date"] = new DateTime();
                            rec["On Hold End Date"] = new DateTime();
                            rec["Reason"] = "";
                            rec["Reason Details"] = "";

                            var valdict = rec.Table.Columns.Cast<DataColumn>().ToDictionary(c => c.ColumnName, c => rec[c]);

                            var n = Conn.UPDRecord(ProjectTable, projrecid, valdict);

                            // Console.WriteLine("updating Project: " + rec["Name"] + "status to In Progress");
                            try
                            {
                                //   DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0);
                                string datenow = DateTime.Now.ToString();
                                DateTime end;

                                // add activity  table
                                List<QuickBaseFieldRecord> newActivityRecordInProgress = new List<QuickBaseFieldRecord>();

                                newActivityRecordInProgress.Add(new QuickBaseFieldRecord { Name = "Related Project", FieldValue = projrecid });
                                newActivityRecordInProgress.Add(new QuickBaseFieldRecord { Name = "Activity Details", FieldValue = "Project is back In Progress" });
                                if (DateTime.TryParse(datenow, out end))
                                {
                                    newActivityRecordInProgress.Add(new QuickBaseFieldRecord { Name = "Date", FieldValue = end.ToShortDateString() });//.ToString("MM-dd-yyyy"); 
                                    newActivityRecordInProgress.Add(new QuickBaseFieldRecord { Name = "Time of Activity", FieldValue = end.ToShortTimeString() });
                                }
                                else
                                {
                                    newActivityRecordInProgress.Add(new QuickBaseFieldRecord { Name = "Date", FieldValue = origin.AddMilliseconds(Convert.ToDouble(datenow)).ToString("MM-dd-yyyy") });
                                    newActivityRecordInProgress.Add(new QuickBaseFieldRecord { Name = "Time of Activity", FieldValue = origin.AddMilliseconds(Convert.ToDouble(datenow)).ToShortTimeString() });
                                }

                                var actrec = Conn.NewRecord(activitytable, newActivityRecordInProgress);

                            }
                            catch (Exception e)
                            {
                                // Console.WriteLine("Error: adding activity: " + rec["Name"] + "status to In Progress " + e.Message);

                            }
                        }
                    }
                    #endregion


                    //  if (DateTime.TryParse(rec["On Hold End Date"], out onholdenddate))
                    try
                    {
                        DateTime.TryParse("08-18-2014", out olddate);

                        //for production
                        if (Environment == "production")
                        {
                            DateTime.TryParse("08-17-2015", out beforemapping);
                        }
                        // for testing 
                        else
                        {
                            DateTime.TryParse("06-24-2015", out beforemapping);
                        }
                        try
                        {
                            orderDate = Convert.ToDateTime(rec["Order Received Date"]);
                        }
                        catch (Exception e)
                        {
                            DateTime.TryParse(rec["Order Received Date"].ToString(), out orderDate);
                        }

                        if (orderDate.Date > olddate.Date && orderDate.Date <= beforemapping.Date)
                        {
                            stepmatchvalue = "71";
                            // Q4 = new QueryStrings(11, comparisonOp: ComparisonOperator.EX, matchingValue: "71", logicalOp: LogicalOperator.NONE);
                            switch (stepType)
                            {
                                case "60":
                                    stepType = "71";
                                    break;
                                case "61":
                                    stepType = "72";
                                    break;
                                case "62":
                                    stepType = "73";
                                    break;
                                case "63":
                                    stepType = "74";
                                    break;
                            }
                        }

                        else if (orderDate.Date <= olddate.Date)
                        {
                            // Q4 = new QueryStrings(11, comparisonOp: ComparisonOperator.EX, matchingValue: "75", logicalOp: LogicalOperator.NONE);
                            stepmatchvalue = "75";
                            switch (stepType)
                            {
                                case "60":
                                    stepType = "75";
                                    break;
                                case "61":
                                    stepType = "76";
                                    break;
                                case "62":
                                    stepType = "77";
                                    break;
                                case "63":
                                    stepType = "78";
                                    break;
                            }
                        }
                        //comment when publish to production
                        else if (stepType == "63" && Environment == "testing")
                        {
                            stepType = "64";
                        }
                    }
                    catch (Exception ex)
                    {
                        if (ex.InnerException != null) logger.Error("Inner Exce=: " + ex.InnerException);
                        logger.Error("Err mes=: " + ex.Message);
                        logger.Error("Stack trace=: " + ex.StackTrace);
                    }

                }
                catch (Exception ex)
                {
                    if (ex.InnerException != null) logger.Error("Inner Exce=: " + ex.InnerException);
                    logger.Error("Err mes=: " + ex.Message);
                    logger.Error("Stack trace=: " + ex.StackTrace);
                }

                var steptableq2 = new QuickBaseMultipleQuery { AndOrOperator = QuickBaseAndOrOperator.AND };
                steptableq2.Queries.Add(new QuickBaseSingleQuery { FieldId = 32, ComparisonOperator = QuickBaseComparisonOperator.EX, Value = CustomerName.ToLower() + "_" + CarrierName.ToLower() });
                steptableq2.Queries.Add(new QuickBaseSingleQuery { FieldId = 11, ComparisonOperator = QuickBaseComparisonOperator.EX, Value = stepmatchvalue });
                var count2 = Conn.DoQueryCount(sttable, steptableq2); //assigned to = 16
                string assignedto = "";
                if (count2 != 0)
                {
                    var stepdataTablex = Conn.DoQueryAndFillTable(sttable, steptableq2, new int[] { 16 }.ToList(), null);
                    DataRow Record = stepdataTablex.Rows[0];
                    assignedto = Record["Assigned To"].ToString();
                }


                var steptableq = new QuickBaseMultipleQuery { AndOrOperator = QuickBaseAndOrOperator.AND };
                steptableq.Queries.Add(new QuickBaseSingleQuery { FieldId = 32, ComparisonOperator = QuickBaseComparisonOperator.EX, Value = CustomerName.ToLower() + "_" + CarrierName.ToLower() });
                steptableq.Queries.Add(new QuickBaseSingleQuery { FieldId = 11, ComparisonOperator = QuickBaseComparisonOperator.EX, Value = stepType });

                var count = Conn.DoQueryCount(sttable, steptableq);
                if (count == 0) return;

                DataTable stepdataTable;

                //testing & prod
                // completiondate = 18 , status = 34 , assigned to = 16 ,  start date = 17

                stepdataTable = Conn.DoQueryAndFillTable(sttable, steptableq, new int[] { 3, 18, 34, 16, 17 }.ToList(), null);

                DataRow Records = stepdataTable.Rows[0];

                DateTime CurrentTime = DateTime.Now;
                if (Records != null)
                {
                    if (eventTYpe == "Complete")
                    {
                        Records["Completion Date"] = CurrentTime.Date.ToString("MM-dd-yyyy"); ;
                        Records["Status"] = "Complete";
                        if (!string.IsNullOrEmpty(AssignedTo))
                            Records["Assigned To"] = AssignedTo;
                    }
                    else
                    {
                        Records["Start Date"] = CurrentTime.Date.ToString("MM-dd-yyyy"); ;
                        Records["Status"] = "In Progress";

                        if (!string.IsNullOrEmpty(assignedto))
                            Records["Assigned To"] = assignedto;

                        else if (!string.IsNullOrEmpty(AssignedTo))
                            Records["Assigned To"] = AssignedTo;



                    }

                    var valstepdict = Records.Table.Columns.Cast<DataColumn>().ToDictionary(c => c.ColumnName, c => Records[c]);
                    var n = Conn.UPDRecord(sttable, Records["Record ID#"].ToString(), valstepdict);
                }


            }
            catch (Exception ex)
            {
                if (ex.InnerException != null) logger.Error("Inner Exce=: " + ex.InnerException);
                logger.Error("Err mes=: " + ex.Message);
                logger.Error("Stack trace=: " + ex.StackTrace);
            }

        }

        public static void UpdateQB(long orderID, long? carrierId, string qbRecId = null)
        {

            Order order = ebnd.Orders.Where(o => o.OrderID == orderID).FirstOrDefault();
            Customer customer = ebnd.Customers.Where(c => c.OrderID == order.OrderID).FirstOrDefault();
            string partnerName = ebnd.Partners.Where(c => c.PartnerId == order.PartnerID).Select(c => c.PartnerName).FirstOrDefault();

            List<Carrier> cars = new List<Carrier>();
            Carrier carObj  = null;

            if (carrierId.HasValue)
                carObj = ebnd.Carriers.Where(c => c.CarrierID == carrierId).FirstOrDefault();
            #region QuickBase

            if (!string.IsNullOrEmpty(qbRecId))
            {
                carObj = ebnd.Carriers.Where(c => c.CarrierID == carrierId.Value).FirstOrDefault();
                logger.Info("QB :  CarrierID: "+ carObj.CarrierID);

                Dictionary<string, string> carInfoUpdVals = new Dictionary<string, string>();

                Carrier parenFeed = ebnd.Carriers.Where(c => c.CarrierID == carObj.MultitenantId).FirstOrDefault();

                carInfoUpdVals.Add("Parent Feed", parenFeed.ProjectName);
                carInfoUpdVals.Add("IsMultitenant", "1");
                carInfoUpdVals.Add("Name", carObj.ProjectName);
                carInfoUpdVals.Add("Carrier Name", carObj.CarrierName);
                bool success = UpdateListOfProjectFields(qbRecId, connection, partnerName, carObj.CarrierName, customer.CustomerName, carInfoUpdVals);
                if(success)
                    logger.Info(String.Format("QBSuccess: QB updated for Connection : {0}", carObj.ProjectName));
                else
                    logger.Info(String.Format("Error: QB failed to be updated for Connection : {0}", carObj.ProjectName));



            }
            else
            { 
            if (carObj != null)
                cars.Add(carObj);
            else
                cars = ebnd.Carriers.Where(c => c.OrderID == order.OrderID).ToList();

            string projid = "";


            foreach (var car in cars)
            {
                Dictionary<string, string> carInfoUpdVals = new Dictionary<string, string>();
                if (car.MultitenantId != null)
                {
                    Carrier parenFeed = ebnd.Carriers.Where(c => c.CarrierID == car.MultitenantId).FirstOrDefault();
                    carInfoUpdVals.Add("Parent Feed", parenFeed.ProjectName);
                }

                if (car.MultitenantStatus == true)
                    carInfoUpdVals.Add("IsMultitenant", "1");
                else
                    carInfoUpdVals.Add("IsMultitenant", "0");

                projid = QBget_projid(order.OrderType, customer.CustomerName, car.CarrierName, car.OEStartDate ?? null);
                bool success = UpdateListOfProjectFields(projid, connection, partnerName, car.CarrierName, customer.CustomerName, carInfoUpdVals);
            }
        }
            #endregion

        }

        public static string QBget_projid( int OrderType, string customername, string carriername, DateTime? OEstart = null)
        {

            string proTable = ConfigurationManager.AppSettings["projecttable"];
            var projectQuery = new QuickBaseMultipleQuery { AndOrOperator = QuickBaseAndOrOperator.AND };

            projectQuery.Queries.Add(new QuickBaseSingleQuery { FieldId = 6, ComparisonOperator = QuickBaseComparisonOperator.EX, Value = customername.ToLower() + "_" + carriername.ToLower() });
            projectQuery.Queries.Add(new QuickBaseSingleQuery { FieldId = 74, ComparisonOperator = QuickBaseComparisonOperator.XCT, Value = "Cancelled" });

            var customerCarrierQuery = new QuickBaseMultipleQuery { AndOrOperator = QuickBaseAndOrOperator.AND };
            customerCarrierQuery.Queries.Add(new QuickBaseSingleQuery { FieldId = 18, ComparisonOperator = QuickBaseComparisonOperator.EX, Value = customername.ToLower() });
            customerCarrierQuery.Queries.Add(new QuickBaseSingleQuery { FieldId = 19, ComparisonOperator = QuickBaseComparisonOperator.EX, Value = carriername.ToLower() });
            customerCarrierQuery.Queries.Add(new QuickBaseSingleQuery { FieldId = 74, ComparisonOperator = QuickBaseComparisonOperator.XCT, Value = "Cancelled" });


            if (OrderType == 1)
            {
                projectQuery.Queries.Add(new QuickBaseSingleQuery { FieldId = 69, ComparisonOperator = QuickBaseComparisonOperator.XCT, Value = "OE" });
                projectQuery.Queries.Add(new QuickBaseSingleQuery { FieldId = 69, ComparisonOperator = QuickBaseComparisonOperator.XCT, Value = "DF" });
                customerCarrierQuery.Queries.Add(new QuickBaseSingleQuery { FieldId = 69, ComparisonOperator = QuickBaseComparisonOperator.XCT, Value = "OE" });
                customerCarrierQuery.Queries.Add(new QuickBaseSingleQuery { FieldId = 69, ComparisonOperator = QuickBaseComparisonOperator.XCT, Value = "DF" });
            }
            else if (OrderType == 2)
            {

                projectQuery.Queries.Add(new QuickBaseSingleQuery { FieldId = 69, ComparisonOperator = QuickBaseComparisonOperator.CT, Value = "OE" });
                projectQuery.Queries.Add(new QuickBaseSingleQuery { FieldId = 70, ComparisonOperator = QuickBaseComparisonOperator.EX, Value = OEstart });

                customerCarrierQuery.Queries.Add(new QuickBaseSingleQuery { FieldId = 69, ComparisonOperator = QuickBaseComparisonOperator.CT, Value = "OE" });
                customerCarrierQuery.Queries.Add(new QuickBaseSingleQuery { FieldId = 70, ComparisonOperator = QuickBaseComparisonOperator.EX, Value = OEstart.Value.Date });

            }

            var mainQuery = new QuickBaseMultipleQuery { AndOrOperator = QuickBaseAndOrOperator.OR };
            mainQuery.Queries.Add(projectQuery);
            mainQuery.Queries.Add(customerCarrierQuery);

            var datacount = connection.DoQueryCount(proTable, mainQuery);

            if (datacount == 0)
            {
              logger.Error( "QB missing Project, This project does noy exist in QB: "+ customername+"_"+carriername);
                return null; // to emove carrier check to create QB
            }
            var dataTable = connection.DoQueryAndFillTable(proTable, mainQuery, new int[] { 3 }.ToList(), null);
            DataRow rec = dataTable.Rows[0];
            return rec["Record ID#"].ToString();
        }

        public static bool UpdateListOfProjectFields(string recid, QuickBaseConnection connection, string PartnerName, string CarrierName, string CustomerName, Dictionary<string, string> updFields, DateTime? OEStartDate = null)
        {

            try
            {
                string projTable = ConfigurationManager.AppSettings["projecttable"];
                string Environment = ConfigurationManager.AppSettings["Environment"];

                using (connection)
                {
                    var projectQuery = new QuickBaseSingleQuery { FieldId = 3, ComparisonOperator = QuickBaseComparisonOperator.EX, Value = recid };
                    var datacount = connection.DoQueryCount(projTable, projectQuery);
                    if (datacount != 0)
                    {

                        DataTable projdataTable = connection.DoQueryAndFillTable(projTable, projectQuery, new int[] { 3 }.ToList(), null);
                        foreach (var updField in updFields)
                        {
                            if (updField.Key == "401k")
                            {
                                projdataTable.Columns.Add("50", typeof(System.Int32));
                                projdataTable.Rows[0]["50"] = updField.Value;
                            }
                            else if (updField.Key == "401K Type")
                            {
                                projdataTable.Columns.Add("161", typeof(String));
                                projdataTable.Rows[0]["161"] = updField.Value;
                            }
                            else
                            {
                                projdataTable.Columns.Add(updField.Key, typeof(string));
                                projdataTable.Rows[0][updField.Key] = updField.Value;
                            }

                        }
                        try
                        {
                            var valprojdict = projdataTable.Rows[0].Table.Columns.Cast<DataColumn>().ToDictionary(c => c.ColumnName, c => projdataTable.Rows[0][c]);
                            var n = connection.UPDRecord(projTable, projdataTable.Rows[0]["Record ID#"].ToString(), valprojdict);
                            logger.Info("QBNameUpdate=: " + recid);

                            return true;
                        }
                        catch (Exception e)
                        {
                            logger.Error("Err mes=: QB status were not updated for connectionName: " + CarrierName);
                            if (e.InnerException != null) logger.Error("Inner Exce=: " + e.InnerException);
                            logger.Error("Err mes=: " + e.Message);
                            logger.Error("Stack trace=: " + e.StackTrace);
                            return false;
                        }

                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("Error updating QB");
                if (ex.InnerException != null) logger.Error("Inner Exce=: " + ex.InnerException);
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(0);
                var line = frame.GetFileLineNumber();
                logger.Error("Err mes=: " + ex.Message);
                logger.Error("Stack trace=: " + ex.StackTrace);
                return false;
            }
           


        }


        public void cancelOrder(long orderid)
        {
            try
            {
                using (eBNPortal_ProductionEntities ebnDB = new eBNPortal_ProductionEntities())
                {
                    //Order orderObj = ebnDB.Orders.Include("Carriers").SingleOrDefault(X => X.OrderID == orderid);
                    Order orderObj = ebnDB.Orders.SingleOrDefault(X => X.OrderID == orderid);
                    List<long> carids = ebnDB.Carriers.Where(c => c.OrderID == orderid && (c.ServiceType == 2 || c.ServiceType == 3 || c.ServiceType == null)).Select(c => c.CarrierID).ToList();

                    foreach (long id in carids)
                    {
                        try
                        {
                            bool cancelled = cancelCarrier(orderid, id);
                        }
                        catch (Exception e)
                        {
                            logger.Info("User=: " + System.Web.HttpContext.Current.User.Identity.Name);
                            if (e.InnerException != null) logger.Error("Inner Exce=: " + e.InnerException);
                            logger.Error("Err mes=: " + e.Message);
                            logger.Error("Stack trace=: " + e.StackTrace);
                        }
                    }


                }
            }
            catch (Exception e)
            {
                logger.Info("User=: " + System.Web.HttpContext.Current.User.Identity.Name);
                if (e.InnerException != null) logger.Error("Inner Exce=: " + e.InnerException);
                logger.Error("Err mes=: " + e.Message);
                logger.Error("Stack trace=: " + e.StackTrace);
            }
        }

        public bool cancelCarrier(long orderID, long carrierID)
        {
            try
            {
                string username = ConfigurationManager.AppSettings["QBUserName"];
                string Password = ConfigurationManager.AppSettings["QBPassword"];
                string SDomain = "santeon.quickbase.com";
                string protable = ConfigurationManager.AppSettings["projecttable"];
                string Environment = ConfigurationManager.AppSettings["environment"];


                using (eBNPortal_ProductionEntities ebnDB = new eBNPortal_ProductionEntities())
                {
                    Carrier CarrierObj = ebnDB.Carriers.SingleOrDefault(Z => Z.CarrierID == carrierID);
                    Order orderObj = ebnDB.Orders.SingleOrDefault(X => X.OrderID == orderID);
                    string CustomerName = ebnDB.Customers.Where(c => c.OrderID == orderID).Select(c => c.CustomerName).FirstOrDefault();
                    string CarrierName = CarrierObj.CarrierName;

                    using (var connection = new QuickBaseConnection(SDomain, username, Password))
                    {
                        var projectQuery = new QuickBaseMultipleQuery { AndOrOperator = QuickBaseAndOrOperator.AND };
                        projectQuery.Queries.Add(new QuickBaseSingleQuery { FieldId = 6, ComparisonOperator = QuickBaseComparisonOperator.EX, Value = CustomerName.ToLower() + "_" + CarrierObj.CarrierName.ToLower() });
                        projectQuery.Queries.Add(new QuickBaseSingleQuery { FieldId = 74, ComparisonOperator = QuickBaseComparisonOperator.XCT, Value = "Cancelled" });

                        var customerCarrierQuery = new QuickBaseMultipleQuery { AndOrOperator = QuickBaseAndOrOperator.AND };
                        customerCarrierQuery.Queries.Add(new QuickBaseSingleQuery { FieldId = 18, ComparisonOperator = QuickBaseComparisonOperator.EX, Value = CustomerName.ToLower() });
                        customerCarrierQuery.Queries.Add(new QuickBaseSingleQuery { FieldId = 19, ComparisonOperator = QuickBaseComparisonOperator.EX, Value = CarrierObj.CarrierName.ToLower() });
                        customerCarrierQuery.Queries.Add(new QuickBaseSingleQuery { FieldId = 74, ComparisonOperator = QuickBaseComparisonOperator.XCT, Value = "Cancelled" });


                        if (orderObj.OrderType == 1)
                        {
                            projectQuery.Queries.Add(new QuickBaseSingleQuery { FieldId = 69, ComparisonOperator = QuickBaseComparisonOperator.XCT, Value = "OE" });
                            customerCarrierQuery.Queries.Add(new QuickBaseSingleQuery { FieldId = 69, ComparisonOperator = QuickBaseComparisonOperator.XCT, Value = "OE" });
                            projectQuery.Queries.Add(new QuickBaseSingleQuery { FieldId = 69, ComparisonOperator = QuickBaseComparisonOperator.XCT, Value = "DF" });
                            customerCarrierQuery.Queries.Add(new QuickBaseSingleQuery { FieldId = 69, ComparisonOperator = QuickBaseComparisonOperator.XCT, Value = "DF" });
                        }
                        else if (orderObj.OrderType == 2)
                        {
                            DateTime? OEstart = CarrierObj.OEStartDate;

                            projectQuery.Queries.Add(new QuickBaseSingleQuery { FieldId = 69, ComparisonOperator = QuickBaseComparisonOperator.CT, Value = "OE" });
                            projectQuery.Queries.Add(new QuickBaseSingleQuery { FieldId = 70, ComparisonOperator = QuickBaseComparisonOperator.EX, Value = OEstart.Value.ToShortDateString() });

                            customerCarrierQuery.Queries.Add(new QuickBaseSingleQuery { FieldId = 69, ComparisonOperator = QuickBaseComparisonOperator.CT, Value = "OE" });
                            customerCarrierQuery.Queries.Add(new QuickBaseSingleQuery { FieldId = 70, ComparisonOperator = QuickBaseComparisonOperator.EX, Value = OEstart.Value.ToShortDateString() });

                        }
                        else if (orderObj.OrderType == 3)
                        {

                            projectQuery.Queries.Add(new QuickBaseSingleQuery { FieldId = 69, ComparisonOperator = QuickBaseComparisonOperator.CT, Value = "DF" });
                            customerCarrierQuery.Queries.Add(new QuickBaseSingleQuery { FieldId = 69, ComparisonOperator = QuickBaseComparisonOperator.CT, Value = "DF" });

                        }

                        var mainQuery = new QuickBaseMultipleQuery { AndOrOperator = QuickBaseAndOrOperator.OR };
                        mainQuery.Queries.Add(projectQuery);
                        mainQuery.Queries.Add(customerCarrierQuery);


                        var datacount = connection.DoQueryCount(protable, mainQuery);

                        if (datacount == 0)
                        {
                            logger.Error("Err: Couldn't cancell proj" + CarrierObj.ProjectName);
                            CarrierObj.QBCancelled = true;
                            ebnDB.SaveChanges();
                            return true; // to emove carrier check to create QB
                        }

                        DataTable projdataTable;
                        if (Environment == "production")//3 = record id#, 74 = status, 249 =nonvipstatus2
                            projdataTable = connection.DoQueryAndFillTable(protable, mainQuery, new int[] { 298, 299, 300, 74, 3, 127 }.ToList(), null);
                        else  //127 = cancellation date
                            projdataTable = connection.DoQueryAndFillTable(protable, mainQuery, new int[] { 338, 339, 40, 340, 74, 3, 127 }.ToList(), null);

                        DataRow Records = projdataTable.Rows[0];
                        if (Records != null)
                        {
                            #region cancellation reason
                            if (orderObj.OrderType != 2)
                            {
                                int? ReasonFieldId = ebnDB.Carriers.Where(c => c.CarrierID == carrierID).Select(c => c.CancellationReasonField).FirstOrDefault();
                                //int ReasonCatId = 3;//ebnDB.CancellationReasonFields.Where(c => c.ID == ReasonFieldId).Select(c => c.CancellationReasonCategory.ID).FirstOrDefault();
                                string CancellationReasonCat = "Partner Request"; ;// ebnDB.CancellationReasonFields.Where(c => c.ID == ReasonFieldId).Select(c => c.CancellationReasonCategory.ReasonCategory).FirstOrDefault();
                                string comment = ebnDB.Carriers.Where(c => c.CarrierID == carrierID).Select(c => c.CancellationReasonComment).FirstOrDefault();
                                string other = ebnDB.Carriers.Where(c => c.CarrierID == carrierID).Select(c => c.OtherCancellationReason).FirstOrDefault();
                                
                                    Records["Cancellation Reason Category"] = CancellationReasonCat;
                             
                            }
                            #endregion
                            Records["Status"] = "Cancelled";
                            ChangeRequestProject crp = ebnDB.ChangeRequestProjects.Where(w => w.ProjectId == carrierID).OrderByDescending(w => w.Id).FirstOrDefault();
                            if (crp != null)
                            {
                                ChangeRequest cr = ebnDB.ChangeRequests.Where(k => k.ID == crp.ChRequestId && (k.ChRequestTypeId == 2 || k.ChRequestTypeId == 1)).FirstOrDefault();
                                if (cr != null)
                                    Records["Cancellation Date"] = cr.ChRequestDate.Value.ToString("MM-dd-yyyy");
                                else
                                    Records["Cancellation Date"] = DateTime.Now.Date.ToString("MM-dd-yyyy");
                            }
                            else
                                Records["Cancellation Date"] = DateTime.Now.Date.ToString("MM-dd-yyyy");

                            try
                            {
                                var valprojdict = Records.Table.Columns.Cast<DataColumn>().ToDictionary(c => c.ColumnName, c => Records[c]);
                                var n = connection.UPDRecord(protable, Records["Record ID#"].ToString(), valprojdict);
                            }
                            catch (Exception e)
                            {
                                logger.Info("User=: " + System.Web.HttpContext.Current.User.Identity.Name);
                                logger.Error("Err mes=: QB status were not updated for connectionID: " + carrierID);
                                if (e.InnerException != null) logger.Error("Inner Exce=: " + e.InnerException);
                                logger.Error("Err mes=: " + e.Message);
                                logger.Error("Stack trace=: " + e.StackTrace);
                                return false;
                            }
                        }

                        CarrierObj.QBCancelled = true;
                        ebnDB.SaveChanges();
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Info("User=: " + System.Web.HttpContext.Current.User.Identity.Name);
                if (e.InnerException != null) logger.Error("Inner Exce=: " + e.InnerException);
                logger.Error("Err mes=: " + e.Message);
                logger.Error("Stack trace=: " + e.StackTrace);
                return false;
            }
        }



    }
}
