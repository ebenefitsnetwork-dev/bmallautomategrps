﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.Net.Mime;
using System.Net.Configuration;
using System.Net;
using System.IO;
using System.Configuration;

namespace AutomateBultOrders
{
    class SndMail : IDisposable
    {
        /*
                public string MsgTo { get; set; }
                public string Msgbody { get; set; }
                * 
                */
        private static SmtpSection smtpSection; // ebnnotification
        private static MailMessage Msg;
        private static SmtpClient SmtpClnt;
        System.Net.Mail.Attachment attachment;


        public void Snd(string EmailTo = null, string Subject = null, string contactName = null, string ActionDesc = null, string DueDate = null, string link = null, string requester = null)
        {
            string DirPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

            using (SmtpClnt = new SmtpClient())
            {
                using (Msg = new MailMessage())
                {
                    Msg.IsBodyHtml = true;
                    if (EmailTo == null)
                    {
                        //Msg.To.Add(new MailAddress("e.adel84@gmail.com"));
                        Msg.To.Add(new MailAddress("nada.naguib@ebenefitsnetwork.com"));

                        Msg.Subject = "Error in copying QB data to Ebn";

                        string path = DirPath + @"\Content\_Copyingdatafaillure.html";                            //Path = Server.MapPath("~/Content/_aduserreg.html");path = System.IO.Path.GetDirectoryName( 
                        Msg.Body = (System.IO.File.ReadAllText(path).Replace("_PartnerName", "wtever"));
                        //  attachment = new System.Net.Mail.Attachment(@"\QBData-%date{yyyyMMdd_HHmmss}.log");
                        attachment = new System.Net.Mail.Attachment(DirPath + @"\log.txt");
                        Msg.Attachments.Add(attachment);
                    }
                    else
                    {
                        Msg.To.Add(new MailAddress(EmailTo));
                        Msg.Subject = Subject;
                        string path = DirPath + @"\Content\_NewActionItem.html";
                        //Path = Server.MapPath("~/Content/_aduserreg.html");path = System.IO.Path.GetDirectoryName( 
                        string Link = ConfigurationManager.AppSettings["WebSiteHyperLink"] + link;
                        Msg.Body = (System.IO.File.ReadAllText(path).Replace("_Contactname", contactName).Replace("_ActionDesc", ActionDesc).Replace("_DueTime", DueDate).Replace("AddyourLinkhere", Link).Replace("_requester", requester));
                    }
                    SmtpClnt.Send(Msg);

                }
            }
            if (File.Exists(DirPath + @"\log.txt"))
            {
                File.Delete(DirPath + @"\log.txt");
            }

        }

        public void Dispose()
        {
            // variables will dispose automatic
            // don't add static objects at this function - because no need for dispose it and  create it again .


            // system will remove objects from garbage collector when system don't use it for a long time  
            GC.SuppressFinalize(this);

        }
    }
}
